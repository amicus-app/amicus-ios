//
//  main.m
//  Amicus-Admin
//
//  Created by Josh Sklar on 7/12/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AAAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AAAppDelegate class]));
    }
}
