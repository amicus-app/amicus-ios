//
//  AAViewController.m
//  Amicus-Admin
//
//  Created by Josh Sklar on 7/12/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import "AAViewController.h"
#import <Parse/Parse.h>

@interface AAViewController () <UITableViewDataSource, UITableViewDelegate>
{
    NSArray *users;
}
@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *label;
@end

@implementation AAViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tableView setDataSource:self];
    [self.tableView setDelegate:self];
    [self fetchUsers];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)refreshBtnTapped:(id)sender
{
    [self fetchUsers];
}

- (void)fetchUsers
{
    PFQuery *q = [PFQuery queryWithClassName:@"MasterUserList"];
    [self.label setText:@"Loading..."];
    [q findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        users = [objects sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            PFObject *o1 = (PFObject*)obj1;
            PFObject *o2 = (PFObject*)obj2;
            return [[o2 createdAt] compare:[o1 createdAt]];
        }];
        
        [self.tableView reloadData];
        [self.label setText:[NSString stringWithFormat:@"Users: %i", users.count]];
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return users.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"cellID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellID];
    }
    PFObject *o = [users objectAtIndex:indexPath.row];
    [cell.textLabel setText:[o valueForKey:@"name"]];
    NSDateFormatter *df = [[NSDateFormatter alloc]init];
    [df setDateStyle:NSDateFormatterFullStyle];
    [cell.detailTextLabel setText:[df stringFromDate:[o createdAt]]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
@end
