//
//  AAAppDelegate.h
//  Amicus-Admin
//
//  Created by Josh Sklar on 7/12/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AAViewController;

@interface AAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) AAViewController *viewController;

@end
