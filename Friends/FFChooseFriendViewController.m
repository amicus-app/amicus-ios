//
//  FFChooseFriendViewController.m
//  Friends
//
//  Created by Josh Sklar on 3/21/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import "FFChooseFriendViewController.h"
#import "FFFacebookController.h"
#import "FFGamesListViewController.h"
#import <Parse/Parse.h>
#import "UIView+Animation.h"
#import "EGORefreshTableHeaderView.h"
#import "FFUtilities.h"
#import "FFProfilePictureCache.h"
#import "UILabel+Font.h"
#import "JSFindFriendsViewController.h"
#import "JSReachability.h"
#import "JSBackButton.h"

static const CGFloat kCancelBtnHeight = 40;

@interface FFChooseFriendViewController () <UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate, EGORefreshTableHeaderDelegate, FFFacebookControllerDelegate>
{
@private
    EGORefreshTableHeaderView *_refreshHeaderView;
	//  Reloading var should really be your tableviews datasource
	//  Putting it here for demo purposes
	BOOL _reloading;
}

/* EGO pull to refresh methods */
- (void)reloadTableViewDataSource;
- (void)doneLoadingTableViewData;

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) UISearchBar *searchBar;
@property (strong, nonatomic) NSMutableArray *filteredFriendsArray;
@property (strong, nonatomic) NSMutableArray *cachedFilteredFriendsArray;

@property (strong, nonatomic) UILabel *loadingLabel;

@property BOOL receivedBothDataSets;

@end

@implementation FFChooseFriendViewController

- (id)init
{
    self = [super init];
    if (self) {
        self.cachedFilteredFriendsArray = nil;
        
        self.loadingLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 100)];
        [self.loadingLabel setTextAlignment:NSTextAlignmentCenter];
        [self.loadingLabel setAppFontBlackItalic:20 color:nil];
        [self.loadingLabel setText:@"Loading Friends..."];
        [self.loadingLabel setBackgroundColor:[UIColor clearColor]];
        [self.loadingLabel setCenter:self.view.center];
        [self.loadingLabel setAlpha:0.];
        [self.view addSubview:self.loadingLabel];
        self.receivedBothDataSets = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, kCancelBtnHeight, self.view.frame.size.width, self.view.frame.size.height - kCancelBtnHeight)];
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    [self.view addSubview:self.tableView];
    
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 0)];
    self.searchBar.delegate = self;
    [self.searchBar sizeToFit];
    [[[self.searchBar subviews] objectAtIndex:0] removeFromSuperview];

    [self.searchBar setPlaceholder:@"Search Friends"];
    self.tableView.tableHeaderView = self.searchBar;
    
    /* pull to refresh methods */
    if (_refreshHeaderView == nil) {
		
		EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - self.tableView.bounds.size.height, self.view.frame.size.width, self.tableView.bounds.size.height)];
		view.delegate = self;
		[self.tableView addSubview:view];
		_refreshHeaderView = view;
	}
    
    // set up the toolbar
    UINavigationBar *navBar = [[UINavigationBar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    
    [navBar setBackgroundImage:[UIImage imageNamed:@"home-navbar"] forBarMetrics:UIBarMetricsDefault];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:navBar.bounds];
    [titleLabel setText:@"Choose Friend"];
    [titleLabel setAppFontBlackItalic:24 color:[UIColor whiteColor]];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [navBar addSubview:titleLabel];
    [self.view addSubview:navBar];
    
    UIButton *dismiss = [[JSBackButton alloc]init];
    [dismiss addTarget:self action:@selector(didTapCancel) forControlEvents:UIControlEventTouchUpInside];

    [navBar addSubview:dismiss];
    
    
    self.receivedBothDataSets = NO;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)filterFriendsList
{
    // check if its stored in cache first
    if (self.cachedFilteredFriendsArray) {
        self.filteredFriendsArray = [NSMutableArray arrayWithArray:self.cachedFilteredFriendsArray];
    }
    else {
        if (![JSReachability hasInternetConnection:@"Can't load friends"])
            return;
        
        [self.loadingLabel partialFadeWithDuration:0.2 afterDelay:0. withFinalAlpha:1. completionBlock:nil];
        PFQuery *q = [PFQuery queryWithClassName:kPFMasterUserListClass];
        NSArray *friendIds = [FFFacebookController getFriendIdsArray];
        [q whereKey:@"fbId" containedIn:friendIds];
        [q findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                NSMutableArray *tempArray = [[NSMutableArray alloc]initWithCapacity:objects.count];
                for (PFObject *obj in objects) {
                    NSDictionary *d = @{@"name": [obj valueForKey:@"name"],
                                        @"id": [obj valueForKey:@"fbId"]};
                    [tempArray addObject:d];
                }
                self.friendsArray = nil;
                self.friendsArray = (NSArray*)tempArray;
                self.filteredFriendsArray = [NSMutableArray arrayWithArray:tempArray];
                [self.loadingLabel partialFadeWithDuration:0.2 afterDelay:0. withFinalAlpha:0. completionBlock:nil];
                [self.tableView reloadData];
                [self doneLoadingTableViewData];
                
                self.cachedFilteredFriendsArray = self.filteredFriendsArray;
                // hide the loading hud, show the table view
            }
        }];
        

        // get it from parse, then save it to user defaults
        
    }
}

#pragma mark - Internal methods

- (void)fetchAndFilterFriendsList
{
    // get list of facebook friends
    [self.loadingLabel partialFadeWithDuration:0.2 afterDelay:0. withFinalAlpha:1. completionBlock:nil];
    
    FFFacebookController *fb = [FFFacebookController sharedInstance];
    [fb activateNetworkConnectionState];
    [fb setDelegate:self];
    [fb requestFriends];
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.filteredFriendsArray count] + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    if (indexPath.row == [self.filteredFriendsArray count]) {
        [cell.imageView setHidden:YES];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        [cell.textLabel setAppFontLightItalic:20 color:nil];
        [cell.textLabel setText:@"Invite Friends..."];
        return cell;
    }
    NSDictionary *friend = [self.filteredFriendsArray objectAtIndex:indexPath.row];
    [cell.textLabel setText:[friend valueForKey:@"name"]];
    [cell.textLabel setAppFontRegular:20 color:nil];

    [cell.imageView setHidden:NO];
    cell.imageView.layer.borderColor = [UIColor blackColor].CGColor;
    [cell.imageView setContentMode:UIViewContentModeScaleToFill];
    cell.imageView.layer.borderWidth = 2.;
    cell.textLabel.textAlignment = NSTextAlignmentLeft;
    
    [cell.imageView setImage:[UIImage imageNamed:@"fb_default_pic"]];
    [[FFProfilePictureCache sharedInstance] getProfilePictureThumbnailForFriend:[friend valueForKey:@"id"] withCompletionBlock:^(NSData *profilePictureData) {
        [cell.imageView setImage:[UIImage imageWithData:profilePictureData]];
    }];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == self.filteredFriendsArray.count) {
        // bring up the find friends VC
        JSFindFriendsViewController *find = [[JSFindFriendsViewController alloc]init];
        UINavigationController *navCont = [[UINavigationController alloc]initWithRootViewController:find];
        [self presentViewController:navCont animated:YES completion:nil];
        return;
    }
    
    NSDictionary *friend = [self.filteredFriendsArray objectAtIndex:indexPath.row];
    
    //[[FFFacebookController sharedInstance] requestMutualFriendsWith:[friend valueForKey:@"id"]];
    
    [self dismissViewControllerAnimated:YES completion:^{
        [self.delegate didSelectFriend:friend];
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [_refreshHeaderView egoRefreshScrollViewDidScroll:self.tableView];
    [self.searchBar resignFirstResponder];
}

#pragma mark - UISearchBar delegate mehods

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
	[self filterFriends:searchText];
    [self.tableView reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar_
{
	[searchBar_ resignFirstResponder];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
}

- (void)filterFriends:(NSString*)searchText
{
    [self.filteredFriendsArray removeAllObjects];
	
	if (searchText == nil || [searchText length] == 0) {
		[self.filteredFriendsArray addObjectsFromArray:self.friendsArray];
	}
	else {
		for (NSDictionary *friend in self.friendsArray) {
			
			NSString *friendName = [friend valueForKey:@"name"];
			
			NSRange searchRange = [friendName rangeOfString:searchText options:NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch];
            if (searchRange.length > 0)
            {
                [self.filteredFriendsArray addObject:friend];
            }
		}
	}
}

#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource
{
    [self fetchAndFilterFriendsList];
	//  should be calling your tableviews data source model to reload
	//  put here just for demo
	_reloading = YES;
}

- (void)doneLoadingTableViewData
{
	//  model should call this when its done loading
	_reloading = NO;
	[_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
}
#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view
{
	[self reloadTableViewDataSource];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view
{
	return _reloading; // should return if data source model is reloading	
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
	
	return [NSDate date]; // should return date data source was last changed
}

#pragma mark - FFFacebookController deelegate methods

- (void)didReceiveFriends:(NSArray *)friends
{
    [[FFFacebookController sharedInstance] inactivateNetworkConnectionState];
    self.cachedFilteredFriendsArray = nil;
    [self filterFriendsList];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)didTapCancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}



@end
