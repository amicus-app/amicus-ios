//
//  FFSinglePlayerViewController.m
//  Friends
//
//  Created by Josh Sklar on 3/30/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import "FFSinglePlayerViewController.h"
#import "FFFacebookController.h"
#import "FFOptionButton.h"
#import "FFUtilities.h"
#import "FFGamesListViewController.h"
#import "UILabel+Font.h"
#import "UIView+Positioning.h"
#import <objc/runtime.h>
#import "JSReachability.h"

#define kEndGameText @"End Game"

static const NSInteger kNumIncorrectAnswersLimit = 8;

@interface FFSinglePlayerViewController () <UIAlertViewDelegate>

@property (strong, nonatomic) UILabel *scoreLabel, *numIncorrectAnswersLabel, *highScoreLabel;
@property (strong, nonatomic) UILabel *scoreValueLabel, *numIncorrectAnswersValueLabel;
@property (strong, nonatomic) UIButton *endGameButton;

@property NSInteger scoreValue, numIncorrentAnswersValue;

@end

@implementation FFSinglePlayerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.title = @"Single Player";
    
    /* deal with setting the score label, and number of incorrect answers */
    self.scoreValue = [[self.singlePlayerGameObj valueForKey:@"score"] integerValue];
    self.numIncorrentAnswersValue = [[self.singlePlayerGameObj valueForKey:@"num_incorrect_answers"] integerValue];
    
    [self.scoreLabel setText:@"Score:"];
    [self.numIncorrectAnswersLabel setText:@"Wrong Answers:"];
    
    [self.scoreValueLabel setText:[NSString stringWithFormat:@"%i", self.scoreValue]];
    [self.scoreValueLabel configureFontSizeWithInitialSize:22];
    [self.numIncorrectAnswersValueLabel setText:[NSString stringWithFormat:@"%i/8", self.numIncorrentAnswersValue]];
    [self.numIncorrectAnswersValueLabel configureFontSizeWithInitialSize:22];

    [self.endGameButton setHidden:NO];
    [self.endGameButton setEnabled:YES];
    [self.endGameButton setTitle:kEndGameText forState:UIControlStateNormal];
    
//    [self.highScoreLabel setText:[NSString stringWithFormat:@"High Score: %i", [[self.singlePlayerGameObj valueForKey:@"high_score"] integerValue]]];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.endGameButton setHidden:YES];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    NSArray *ar = @[self.scoreLabel, self.numIncorrectAnswersLabel];
    for (UILabel *l in ar) {
        [l setText:@""];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Virtual functions

- (void)determineMove
{
    if (![JSReachability hasInternetConnection:@"Can't play"])
        return;
    
    FFFacebookController *f = [FFFacebookController sharedInstance];
    [f activateNetworkConnectionState];
    [f setDelegate:self];
    [f requestMutualFriendsWith:[[FFFacebookController me] valueForKey:@"id"]];
}

- (void)processMoveWithNameSelected:(NSString*)name
{
    [self.scoreValueLabel setText:[NSString stringWithFormat:@"%i", self.scoreValue]];
    [self.scoreValueLabel configureFontSizeWithInitialSize:22];
    [self.numIncorrectAnswersValueLabel setText:[NSString stringWithFormat:@"%i/8", self.numIncorrentAnswersValue]];
    [self.numIncorrectAnswersValueLabel configureFontSizeWithInitialSize:22];

    
    PFQuery *q = [PFQuery queryWithClassName:kPFSinglePlayerGamesClassName];
    NSString *gameName = [NSString stringWithFormat:@"SG%@",[[FFFacebookController me]valueForKey:@"id"]];
    [q whereKey:@"game_name" equalTo:gameName];
    [q findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error) {
            JSAssert(NO, [error description]);
        }
        else {
            JSAssert(objects.count <= 1, @"More than 1 single player game for you in database. Occured in processMoveWithNameSelected");
            PFObject *o = [objects objectAtIndex:0];
            [o setValue:[NSNumber numberWithInteger:self.scoreValue] forKey:@"score"];
            [o setValue:[NSNumber numberWithInteger:self.numIncorrentAnswersValue] forKey:@"num_incorrect_answers"];
            [o saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (!succeeded)
                    JSAssert(false, [error description]);
                [self.home fetchSinglePlayerGame];
            }];
        }
    }];
}

- (void)dispatchTimeRanOut
{
    if (self.numIncorrentAnswersValue == kNumIncorrectAnswersLimit) {
        [self endCurrentGame];
    }
    else {
        UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"Time Ran Out" message:@"" delegate:self cancelButtonTitle:@"Stop Playing" otherButtonTitles:@"Continue Playing", nil];
        av.tag = kSinglePlayerTimeRanOutAlertViewTag;
        
        [av show];
    }
}

#pragma mark - Overridden functions

- (void)setupUI
{
    [super setupUI];
    
    self.scoreLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200, 35)];
    self.numIncorrectAnswersLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 35, 200, 35)];
    
    NSArray *ar = @[self.numIncorrectAnswersLabel, self.scoreLabel];
    for (UILabel *l in ar) {
        [l setAppFontLightItalic:22 color:[UIColor whiteColor]];
        l.textAlignment = NSTextAlignmentCenter;
        l.backgroundColor = [UIColor clearColor];
        [self.view addSubview:l];
    }
    
    self.scoreValueLabel = [[UILabel alloc]initWithFrame:CGRectMake([self.scoreLabel getLocationOfRight], 2, 31, 31)];
    [self.scoreValueLabel configureLabelForSinglePlayerScore];
    
    self.numIncorrectAnswersValueLabel = [[UILabel alloc]initWithFrame:CGRectMake([self.numIncorrectAnswersLabel getLocationOfRight], 5 + [self.scoreValueLabel getLocationOfBottom], 31, 31)];
    self.numIncorrectAnswersValueLabel.backgroundColor = [UIColor clearColor];
    [self.numIncorrectAnswersValueLabel setAppFontBlackItalic:22 color:[UIColor colorWithRed:156./255. green:101./255. blue:109./255. alpha:1]];
    
    NSArray *arr = @[self.scoreValueLabel, self.numIncorrectAnswersValueLabel];
    for (UILabel *l in arr) {
        l.textAlignment = NSTextAlignmentCenter;
        [self.view addSubview:l];
    }
    
    static CGFloat endGameButtonWidth = 70;
    self.endGameButton = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width - 5 - endGameButtonWidth, 11, endGameButtonWidth, 22)];
    [self.endGameButton.titleLabel setAppFontBlackItalic:15 color:[UIColor whiteColor]];
    [self.endGameButton setTitle:kEndGameText forState:UIControlStateNormal];
    [self.endGameButton setShowsTouchWhenHighlighted:YES];
    [self.endGameButton addTarget:self action:@selector(endCurrentGame) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationController.navigationBar addSubview:self.endGameButton];
}

- (void)dispatchSelectedOptionButton:(FFOptionButton*)btn
{
    NSString *name = btn.name;

    // correct
    if ([name isEqualToString:self.answersName])
        self.scoreValue += self.timeTakenToAnswer;
    
    // incorrect
    else {
        self.numIncorrentAnswersValue += 1;
    }
    
    [self.timerForAnswer invalidate];
    self.timerForAnswer = nil;
    [self.pieTimerCountroller stopAnimation];
    
    [self animateCorrectOptionButtonWithCompletionBlock:^(BOOL finished) {
        
        if (self.numIncorrentAnswersValue == kNumIncorrectAnswersLimit) {
            [self endCurrentGame];
            return;
        }

        [self processMoveWithNameSelected:name];

        
        UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"" message:@"" delegate:self cancelButtonTitle:@"Stop" otherButtonTitles:@"Continue", nil];
        if ([name isEqualToString:self.answersName]) {
            [av setTitle:@"Correct"];
        }
        else {
            [av setTitle:@"Incorrect"];
        }
        [av show];
    }];
}

#pragma mark - UIAlertView delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"alert view %i got clicked ", alertView.tag);
    if (alertView.tag == kEndGameAlertViewTag) {
        if (buttonIndex == 1) {
            // end the round
            [self endCurrentRound];
        }
    }
    else if (alertView.tag == kSinglePlayerTimeRanOutAlertViewTag) {
        if (buttonIndex == 0)
            [self endCurrentRound];
        else if (buttonIndex == 1) {
            self.numIncorrentAnswersValue += 1;
            
            [self.timerForAnswer invalidate];
            self.timerForAnswer = nil;
            [self.pieTimerCountroller reset];
            
            [self.home disableSinglePlayerButton];
            if (self.numIncorrentAnswersValue == kNumIncorrectAnswersLimit) {
                [self endCurrentGame];
                return;
            }
            [self processMoveWithNameSelected:nil];
            [self resetUI];
            [self determineMove];
        }
    }
    else if (alertView.tag == kSinglePlayerNewHighScoeAlertViewTag) {
//        NSString *score = objc_getAssociatedObject(alertView, @"new_high_score_key");
//
//        NSString *msg = [NSString stringWithFormat:@"Check out my new high score on Amicus - %@!", score];
//        [[NSNotificationCenter defaultCenter] postNotificationName:kNCNewHighScore object:msg];
    }
    else {
        if (buttonIndex == 0) {
            [self.navigationController popViewControllerAnimated:YES];
        }
        else if (buttonIndex == 1) {
            [self resetUI];
            [self determineMove];
        }
        else {
            JSAssert(NO, @"Clicked a button index other than 0 or 1");
        }
    }
    

}

#pragma mark - Internal methods

- (void)resetUI
{
    [self.questionLabel setText:@"Loading..."];
    [self.answerLabel setText:@""];
    self.timeTakenToAnswer = kRoundDuration;
    [self.timerLabel setText:[NSString stringWithFormat:@"%i",(NSInteger)kRoundDuration]];
    [self.timerForAnswer invalidate];
    self.timerForAnswer = nil;
    [self.pieTimerCountroller reset];
    
    NSArray *optionButtons = @[self.optionButton1, self.optionButton2, self.optionButton3, self.optionButton4];
    for (FFOptionButton *b in optionButtons) {
        [b setAlpha:0.];
        [b resetBackgroundImage];
    }
    
    [self.loadingActivityIndicator setHidden:NO];

    [self.imageView setImage:nil];
}

- (void)endCurrentGame
{
    if (![JSReachability hasInternetConnection:@"Can't play"])
        return;
    
    [self.endGameButton setEnabled:NO];
    [self.endGameButton setTitle:@"Ending..." forState:UIControlStateNormal];
    
    PFQuery *q = [PFQuery queryWithClassName:kPFSinglePlayerGamesClassName];
    NSString *gameName = [NSString stringWithFormat:@"SG%@",[[FFFacebookController me]valueForKey:@"id"]];
    [q whereKey:@"game_name" equalTo:gameName];
    [q findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error) {
            JSAssert(NO, [error description]);
        }
        else {
            JSAssert(objects.count <= 1, @"More than 1 single player game for you in database. Occured in endCurrentGame");
            PFObject *o = [objects objectAtIndex:0];
            [o setValue:@0 forKey:@"score"];
            [o setValue:@0 forKey:@"num_incorrect_answers"];
            
            if ([[o valueForKey:@"high_score"] intValue] < self.scoreValue) {
                
                NSString *msg = [NSString stringWithFormat:@"Check out my new high score on Amicus - %i!", self.scoreValue];
                
                [[FFFacebookController sharedInstance]  postToFacebook:msg];
                
                
                UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"New High Score!" message:nil delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
                av.tag = kSinglePlayerNewHighScoeAlertViewTag;
                [av show];

                
                [o setValue:[NSNumber numberWithInteger:self.scoreValue] forKey:@"high_score"];
                
                PFQuery *recordsQuery = [PFQuery queryWithClassName:kPFRecordsClassName];
                [recordsQuery whereKey:kPFRecordsUserIdKey equalTo:[[FFFacebookController me] valueForKey:@"id"]];
                [recordsQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                    JSAssert(objects.count == 1, @"more than 1 record object for a user id");
                    PFObject *obj = [objects objectAtIndex:0];
                    [obj setValue:[NSNumber numberWithInteger:self.scoreValue] forKey:@"single_player_high_score"];
                    [obj saveInBackground];
                }];
            }
            
            [o saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (!succeeded)
                    JSAssert(false, [error description]);
                UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"Game Over" message:@"This game has ended." delegate:self.home cancelButtonTitle:@"OK" otherButtonTitles:@"Play Again", nil];
                av.tag = kSinglePlayerGameOverAlertViewTag;
                [av show];

                [self.home fetchSinglePlayerGame];
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }
    }];
}

- (void)endCurrentRound
{
    if (![JSReachability hasInternetConnection:@"Can't play"])
        return;
    
    self.numIncorrentAnswersValue += 1;
    
    [self.timerForAnswer invalidate];
    self.timerForAnswer = nil;
    [self.pieTimerCountroller reset];
    
    [self.home disableSinglePlayerButton];
    if (self.numIncorrentAnswersValue == kNumIncorrectAnswersLimit) {
        [self endCurrentGame];
        return;
    }
    [self.navigationController popViewControllerAnimated:YES];
    [self processMoveWithNameSelected:nil];
}

@end
