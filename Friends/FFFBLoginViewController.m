//
//  FFFBLoginViewController.m
//  Friends
//
//  Created by Josh Sklar on 3/21/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import "FFFBLoginViewController.h"
#import "FFFacebookController.h"
#import "FFGamesListViewController.h"
#import "UIView+Positioning.h"
#import "UILabel+Font.h"
#import "JSReachability.h"

@interface FFFBLoginViewController () <FFFacebookControllerDelegate>
{
@private
    BOOL gotFriendsAndMe;
    BOOL stopAnimating;
    
}

@property (strong, nonatomic) UIButton *loginWithFBButton;

@end

@implementation FFFBLoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];    
    self.title = @"Amicus";
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"home-navbar"] forBarMetrics:UIBarMetricsDefault];

    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]];
    
    [self setupSlidingView];
    [self setupLoginButtons];
    [self setupHints];
    
    gotFriendsAndMe = NO;
    stopAnimating = NO;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didPressLoginWithFacebookBtn:(id)sender
{
    if (![JSReachability hasInternetConnection:@"Can't log in"])
        return;
    
    FFFacebookController *facebookController = [FFFacebookController sharedInstance];
    facebookController.delegate = self;
    [facebookController login];
    [self.loginWithFBButton setEnabled:NO];
    [self.loginWithFBButton setTitle:@"Logging In..." forState:UIControlStateNormal];
}

-(void)didLogin
{
    if (gotFriendsAndMe) {
        [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:kDidLogInKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        FFGamesListViewController *games = [[FFGamesListViewController alloc]init];
        stopAnimating = YES;
        [self.navigationController pushViewController:games animated:YES];
    }
    gotFriendsAndMe = YES;
}

- (void)didReceiveFriends:(NSArray *)friends
{
    if (gotFriendsAndMe) {
        [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:kDidLogInKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        FFGamesListViewController *games = [[FFGamesListViewController alloc]init];
        
        [self.navigationController pushViewController:games animated:YES];
    }
    gotFriendsAndMe = YES;
}

- (void)didCancel
{
    [self.loginWithFBButton setEnabled:YES];
    gotFriendsAndMe = NO;
}

- (void)setupSlidingView
{
    UIImageView *iv = [[UIImageView alloc]initWithFrame:CGRectMake(0, 20, 536, [UIScreen mainScreen].bounds.size.height - 104)];
    iv.contentMode = UIViewContentModeScaleToFill;
    iv.image = [UIImage imageNamed:@"moving_background"];
    
    [self beginSlidingAnimation:iv];
    [self.view addSubview:iv];
}

- (void)beginSlidingAnimation:(UIImageView*)iv
{
    
    [UIView animateWithDuration:10
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         iv.frame = CGRectMake(-216, 20, 536, [UIScreen mainScreen].bounds.size.height - 104);
                     } completion:^(BOOL finished) {
                         if (stopAnimating)
                             return;
                         [UIView animateWithDuration:1
                                               delay:0
                                             options:UIViewAnimationOptionCurveEaseInOut
                                          animations:^{
                                              iv.frame = CGRectMake(0, 20, 536, [UIScreen mainScreen].bounds.size.height - 104);
                                          } completion:^(BOOL finished) {
                                              [self beginSlidingAnimation:iv];
                                          }];
                     }];
}

- (void)setupLoginButtons
{
    CGFloat height = 150;
    UIButton *twtrBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [twtrBtn setFrame:CGRectMake(0, height, 108, 106)];
    [twtrBtn setImage:[UIImage imageNamed:@"twitter"] forState:UIControlStateNormal];
    [self.view addSubview:twtrBtn];
    
    self.loginWithFBButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.loginWithFBButton setFrame:CGRectMake([twtrBtn getLocationOfRight], height, 108, 106)];
    [self.loginWithFBButton setImage:[UIImage imageNamed:@"facebook"] forState:UIControlStateNormal];
    [self.loginWithFBButton addTarget:self action:@selector(didPressLoginWithFacebookBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.loginWithFBButton];
    
    UIButton *instBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [instBtn setFrame:CGRectMake([self.loginWithFBButton getLocationOfRight], height, 108, 106)];
    [instBtn setImage:[UIImage imageNamed:@"instagram"] forState:UIControlStateNormal];
    [self.view addSubview:instBtn];
    
    UILabel *signInBar = [[UILabel alloc]initWithFrame:CGRectMake(0, height + 178, 320, 52)];
    [signInBar setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"sign-in-bar"]]];
    [signInBar setAppFontBlackItalic:20 color:nil];
    signInBar.text = @"sign in and start playing today";
    [signInBar configureFontSizeWithInitialSize:20];
    signInBar.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:signInBar];
}

- (void)setupHints
{
    UIImageView *iv1 = [[UIImageView alloc]initWithFrame:CGRectMake(0, 45, 136, 35)];
    [iv1 centerHorizontally];
    [iv1 setImage:[UIImage imageNamed:@"tap-here"]];
    [self.view addSubview:iv1];
    
    UIImageView *iv2 = [[UIImageView alloc]initWithFrame:CGRectMake(0, 80, 15, 55)];
    [iv2 centerHorizontally];
    [iv2 setImage:[UIImage imageNamed:@"arrow"]];
    [self.view addSubview:iv2];
}


@end
