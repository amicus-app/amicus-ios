//
//  FFGamesListCell.h
//  Friends
//
//  Created by Josh Sklar on 4/2/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FFGamesListCell : UICollectionViewCell

@property (strong, nonatomic) UILabel *nameLabel;

- (void)setProfilePicture:(NSData*)data;

@end
