//
//  FFGamesListFlowLayout.h
//  Friends
//
//  Created by Josh Sklar on 4/2/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FFGamesListFlowLayout : UICollectionViewFlowLayout
{
    UICollectionViewScrollDirection _scrollDirection;
}

- (UICollectionViewScrollDirection)scrollDirection;

@property (nonatomic) UICollectionViewScrollDirection scrollDirection;

@end
