//
//  FFChooseFriendViewController.h
//  Friends
//
//  Created by Josh Sklar on 3/21/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FFChooseFriendDelegate <NSObject>

- (void)didSelectFriend:(NSDictionary*)friendDict;

@end

@interface FFChooseFriendViewController : UIViewController

- (void)filterFriendsList;
- (id)init;
@property (strong, nonatomic) NSArray *friendsArray;
@property (strong, nonatomic) id<FFChooseFriendDelegate>delegate;

@end
