//
//  JSFindFriendsViewController.m
//  Amicus
//
//  Created by Josh Sklar on 5/21/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import "JSFindFriendsViewController.h"
#import "JSButton.h"
#import "UIView+Positioning.h"
#import "FFFacebookController.h"
#import "JSInviteFriendsViewController.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "UILabel+Font.h"
#import "UIView+Animation.h"
#import "JSReachability.h"
#import "JSBackButton.h"

@interface JSFindFriendsViewController ()

@property (strong, nonatomic) UIButton *dismiss;

@end

@implementation JSFindFriendsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Find Friends";
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"home-navbar"] forBarMetrics:UIBarMetricsDefault];

    
    UIButton *b = [[JSButton alloc]initWithFrame:CGRectMake(0, 20, 250, 70)];
    [b setTitle:@"Find Friends From Facebook" forState:UIControlStateNormal];
    [b.titleLabel setAppFontBlackItalic:20 color:nil];
    [b addTarget:self action:@selector(didTapFBFriends) forControlEvents:UIControlEventTouchUpInside];
    [b centerHorizontally];
    [self.view addSubview:b];
    
    UIButton *bb = [[JSButton alloc]initWithFrame:CGRectMake(0, [b getLocationOfBottom] + 10, 250, 70)];
    [bb setTitle:@"Find Friends From Contacts" forState:UIControlStateNormal];
    [bb.titleLabel setAppFontBlackItalic:20 color:nil];
    [bb centerHorizontally];
    [bb addTarget:self action:@selector(didTapContacts) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:bb];
    
    self.dismiss = [[JSBackButton alloc]init];
    [self.dismiss addTarget:self action:@selector(didTapCancel) forControlEvents:UIControlEventTouchUpInside];
    
    [self.navigationController.navigationBar addSubview:self.dismiss];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.dismiss partialFadeWithDuration:0.2 afterDelay:0. withFinalAlpha:1. completionBlock:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.dismiss partialFadeWithDuration:0.2 afterDelay:0. withFinalAlpha:0. completionBlock:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didTapFBFriends
{
    if (![JSReachability hasInternetConnection:@"Can't invite friends"])
        return;
    
    JSInviteFriendsViewController *invite = [[JSInviteFriendsViewController alloc]initWithType:JSInviteFriendsTypeFacebook];
    [invite setFriends:[FFFacebookController getFriendsArray]];
    [self.navigationController pushViewController:invite animated:YES];
}

- (void)didTapContacts
{ // Request authorization to Address Book
    if (![JSReachability hasInternetConnection:@"Can't invite friends"])
        return;
    
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
        ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
            // First time access has been granted, add the contact
            NSArray *people = (__bridge NSArray*)ABAddressBookCopyArrayOfAllPeople(addressBookRef);
            JSInviteFriendsViewController *invite = [[JSInviteFriendsViewController alloc]initWithType:JSInviteFriendsTypeContacts];
            [invite setFriends:people];
            [self.navigationController pushViewController:invite animated:YES];

        });
    }
    else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
        // The user has previously given access, add the contact
        NSArray *people = (__bridge NSArray*)ABAddressBookCopyArrayOfAllPeople(addressBookRef);
        JSInviteFriendsViewController *invite = [[JSInviteFriendsViewController alloc]initWithType:JSInviteFriendsTypeContacts];
        [invite setFriends:people];
        [self.navigationController pushViewController:invite animated:YES];
    }
    else {
        // The user has previously denied access
        [[[UIAlertView alloc]initWithTitle:@"Error" message:@"Change privacy settings in the Settings app and allow contacts access." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        // Send an alert telling user to change privacy setting in settings app
    }
}

- (void)didTapCancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end