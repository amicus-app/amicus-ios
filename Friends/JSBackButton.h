//
//  JSBackButton.h
//  Amicus
//
//  Created by Josh Sklar on 5/29/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JSBackButton : UIButton

/* these are used in order to, say, set text to a back button */
- (void)setToBackButton;
- (void)setToNormalButton;

@end
