//
//  FFRecordCell.h
//  Amicus
//
//  Created by Josh Sklar on 5/22/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FFRecord;

@interface FFRecordCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *overallRecordValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *themVsYouValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *spHighScoreValueLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profilePicImageView;

- (void)configureLabels;
- (void)initWithRecord:(FFRecord*)r;
@end
