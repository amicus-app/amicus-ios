//
//  FFRecordViewController.m
//  Friends
//
//  Created by Josh Sklar on 4/4/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import "FFRecordViewController.h"
#import <Parse/Parse.h>
#import "FFFacebookController.h"
#import "FFUtilities.h"
#import "UIView+Animation.h"
#import <QuartzCore/QuartzCore.h>
#import "UIView+Positioning.h"
#import "UILabel+Font.h"
#import "FFRecordCell.h"
#import "FFRecord.h"
#import "FFProfilePictureCache.h"
#import "JSReachability.h"
#import "JSBackButton.h"

static const CGFloat kMyRecordLabelHeight = 70. / 2.;

@interface FFRecordViewController () <UITableViewDataSource, UITableViewDelegate>

- (void)setupMyRecordLabels;

@property (strong, nonatomic) UITableView *friendsScoresTableView;
@property (strong, nonatomic) NSMutableArray *friendsScores;
@property (strong, nonatomic) NSMutableArray *individualMeVsOpponentsScores;

@property (strong, nonatomic) NSMutableArray *records;

@property (strong, nonatomic) UILabel *highScoreLabel, *winsLabel, *lossesLabel;
@property (strong, nonatomic) UILabel *highScoreValueLabel;

@property (strong, nonatomic) UIActivityIndicatorView *loadingActivityIndicator;

// to determine when to reload the table view
@property BOOL firstDataAlreadyLoaded;

@end

@implementation FFRecordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]];
    
    // set up the toolbar
    UINavigationBar *navBar = self.navigationController.navigationBar;
    
    [navBar setBackgroundImage:[UIImage imageNamed:@"home-navbar"] forBarMetrics:UIBarMetricsDefault];
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:navBar.bounds];
    [titleLabel setText:@"Records"];
    [titleLabel setAppFontBlackItalic:24 color:[UIColor whiteColor]];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [navBar addSubview:titleLabel];
    
    UIButton *dismiss = [[JSBackButton alloc]init];
    [dismiss addTarget:self action:@selector(didTapDone) forControlEvents:UIControlEventTouchUpInside];
    
    [navBar addSubview:dismiss];
    
    self.firstDataAlreadyLoaded = NO;
    [self setupMyRecordLabels];
    
    self.friendsScores = [[NSMutableArray alloc]init];
    self.individualMeVsOpponentsScores = [[NSMutableArray alloc]init];
    self.records = [[NSMutableArray alloc]init];

    [self setupTableView];
    
    self.loadingActivityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(160, 200, 0,0)];
    [self.loadingActivityIndicator centerHorizontally];
    [self.loadingActivityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [self.loadingActivityIndicator setColor:[UIColor blueColor]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (![JSReachability hasInternetConnection:@"Can't load records"])
        return;
    
    [self.loadingActivityIndicator startAnimating];
    [self.view addSubview:self.loadingActivityIndicator];
    
    PFQuery *query = [PFQuery queryWithClassName:kPFRecordsClassName];
    [query whereKey:kPFRecordsUserIdKey equalTo:[[FFFacebookController me] valueForKey:@"id"]];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        //js assert error
        if (objects.count == 1) {
            PFObject *obj = [objects objectAtIndex:0];
            [self.highScoreValueLabel setText:[NSString stringWithFormat:@"%i", [[obj valueForKey:@"single_player_high_score"] integerValue]]];
            [self.winsLabel setText:[NSString stringWithFormat:@"Wins: %i", [[obj valueForKey:@"wins"] integerValue]]];
            [self.lossesLabel setText:[NSString stringWithFormat:@"Losses: %i", [[obj valueForKey:@"losses"] integerValue]]];
        }
        else {
            [self.highScoreValueLabel setText:@"0"];
            [self.winsLabel setText:@"Wins: 0"];
            [self.lossesLabel setText:@"Losses: 0"];
        }
    }];
    
    PFQuery *friendsRecords = [PFQuery queryWithClassName:kPFRecordsClassName];
    NSArray *friends = [FFFacebookController getFriendIdsArray];
    [friendsRecords whereKey:kPFRecordsUserIdKey containedIn:friends];
    [friendsRecords findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error)
            JSAssert(false, [error description]);
        [self.friendsScores addObjectsFromArray:objects];
        [self dataLoaded];
    }];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"player1 == %@ OR player2 == %@", [[FFFacebookController me] valueForKey:@"id"], [[FFFacebookController me] valueForKey:@"id"]];
    PFQuery *individualRecords = [PFQuery queryWithClassName:kPFGameRecordsClassName predicate:pred];
    [individualRecords findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error)
            JSAssert(false, [error description]);
        [self.individualMeVsOpponentsScores addObjectsFromArray:objects];
        [self dataLoaded];
    }];
    
}

- (void)dataLoaded
{
    if (self.firstDataAlreadyLoaded) {
        
        // make all the record objects
        for (PFObject *obj in self.friendsScores) {
            // figure out if this friend has an individual record with you
            NSString *friendId = [obj valueForKey:kPFRecordsUserIdKey];
            FFRecord *r = [[FFRecord alloc]init];
            r.fbId = friendId;
            r.name = [obj valueForKey:@"userName"];
            r.overallWins = [obj valueForKey:@"wins"];
            r.overallLosses = [obj valueForKey:@"losses"];
            r.spHighScore = [obj valueForKey:@"single_player_high_score"];
            r.theirNumWinsAgainstMe = @0;
            r.myNumWinsAgainstThem = @0;
            
            for (PFObject *indObj in self.individualMeVsOpponentsScores) {
                if (([[indObj valueForKey:@"player1"] isEqualToString:friendId] &&
                     [[indObj valueForKey:@"player2"] isEqualToString:[[FFFacebookController me] valueForKey:@"id"]])
                    ||
                     ([[indObj valueForKey:@"player2"] isEqualToString:friendId] &&
                      [[indObj valueForKey:@"player1"] isEqualToString:[[FFFacebookController me] valueForKey:@"id"]])) {
                         
                    if ([[indObj valueForKey:@"player1"] isEqualToString:friendId]) {
                        r.theirNumWinsAgainstMe = [indObj valueForKey:@"player1_score"];
                        r.myNumWinsAgainstThem = [indObj valueForKey:@"player2_score"];
                    }
                    else {
                        r.theirNumWinsAgainstMe = [indObj valueForKey:@"player2_score"];
                        r.myNumWinsAgainstThem = [indObj valueForKey:@"player1_score"];
                    }
                         break;
                }
                else {
                    r.theirNumWinsAgainstMe = @0;
                    r.myNumWinsAgainstThem = @0;
                }
                
            }
            [self.records addObject:r];
        }
        
        [self.records sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            FFRecord *r1 = (FFRecord*)obj1;
            FFRecord *r2 = (FFRecord*)obj2;
            return [r2 compare:r1];
        }];
        
        [self.loadingActivityIndicator stopAnimating];
        [self.loadingActivityIndicator removeFromSuperview];
        
        [self.friendsScoresTableView partialFadeWithDuration:0.4 afterDelay:0. withFinalAlpha:1. completionBlock:nil];
        
        if (self.records.count == 0)
            [self.friendsScoresTableView setHidden:YES];
        else
            [self.friendsScoresTableView setHidden:NO];
        
        [self.friendsScoresTableView reloadData];
        self.firstDataAlreadyLoaded = NO;
    }
    self.firstDataAlreadyLoaded = YES;
}
- (void)viewDidAppear:(BOOL)animated
{
}

- (void)didTapDone
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)setupMyRecordLabels
{
    UIImageView *iv = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 70)];
    iv.image = [UIImage imageNamed:@"scoreboard-background"];
    [self.view addSubview:iv];
    
    self.highScoreLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 240, kMyRecordLabelHeight)];
    [self.highScoreLabel setText:@"High Score (single player):"];

    self.highScoreValueLabel = [[UILabel alloc]initWithFrame:CGRectMake(245, 0, 65, kMyRecordLabelHeight)];
    [self.highScoreValueLabel configureLabelForSinglePlayerScore];

    [self.winsLabel setText:@"Wins: "];
    [self.lossesLabel setText:@"Losses: "];
    self.winsLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, kMyRecordLabelHeight, 150, kMyRecordLabelHeight)];
    self.lossesLabel = [[UILabel alloc]initWithFrame:CGRectMake(150, kMyRecordLabelHeight, 150, kMyRecordLabelHeight)];
    
    NSArray *ar = @[self.highScoreLabel, self.winsLabel, self.lossesLabel];
    for (UILabel *l in ar) {
        [l setAppFontRegularItalic:25 color:[UIColor whiteColor]];
        [l configureFontSizeWithInitialSize:25];
        l.textAlignment = NSTextAlignmentCenter;
        l.backgroundColor = [UIColor clearColor];
        [self.view addSubview:l];
    }

    [self.view addSubview:self.highScoreValueLabel];
}

- (void)setupTableView
{
    self.friendsScoresTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, [self.lossesLabel getLocationOfBottom], self.view.frame.size.width, self.view.frame.size.height - [self.lossesLabel getLocationOfBottom] - 44) style:UITableViewStylePlain];
    [self.friendsScoresTableView setDataSource:self];
    [self.friendsScoresTableView setDelegate:self];
    [self.view addSubview:self.friendsScoresTableView];
    [self.friendsScoresTableView setAlpha:0.];
    self.friendsScoresTableView.backgroundColor = [UIColor clearColor];
}

#pragma mark - UITableView data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.records.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70.;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"ffrecordCell";
    FFRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"FFRecordCell" owner:self options:nil];
        cell = [nibs objectAtIndex:0];
    }
    [cell configureLabels];
    
    FFRecord *r = [self.records objectAtIndex:indexPath.row];
    [cell initWithRecord:r];
    
    [[FFProfilePictureCache sharedInstance] getProfilePictureThumbnailForFriend:r.fbId withCompletionBlock:^(NSData *profilePictureData) {
        [cell.profilePicImageView setImage:[UIImage imageWithData:profilePictureData]];
    }];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
