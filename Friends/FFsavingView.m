//
//  FFLoadingView.m
//  Amicus
//
//  Created by Josh Sklar on 6/1/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import "FFSavingView.h"
#import "UIView+Positioning.h"
#import "UILabel+Font.h"

@interface FFSavingView ()

@property (strong, nonatomic) UIActivityIndicatorView *act;

@end

@implementation FFSavingView

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.25];
        UILabel *savingLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 100, 320, 40)];
        [savingLabel setTextAlignment:NSTextAlignmentCenter];
        [savingLabel centerHorizontally];
        [savingLabel setBackgroundColor:[UIColor clearColor]];
        [savingLabel setText:@"Loading..."];
        [savingLabel setAppFontBoldItalic:32 color:[UIColor whiteColor]];
        self.act = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(0, [savingLabel getLocationOfBottom] + 10, 50, 50)];
        [self.act setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [self.act centerHorizontally];
        [self addSubview:savingLabel];
        [self setAlpha:0.];
        [self addSubview:self.act];
    }
    return self;
}

- (void)show
{
    [self.act startAnimating];
    [self setAlpha:1.];
}

- (void)hide
{
    [self.act stopAnimating];
    [self setAlpha:0.];
}

@end
