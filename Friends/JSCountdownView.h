//
//  JSCountdownView.h
//  pie
//
//  Created by Josh Sklar on 5/15/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface JSCountdownView : UIView

// A value between 0.0 and 1.0, with 1.0 being a totally-filled circle. Defaults to 1.0.
@property (assign) float value;

// The angle of the line at 100%. Value should be in radians.
@property (assign) float angle;

// The color of the circle.
@property (strong) UIColor *color;

// The width of the border.
@property (assign) CGFloat borderWidth;

// Whether the circle should be drawn clockwise or counter-clockwise. Defaults to NO.
@property (assign, getter = isClockwise) BOOL clockwise;

@end