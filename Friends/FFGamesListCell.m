//
//  FFGamesListCell.m
//  Friends
//
//  Created by Josh Sklar on 4/2/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import "FFGamesListCell.h"
#import <QuartzCore/QuartzCore.h>
#import "UILabel+Font.h"

static const CGFloat kLabelHeight = 50.;

@interface FFGamesListCell ()

@property (strong, nonatomic) UIImageView *profilePictureImageView;
@property (strong, nonatomic) UIActivityIndicatorView *act;

@end

@implementation FFGamesListCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"picture-box"]]];
//        [self.layer setBorderWidth:2.];
//        [self.layer setBorderColor:[UIColor blackColor].CGColor];
        self.profilePictureImageView = [[UIImageView alloc]initWithFrame:CGRectMake(4, 3, 74, 74)];
        [self.contentView addSubview:self.profilePictureImageView];
        
        self.nameLabel = [[UILabel alloc] init];
        [self.nameLabel setFrame:CGRectMake(0., 78., frame.size.width, 26)];

        [self.nameLabel setNumberOfLines:2];
        [self.nameLabel setAppFontRegular:17. color:nil];
        
        [self.nameLabel setTextColor:[UIColor blackColor]];//colorWithRed:90./255. green:91./255. blue:93./255. alpha:1.]];
        [self.nameLabel setBackgroundColor:[UIColor clearColor]];
        [self.nameLabel setTextAlignment:NSTextAlignmentCenter];
        [self addSubview:self.nameLabel];
        
        self.act = [[UIActivityIndicatorView alloc]initWithFrame:self.profilePictureImageView.frame];
        [self.act setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
        [self.act setHidesWhenStopped:YES];
        [self.act startAnimating];
        [self addSubview:self.act];
        [self bringSubviewToFront:self.act];
    }
    return self;
}

- (void)setProfilePicture:(NSData*)data
{
    [self.act stopAnimating];
    [self.profilePictureImageView setImage:[UIImage imageWithData:data]];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
