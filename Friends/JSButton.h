//
//  JSButton.h
//  Amicus
//
//  Created by Josh Sklar on 4/9/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^block)(id sender);

@interface JSButton : UIButton

- (void)performBlockOnTouchUpInside:(block)b;

@end
