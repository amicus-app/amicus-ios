//
//  FFOptionButton.h
//  Friends
//
//  Created by Josh Sklar on 3/28/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FFOptionButton : UIButton

- (void)setName:(NSString*)title;
- (NSString*)name;

- (void)setBackgroundImage:(NSData*)data;
- (void)resetBackgroundImage;
- (void)flashWithCompletionBlock:(void (^)(BOOL finished))block;

- (void)setEnabled:(BOOL)enabled;

@end
