//
//  UILabel+Font.m
//  Amicus
//
//  Created by Josh Sklar on 5/15/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import "UILabel+Font.h"

@implementation UILabel (Font)

/*
 2013-05-15 19:24:48.963 Fonts[8739:c07] Family name: Brandon Grotesque
 2013-05-15 19:24:48.964 Fonts[8739:c07]     Font name: BrandonGrotesque-RegularItalic
 2013-05-15 19:24:48.964 Fonts[8739:c07]     Font name: BrandonGrotesque-MediumItalic
 2013-05-15 19:24:48.964 Fonts[8739:c07]     Font name: BrandonGrotesque-BoldItalic
 2013-05-15 19:24:48.965 Fonts[8739:c07]     Font name: BrandonGrotesque-LightItalic
 2013-05-15 19:24:48.965 Fonts[8739:c07]     Font name: BrandonGrotesque-ThinItalic
 2013-05-15 19:24:48.965 Fonts[8739:c07]     Font name: BrandonGrotesque-Regular
 2013-05-15 19:24:48.966 Fonts[8739:c07]     Font name: BrandonGrotesque-Thin
 2013-05-15 19:24:48.966 Fonts[8739:c07]     Font name: BrandonGrotesque-BlackItalic
 2013-05-15 19:24:48.967 Fonts[8739:c07]     Font name: BrandonGrotesque-Bold
 2013-05-15 19:24:48.967 Fonts[8739:c07]     Font name: BrandonGrotesque-Medium
 2013-05-15 19:24:48.983 Fonts[8739:c07]     Font name: BrandonGrotesque-Light
 2013-05-15 19:24:48.984 Fonts[8739:c07]     Font name: BrandonGrotesque-Black
 */

- (void)setAppFont:(NSString*)fontName size:(CGFloat)size color:(UIColor*)color
{
    [self setFont:[UIFont fontWithName:fontName size:size]];
    if (color)
        [self setTextColor:color];}

- (void)setAppFontRegular:(CGFloat)size color:(UIColor*)color
{
    [self setAppFont:@"BrandonGrotesque-Regular" size:size color:color];
}

- (void)setAppFontRegularItalic:(CGFloat)size color:(UIColor *)color
{
    [self setAppFont:@"BrandonGrotesque-RegularItalic" size:size color:color];
}

- (void)setAppFontBlack:(CGFloat)size  color:(UIColor*)color;
{
    [self setAppFont:@"BrandonGrotesque-Black" size:size color:color];
}

- (void)setAppFontMedium:(CGFloat)size color:(UIColor*)color
{
    [self setAppFont:@"BrandonGrotesque-Medium" size:size color:color];
}

- (void)setAppFontBlackItalic:(CGFloat)size color:(UIColor*)color
{
    [self setAppFont:@"BrandonGrotesque-BlackItalic" size:size color:color];
}

- (void)setAppFontLightItalic:(CGFloat)size color:(UIColor*)color
{
    [self setAppFont:@"BrandonGrotesque-LightItalic" size:size color:color];
}

- (void)setAppFontBoldItalic:(CGFloat)size color:(UIColor*)color
{
    [self setAppFont:@"BrandonGrotesque-BoldItalic" size:size color:color];
}

- (void)configureFontSizeWithInitialSize:(CGFloat)initialSize
{
    [self setFont:[UIFont fontWithName:self.font.fontName size:initialSize]];
    CGSize size = [self.text sizeWithFont:self.font];
    initialSize--;
    while (size.width > self.bounds.size.width) {
        [self setFont:[UIFont fontWithName:self.font.fontName size:initialSize]];
        size = [self.text sizeWithFont:self.font];
        initialSize--;
    }
}

- (void)configureLabelForSinglePlayerScore
{
//    [self setAppFontBlackItalic:22 color:[UIColor colorWithRed:31./255. green:83./255. blue:135./255. alpha:1]];
    [self setAppFontBlackItalic:22 color:[UIColor colorWithRed:95./255. green:140./255. blue:86./255. alpha:1]];
//    [self setBackgroundColor:[UIColor colorWithRed:95./255. green:140./255. blue:86./255. alpha:1]];
    [self setBackgroundColor:[UIColor whiteColor]];
    self.textAlignment = NSTextAlignmentCenter;
}

@end
