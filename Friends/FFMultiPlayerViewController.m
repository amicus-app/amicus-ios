//
//  FFMultiPlayerViewController.m
//  Friends
//
//  Created by Josh Sklar on 3/30/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import "FFMultiPlayerViewController.h"
#import "FFFacebookController.h"
#import "FFGamesListViewController.h"
#import "FFUtilities.h"
#import "FFOptionButton.h"
#import "UILabel+Font.h"
#import "UIView+Positioning.h"
#import "JSReachability.h"
#import "FFSavingView.h"

static const CGFloat kGameWinningPoints = 50;

@interface FFMultiPlayerViewController () <UIAlertViewDelegate>

@property (strong, nonatomic) UILabel *creatorsScoreLabel;
@property (strong, nonatomic) UILabel *opponentScoreLabel;
@property (strong, nonatomic) UILabel *creatorsScoreValueLabel, *opponentsScoreValueLabel;

@property (strong, nonatomic) NSString *nextPersonsTurn;
@property (strong, nonatomic) NSString *myPosition;

@property (strong, nonatomic) FFSavingView *savingView;

@end

@implementation FFMultiPlayerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.savingView = [[FFSavingView alloc]initWithFrame:self.navigationController.view.bounds];
    [self.navigationController.view addSubview:self.savingView];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.title = [self.friendDict valueForKey:@"name"];
    
    /* set the score labels */
    NSString *creatorLabelText = [self.gameObj valueForKey:kPFCreatorName];
    NSString *creatorScoreLabelText = [NSString stringWithFormat:@"%@", [self.gameObj valueForKey:@"creator_score"]];
    NSString *opponentLabelText = [self.gameObj valueForKey:kPFOpponentName];
    NSString *opponentScoreLabelText = [NSString stringWithFormat:@"%@",  [self.gameObj valueForKey:@"opponent_score"]];
    
    if ([[self.gameObj valueForKey:@"creator_score"] integerValue] > [[self.gameObj valueForKey:@"opponent_score"] integerValue]) {
        [self.creatorsScoreLabel setText:creatorLabelText];
        [self.creatorsScoreValueLabel setText:creatorScoreLabelText];
        [self.opponentScoreLabel setText:opponentLabelText];
        [self.opponentsScoreValueLabel setText:opponentScoreLabelText];
    }
    else {
        [self.creatorsScoreLabel setText:opponentLabelText];
        [self.creatorsScoreValueLabel setText:opponentScoreLabelText];
        [self.opponentScoreLabel setText:creatorLabelText];
        [self.opponentsScoreValueLabel setText:creatorScoreLabelText];
    }
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    NSArray *ar = @[self.creatorsScoreLabel, self.opponentScoreLabel];
    for (UILabel *l in ar) {
        [l setText:@""];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Virtual functions

- (void)determineMove
{
    if (![JSReachability hasInternetConnection:@"Can't play"])
        return;
    
    PFQuery *q = [PFQuery queryWithClassName:kPFGamesClassName];
    [q whereKey:@"decoy_gameID" equalTo:self.gameID];
    [q findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            PFObject *game = [objects objectAtIndex:0];
            if ([[game valueForKey:@"turn"] isEqualToString:[[FFFacebookController me]valueForKey:@"id"]]) {
                
                //                [self.questionLabel setText:@"Your turn."];
                // it's my turn, need to see if i am the creator, set the labels to say Your Turn
                if ([[game valueForKey:kPFCreatorID] isEqualToString:[[FFFacebookController me] valueForKey:@"id"]]) {
                    // I am the creator, and it is my turn
                    FFFacebookController *f = [FFFacebookController sharedInstance];
                    [f setDelegate:self];
                    [f requestMutualFriendsWith:[game valueForKey:kPFOpponentID]];
                    // fetch a question
                    self.nextPersonsTurn = [game valueForKey:kPFOpponentID];
                    self.myPosition = @"creator";
                }
                // otherwise, it's my turn and i'm not the creator, so see what the creator's question was
                else {
                    self.nextPersonsTurn = [game valueForKey:kPFCreatorID];
                    self.myPosition = @"opponent";
                    PFQuery *q = [PFQuery queryWithClassName:kPFGameTurnsClass];
                    [q whereKey:@"decoy_game_id" equalTo:self.gameID];
                    [q findObjectsInBackgroundWithBlock:^(NSArray *gameTurnsObjects, NSError *error) {
                        JSAssert(gameTurnsObjects.count == 1, @"More than 1 object returned for a given decoy_game_id");
                        if (!error) {
                            PFObject *o = [gameTurnsObjects objectAtIndex:0];
                            
                            /* determine if it's a cover photo question or a post question */
                            if ([[o valueForKey:@"question"] rangeOfString:@"Whose cover photo is this?"].location != NSNotFound || [[o valueForKey:@"question"] rangeOfString:@"Who uploaded this photo?"].location != NSNotFound) {
                                
                                NSString *sourceURL = nil;
                                NSString *question = nil;
                                if ([[o valueForKey:@"question"] rangeOfString:@"Whose cover photo is this?"].location != NSNotFound) {
                                    sourceURL = [[o valueForKey:@"question"] substringFromIndex:[[o valueForKey:@"question"] rangeOfString:@"Whose cover photo is this?"].length];
                                    question = @"Whose cover photo is this?";
                                }
                                else {
                                    sourceURL = [[o valueForKey:@"question"] substringFromIndex:[[o valueForKey:@"question"] rangeOfString:@"Who uploaded this photo?"].length];
                                    question = @"Who uploaded this photo?";
                                }
                                
                                [self didReceivePhoto:sourceURL question:question forFriend:@{@"name": [o valueForKey:@"answer"]}  withOtherFriends:[o valueForKey:@"options"]];
                            }
                            else { /* its a post question */
                                [self didRecieveRandomFriendsPost:[o valueForKey:@"question"] answer:[o valueForKey:@"answer_text"] forFriend:@{@"name": [o valueForKey:@"answer"]} withOtherFriends:[o valueForKey:@"options"]];
                            }
                        }
                        else
                            NSLog(@"%@", [error description]);
                    }];
                    // here, go fetch the creators previous question, then delete it from the table
                }
            }
            else {
                [self.loadingActivityIndicator setHidden:YES];
                self.nextPersonsTurn = [[FFFacebookController me] valueForKey:@"id"];
                if ([[game valueForKey:kPFOpponentID] isEqualToString:[[FFFacebookController me] valueForKey:@"id"]]) {
                    // i'm the opponent
                    [self.questionLabel setText:[NSString stringWithFormat:@"%@'s turn.", [game valueForKey:kPFCreatorName]]];
                    self.myPosition = @"opponent";
                }
                else {
                    [self.questionLabel setText:[NSString stringWithFormat:@"%@'s turn.", [game valueForKey:kPFOpponentName]]];
                    self.myPosition = @"creator";
                }
            }
        }
        
        else {
            NSLog(@"asdf parse error");
        }
    }];
}

- (void)processMoveWithNameSelected:(NSString*)name
{
    [self.savingView show];
    
    /* virtual function */
    if ([self.myPosition isEqualToString:@"creator"]) {
        [self storeQuestionForOpponent];
    }
    else {
        /* delete the question object */
        PFQuery *q = [PFQuery queryWithClassName:kPFGameTurnsClass];
        [q whereKey:@"decoy_game_id" equalTo:self.gameID];
        [q findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                PFObject *obj = [objects objectAtIndex:0];
                [obj deleteInBackground];
            }
            else {
                NSLog(@"%@", [error description]);
            }
        }];
    }
    
    PFQuery *query = [PFQuery queryWithClassName:kPFGamesClassName];
    [query whereKey:@"decoy_gameID" equalTo:self.gameID];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        JSAssert([objects count] == 1, @"Returns more than 1 game for a given gameID");
        if (!error) {
            BOOL wonGame = NO;
            
            PFObject *game = [objects objectAtIndex:0];
            [game setValue:self.nextPersonsTurn forKey:@"turn"];
            
            NSString *otherPlayer = nil;
            
            if ([self.myPosition isEqualToString:@"creator"]) {
                NSNumber *prevScore = [game valueForKey:@"creator_score"];
                NSNumber *newScore = prevScore;
                if ([name isEqualToString:self.answersName]) {
                    newScore = [NSNumber numberWithFloat:[prevScore floatValue] + self.timeTakenToAnswer];
                }
              
                if ([newScore floatValue] >= kGameWinningPoints)
                    wonGame = YES;
                
                otherPlayer = [game valueForKey:kPFOpponentID];
                [game setValue:newScore forKey:@"creator_score"];
            }
            else {
                NSNumber *prevScore = [game valueForKey:@"opponent_score"];
                NSNumber *newScore = prevScore;
                if ([name isEqualToString:self.answersName]) {
                    newScore = [NSNumber numberWithFloat:[prevScore floatValue] + self.timeTakenToAnswer];
                }
                
                if ([newScore floatValue] >= kGameWinningPoints)
                    wonGame = YES;
                
                otherPlayer = [game valueForKey:kPFCreatorID];
                [game setValue:newScore forKey:@"opponent_score"];
            }
            
            if (wonGame) {
                [self didWinGame:game withOtherPlayer:otherPlayer];
                return;
            }
            
            [game saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (!error) {
                    [self.home fetchGames];
                    
                    [self.savingView hide];
                    
                    [self.navigationController popViewControllerAnimated:YES];
                    
                    /* decrement the badge number - EVERY TIME other than the first move */
                    
                    if ([[self.gameObj valueForKey:kPFGameStatusKey] isEqualToString:kPFGameStatusStarted]) {
                        PFObject *obj = [self.gameObj valueForKey:@"pfObj_ptr"];
                        [obj setValue:kPFGameStatusInProgress forKey:kPFGameStatusKey];
                        [obj saveInBackground];
                    }
                    else {
                        // decrement the badge number
                        PFInstallation *currentInstallation = [PFInstallation currentInstallation];
                        
                        if(currentInstallation.badge > 0) {
                            currentInstallation.badge = currentInstallation.badge - 1;
                            [currentInstallation saveInBackground];
                        }
                    }
                    
                    NSString *pushMsg = [NSString stringWithFormat:@"%@ has taken their turn.", [[FFFacebookController me] valueForKey:@"name"]];
                    NSDictionary *pushDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                              pushMsg, @"alert",
                                              @"Increment", @"badge",
                                              @"default", @"sound",
                                              nil];
                    JSAssert(otherPlayer != nil, @"didn't send a push to anyone...");
                    NSString *formattedID = [NSString stringWithFormat:@"fb%@", otherPlayer];
                    [PFPush sendPushDataToChannelInBackground:formattedID withData:pushDict];
                }
                else {
                    JSAssert(NO, [error description]);
                }
            }];
        }
    }];
}

- (void)didWinGame:(PFObject*)game withOtherPlayer:(NSString*)otherPlayer
{
    
    // decrement the badge number
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    
    if(currentInstallation.badge > 0) {
        currentInstallation.badge = currentInstallation.badge - 1;
        [currentInstallation saveInBackground];
    }
    
    /* delete the game object, update the users Record with a win, update the other guys record with a loss, and send a push notification saying "... has won the game" */
    [game deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!succeeded)
            JSAssert(false, [error description]);
        
        [self.savingView hide];
        [self.navigationController popViewControllerAnimated:YES];
        [self.home fetchGames];
        PFQuery *recordObject = [PFQuery queryWithClassName:kPFRecordsClassName];
        [recordObject whereKey:kPFRecordsUserIdKey equalTo:[[FFFacebookController me] valueForKey:@"id"]];
        [recordObject findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            JSAssert([objects count] == 1, @"user ID object exists more than once in records table");
            PFObject *obj = [objects objectAtIndex:0];
            NSNumber *oldNumWins = [obj valueForKey:@"wins"];
            [obj setValue:[NSNumber numberWithInteger:([oldNumWins integerValue] + 1)] forKey:@"wins"];
            [obj saveInBackground];
            [[[UIAlertView alloc]initWithTitle:@"You Won" message:@"You won this game! Nice Job." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        }];
        
        PFQuery *otherPlayersRecord = [PFQuery queryWithClassName:kPFRecordsClassName];
        [otherPlayersRecord whereKey:kPFRecordsUserIdKey equalTo:otherPlayer];
        [otherPlayersRecord findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            JSAssert([objects count] == 1, @"user ID object exists more than once in records table");
            PFObject *obj = [objects objectAtIndex:0];
            NSNumber *oldNumLosses = [obj valueForKey:@"losses"];
            [obj setValue:[NSNumber numberWithInteger:([oldNumLosses integerValue] + 1)] forKey:@"losses"];
            [obj saveInBackground];
        }];
        
        // send push to the other player
        NSString *pushMsg = [NSString stringWithFormat:@"%@ has won the game.", [[FFFacebookController me] valueForKey:@"name"]];
        NSDictionary *pushDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                  pushMsg, @"alert",
                                  @"default", @"sound",
                                  nil];
        
        NSString *formattedID = [NSString stringWithFormat:@"fb%@", otherPlayer];
        [PFPush sendPushDataToChannelInBackground:formattedID withData:pushDict];
        
        /* create a game record object adding a win to this user and the other users record */
        // do a query first to see if it's there yet
        NSString *myId = [[FFFacebookController me] valueForKey:@"id"];
        NSString *predString = [NSString stringWithFormat:@"(player1=='%@' AND player2=='%@') OR (player1=='%@' AND player2=='%@')", myId, otherPlayer, otherPlayer, myId];
        NSPredicate *pred = [NSPredicate predicateWithFormat:predString];
        PFQuery *q = [PFQuery queryWithClassName:kPFGameRecordsClassName predicate:pred];
        [q findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            JSAssert(objects.count <= 1, @"When looking at the game records class, it got more than 1 object whenn looking up my game record");
            if (!error) {
                if (objects.count == 0) {
                    // create the object
                    PFObject *gameRecordObj = [PFObject objectWithClassName:kPFGameRecordsClassName];
                    [gameRecordObj setValue:[[FFFacebookController me] valueForKey:@"id"] forKey:@"player1"];
                    [gameRecordObj setValue:[[FFFacebookController me] valueForKey:@"name"] forKey:@"player1_name"];
                    [gameRecordObj setValue:otherPlayer forKey:@"player2"];
                    [gameRecordObj setValue:[FFFacebookController getFriendName:otherPlayer] forKey:@"player2_name"];
                    [gameRecordObj setValue:@1 forKey:@"player1_score"];
                    [gameRecordObj setValue:@0 forKey:@"player2_score"];
                    [gameRecordObj saveInBackground];
                }
                else if (objects.count == 1) {
                    PFObject *gameRecordObj = [objects objectAtIndex:0];
                    if ([[gameRecordObj valueForKey:@"player1"] isEqualToString:[[FFFacebookController me] valueForKey:@"id"]]) {
                        NSInteger newScore = [[gameRecordObj valueForKey:@"player1_score"] integerValue] + 1;
                        [gameRecordObj setValue:[NSNumber numberWithInteger:newScore] forKey:@"player1_score"];
                    }
                    else {
                        NSInteger newScore = [[gameRecordObj valueForKey:@"player2_score"] integerValue] + 1;
                        [gameRecordObj setValue:[NSNumber numberWithInteger:newScore] forKey:@"player2_score"];
                    }
                    [gameRecordObj saveInBackground];
                }
            }
        }];
    }];
}

- (void)storeQuestionForOpponent
{
    JSAssert(self.currentQuesiton != nil, @"self.currentQuestion is nil");
    JSAssert(self.currentFriendOptions != nil, @"self.currentFriendOptions is nil");
    JSAssert(self.answersName != nil, @"self.answers name is nil");
    
    PFObject *obj = [PFObject objectWithClassName:kPFGameTurnsClass];
    [obj setValue:self.gameID forKey:@"decoy_game_id"];
    [obj setValue:self.currentQuesiton forKey:@"question"];
    [obj setValue:self.currentAnswerText forKey:@"answer_text"];
    [obj setValue:self.currentFriendOptions forKey:@"options"];
    [obj setValue:self.answersName forKey:@"answer"];
    [obj saveInBackground];
    
    // also store it in a master questions class
    
    PFObject *question = [PFObject objectWithClassName:@"QuestionLog"];
    [question setValue:self.currentQuesiton forKey:@"question"];
    [question setValue:self.currentAnswerText forKey:@"answer_text"];
    [question setValue:self.currentFriendOptions forKey:@"options"];
    [question setValue:self.answersName forKey:@"answer"];
    [question saveInBackground];
}

- (void)dispatchSelectedOptionButton:(FFOptionButton*)btn
{
    NSString *name = btn.name;
    
    JSAssert(name != nil, @"MAKE THE BUTTONS IN THE GAME PLAY SCREEN THEIR OWN CLASS!");
    
    [self.timerForAnswer invalidate];
    self.timerForAnswer = nil;
    [self.pieTimerCountroller stopAnimation];
    
    [self animateCorrectOptionButtonWithCompletionBlock:^(BOOL finished) {
        if ([name isEqualToString:self.answersName]) {
            [[[UIAlertView alloc]initWithTitle:@"Nice Job" message:@"Answer Correct." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        }
        else {
            [[[UIAlertView alloc]initWithTitle:@"Sorry" message:@"Wrong answer." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        }
        return;
    }];
    
    [self processMoveWithNameSelected:name];
}

- (void)dispatchTimeRanOut
{
    [[[UIAlertView alloc]initWithTitle:@"Out Of Time" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Overridden functions

- (void)setupUI
{
    [super setupUI];
    
    self.creatorsScoreLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200, 35)];
    self.opponentScoreLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 35, 200, 35)];
    
    
    NSArray *ar = @[self.creatorsScoreLabel, self.opponentScoreLabel];
    for (UILabel *l in ar) {
        l.backgroundColor = [UIColor clearColor];
        [l setAppFontLightItalic:22 color:[UIColor whiteColor]];
        l.textAlignment = NSTextAlignmentCenter;
        [self.view addSubview:l];
    }
    
    self.creatorsScoreValueLabel = [[UILabel alloc]initWithFrame:CGRectMake([self.creatorsScoreLabel getLocationOfRight], 2, 31, 31)];
    [self.creatorsScoreValueLabel setBackgroundColor:[UIColor colorWithRed:95./255. green:140./255. blue:86./255. alpha:1]];
    
    self.opponentsScoreValueLabel = [[UILabel alloc]initWithFrame:CGRectMake([self.opponentScoreLabel getLocationOfRight], 2 + [self.creatorsScoreValueLabel getLocationOfBottom], 31, 31)];
    [self.opponentsScoreValueLabel setBackgroundColor:[UIColor colorWithRed:156./255. green:101./255. blue:109./255. alpha:1]];
    
    NSArray *arr = @[self.creatorsScoreValueLabel, self.opponentsScoreValueLabel];
    for (UILabel *l in arr) {
        [l setAppFontBlackItalic:22 color:[UIColor colorWithRed:31./255. green:83./255. blue:135./255. alpha:1]];
        l.textAlignment = NSTextAlignmentCenter;
        [self.view addSubview:l];
    }
}

#pragma mark - UIAlertView Delegate methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == kEndGameAlertViewTag) {
        if (buttonIndex == 1) {
            // end the round
            [self processMoveWithNameSelected:nil];
        }
    }
}

@end
