//
//  FFAppDelegate.m
//  Friends
//
//  Created by Josh Sklar on 3/21/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import "FFAppDelegate.h"
#import "FFFacebookController.h"
#import "FFFBLoginViewController.h"
#import "FFGamesListViewController.h"
#import <Parse/Parse.h>

@implementation FFAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Parse setApplicationId:@"ZQNjrq9Nbnju7xBqLjmTrUgrpBCEfRJPi4WGzPwN"
                  clientKey:@"x9NMzHmr8WbtGNetOUlYejuT8ApOf3yXwGz6PFDI"];
//    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    
    if (![def valueForKey:kDidLogInKey]) {
        FFFBLoginViewController *home = [[FFFBLoginViewController alloc] init];
        self.navController = [[UINavigationController alloc]initWithRootViewController:home];
    }
    else {
        FFGamesListViewController *home = [[FFGamesListViewController alloc]init];
        self.navController = [[UINavigationController alloc]initWithRootViewController:home];
    }
    
    if ([[UINavigationBar class] respondsToSelector:@selector(appearance)])
    {
        [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                              [UIColor whiteColor], UITextAttributeTextColor,
                                                              [UIColor blackColor], UITextAttributeTextShadowColor,
                                                              [NSValue valueWithUIOffset:UIOffsetMake(1, 0)], UITextAttributeTextShadowOffset,
                                                              [UIFont fontWithName:@"BrandonGrotesque-BoldItalic" size:24], UITextAttributeFont,
                                                              nil]];
    }
    
    self.window.rootViewController = self.navController;
    [self.window makeKeyAndVisible];
    
    // Register for push notifs
    [application registerForRemoteNotificationTypes:
     UIRemoteNotificationTypeBadge |
     UIRemoteNotificationTypeAlert |
     UIRemoteNotificationTypeSound];
    
    /* set badge number to 0 - for testing purposes */
//    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
//    if(currentInstallation.badge > 0) {
//        currentInstallation.badge = 0;
//        [currentInstallation saveInBackground];
//    }
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    
    if(currentInstallation.badge > 0) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kFFHasTurnsPending object:nil];
    }
    
    [self clearAllPushNotifs];

    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    //return [PFFacebookUtils handleOpenURL:url];
    return [[FFFacebookController sharedInstance]handleOpenURL:url];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    //return [PFFacebookUtils handleOpenURL:url];
    return [[FFFacebookController sharedInstance]handleOpenURL:url];
}

#pragma mark - Push notification methods

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)newDeviceToken
{
    // Tell Parse about the device token.
    NSLog(@"Registered for push notifications!");
    [PFPush storeDeviceToken:newDeviceToken];
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Failed to register for push notifications, with error %@",error);
}


- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kPushNotificationReceived object:nil];
    
}

- (void)clearAllPushNotifs
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}



@end
