//
//  UIView+Positioning.m
//  Amicus
//
//  Created by Josh Sklar on 5/16/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import "UIView+Positioning.h"

@implementation UIView (Positioning)

- (void)centerHorizontally
{
    CGRect oldFrame = self.frame;
    self.frame = CGRectMake(([UIScreen mainScreen].bounds.size.width - oldFrame.size
                            .width)/2,
                            oldFrame.origin.y,
                            oldFrame.size.width,
                            oldFrame.size.height);
}

- (void)centerHorizontallyWithinView:(UIView*)view
{
    CGRect oldFrame = self.frame;
    self.frame = CGRectMake((view.frame.size.width - oldFrame.size.width)/2,
                            oldFrame.origin.y,
                            oldFrame.size.width,
                            oldFrame.size.height);
}

- (CGFloat)getLocationOfBottom
{
    return self.frame.origin.y + self.frame.size.height;
}

- (CGFloat)getLocationOfRight
{
    return self.frame.origin.x + self.frame.size.width;
}

@end
