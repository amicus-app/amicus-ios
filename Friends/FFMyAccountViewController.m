//
//  FFMyAccountViewController.m
//  Amicus
//
//  Created by Josh Sklar on 5/29/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import "FFMyAccountViewController.h"
#import "UILabel+Font.h"
#import <QuartzCore/QuartzCore.h>
#import "UIView+Positioning.h"
#import "FFProfilePictureCache.h"
#import "FFFacebookController.h"
#import <Parse/Parse.h>
#import "JSReachability.h"
#import "JSBackButton.h"

@interface FFMyAccountViewController ()

@property (strong, nonatomic) UILabel *highScoreValueLabel, *winsValueLabel, *lossesValueLabel;
@property (strong, nonatomic) UIActivityIndicatorView *loadingActivityIndicator;

@end

@implementation FFMyAccountViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]];
    
    // set up the toolbar
    UINavigationBar *navBar = self.navigationController.navigationBar;
    
    [navBar setBackgroundImage:[UIImage imageNamed:@"home-navbar"] forBarMetrics:UIBarMetricsDefault];
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:navBar.bounds];
    [titleLabel setText:@"My Account"];
    [titleLabel setAppFontBlackItalic:24 color:[UIColor whiteColor]];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [navBar addSubview:titleLabel];
    
    UIButton *dismiss = [[JSBackButton alloc]init];
    [dismiss addTarget:self action:@selector(didTapDone) forControlEvents:UIControlEventTouchUpInside];
    [navBar addSubview:dismiss];
    
    [self setupView];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (![JSReachability hasInternetConnection:@"Can't load my account"])
        return;
    
    [self.loadingActivityIndicator startAnimating];
    [self.view addSubview:self.loadingActivityIndicator];
    
    PFQuery *query = [PFQuery queryWithClassName:kPFRecordsClassName];
    [query whereKey:kPFRecordsUserIdKey equalTo:[[FFFacebookController me] valueForKey:@"id"]];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        //js assert error
        [self.loadingActivityIndicator stopAnimating];
        [self.loadingActivityIndicator removeFromSuperview];
        if (objects.count == 1) {
            PFObject *obj = [objects objectAtIndex:0];
            [self.highScoreValueLabel setText:[NSString stringWithFormat:@"%i", [[obj valueForKey:@"single_player_high_score"] integerValue]]];
            [self.highScoreValueLabel configureFontSizeWithInitialSize:45];
            [self.winsValueLabel setText:[NSString stringWithFormat:@"%i", [[obj valueForKey:@"wins"] integerValue]]];
            [self.lossesValueLabel setText:[NSString stringWithFormat:@"%i", [[obj valueForKey:@"losses"] integerValue]]];
        }
        else {
            [self.highScoreValueLabel setText:@"0"];
            [self.winsValueLabel setText:@"0"];
            [self.lossesValueLabel setText:@"0"];
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didTapDone
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)setupView
{
    CGFloat xOffset = 20;
    CGFloat yOffset = 20;
    
    UIImageView *iv = [[UIImageView alloc]initWithFrame:CGRectMake(xOffset, yOffset, 50, 50)];
    [iv setImage:[UIImage imageNamed:@"fb_default_pic"]];
    [[FFProfilePictureCache sharedInstance] getProfilePictureThumbnailForFriend:[[FFFacebookController me] valueForKey:@"id"] withCompletionBlock:^(NSData *profilePictureData) {
        [iv setImage:[UIImage imageWithData:profilePictureData]];
    }];
    
    iv.layer.borderColor = [UIColor whiteColor].CGColor;
    iv.layer.borderWidth = 2.;

    
    [self.view addSubview:iv];
    
    UILabel *nameLabel = [[UILabel alloc]initWithFrame:CGRectMake([iv getLocationOfRight] + 10, yOffset, self.view.frame.size.width - xOffset - 10 - [iv getLocationOfRight], 50)];
    [nameLabel setAppFontBlack:35 color:nil];
    [nameLabel setNumberOfLines:3];
    [nameLabel setText:[[FFFacebookController me] valueForKey:@"name"]];
    
    UILabel *highScoreLabel = [[UILabel alloc]initWithFrame:CGRectMake(xOffset, [iv getLocationOfBottom] + 10, 150, 80)];
    [highScoreLabel setText:@"High Score (single player):"];
    [highScoreLabel setNumberOfLines:2];
    
    UILabel *winsLabel = [[UILabel alloc]initWithFrame:CGRectMake(xOffset, [highScoreLabel getLocationOfBottom] + 10, 150, 40)];
    UILabel *lossesLabel = [[UILabel alloc]initWithFrame:CGRectMake(xOffset, [winsLabel getLocationOfBottom] + 10, 150, 40)];
    
    [winsLabel setText:@"Wins: "];
    [lossesLabel setText:@"Losses: "];
    
    self.highScoreValueLabel = [[UILabel alloc]initWithFrame:CGRectMake([highScoreLabel getLocationOfRight] + 10, [iv getLocationOfBottom] + 10, 100, 80)];
    [self.highScoreValueLabel configureLabelForSinglePlayerScore];
    [self.view addSubview:self.highScoreValueLabel];
    
    self.winsValueLabel = [[UILabel alloc]initWithFrame:CGRectMake([winsLabel getLocationOfRight] + 10, [highScoreLabel getLocationOfBottom] + 10, 150, 40)];
    self.lossesValueLabel = [[UILabel alloc]initWithFrame:CGRectMake([lossesLabel getLocationOfRight] + 10, [winsLabel getLocationOfBottom] + 10, 150, 40)];
    
    NSArray *a = @[self.winsValueLabel, self.lossesValueLabel];
    for (UILabel *l in a) {
        [l setTextAlignment:NSTextAlignmentLeft];
        [l setBackgroundColor:[UIColor clearColor]];
        [l setAppFontRegularItalic:25 color:[UIColor whiteColor]];
        [self.view addSubview:l];
    }

    
    NSArray *ar = @[highScoreLabel, winsLabel, lossesLabel];
    for (UILabel *l in ar) {
        [l setAppFontRegularItalic:25 color:[UIColor whiteColor]];
        l.textAlignment = NSTextAlignmentRight;
        l.backgroundColor = [UIColor clearColor];
        [self.view addSubview:l];
    }
    
    NSArray *arr = @[nameLabel];
    for (UILabel *l in arr) {
        [l setBackgroundColor:[UIColor clearColor]];
        [l configureFontSizeWithInitialSize:35];
        [self.view addSubview:l];
    }
    
    UIImageView *backgroundIV = [[UIImageView alloc]initWithFrame:CGRectMake(xOffset, yOffset/2, self.view.frame.size.width - xOffset, self.view.frame.size.height)];
    [backgroundIV centerHorizontally];
    [backgroundIV setImage:[UIImage imageNamed:@"scoreboard-background"]];
    [self.view addSubview:backgroundIV];
    [self.view sendSubviewToBack:backgroundIV];
    
    self.loadingActivityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(160, 200, 0,0)];
    [self.loadingActivityIndicator centerHorizontally];
    [self.loadingActivityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [self.loadingActivityIndicator setColor:[UIColor blueColor]];
}

@end
