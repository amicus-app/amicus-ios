//
//  FFUtilities.m
//  Friends
//
//  Created by Josh Sklar on 3/28/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import "FFUtilities.h"
#import <Parse/Parse.h>

@implementation FFUtilities

void JSAssert(BOOL condition, NSString* msg)
{
    if (!condition) {
        [[[UIAlertView alloc]initWithTitle:@"Error" message:@"Something went wrong, please quit the app." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        if (msg) {
            PFObject *errObj = [PFObject objectWithClassName:@"Errors"];
            [errObj setValue:msg forKey:@"message"];
            [errObj saveInBackground];
        }
//        [[[UIAlertView alloc]initWithTitle:@"Caught Bug." message:[NSString stringWithFormat:@"%@. Please tell Josh and quit the app.", msg] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
}
/* used to be
         [[[UIAlertView alloc]initWithTitle:@"Caught Bug." message:[NSString stringWithFormat:@"%@. Please tell Josh and quit the app.", msg] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
 */
@end
