//
//  JSReachability.m
//  JSKit
//
//  Created by Josh Sklar on 1/7/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import "JSReachability.h"
#import "Reachability.h"

@interface JSReachability ()

@end

@implementation JSReachability

+ (BOOL)hasInternetConnection:(NSString*)errorMessage{
    Reachability *r = [Reachability reachabilityForInternetConnection];
    if (![r isReachable]) {
        [[[UIAlertView alloc]initWithTitle:@"No Internet Connection" message:[NSString stringWithFormat:@"%@, because there is no internet connection.", errorMessage] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        return NO;
    }
    return YES;
}

@end
