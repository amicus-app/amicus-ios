//
//  UIView+Animation.h
//  LegendsCard
//
//  Created by Josh Sklar on 7/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Animation)
-(void)partialFadeWithDuration: (NSTimeInterval) duration afterDelay: (NSTimeInterval) delay withFinalAlpha:(double)finalAlpha completionBlock:(void (^)(BOOL finished))block;

- (void)hideWithFade:(BOOL)hidden withDuration:(NSTimeInterval)duration afterDelay:(NSTimeInterval)delay;

//-(void)shakeScreen;
-(void)move:(NSString*)direction by:(NSInteger)amount withDuration:(NSTimeInterval)duration completionBlock:(void (^)(BOOL finished))block;

- (void)moveToFrame:(CGRect)frame withDuration:(NSTimeInterval)duration completionBlock:(void (^)(BOOL finished))block;

- (void)shrink:(NSInteger)amount withDuration:(NSTimeInterval)duration completionBlock:(void (^)(BOOL finished))block;
- (void)grow:(NSInteger)amount withDuration:(NSTimeInterval)duration completionBlock:(void (^)(BOOL finished))block;

- (void)blinkwithDuration:(NSTimeInterval)duration completionBlock:(void (^)(BOOL finished))block;

@end
