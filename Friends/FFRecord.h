//
//  FFRecord.h
//  Amicus
//
//  Created by Josh Sklar on 5/22/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FFRecord : NSObject

@property (strong, nonatomic) NSString *name, *fbId;
@property (strong, nonatomic) NSNumber *spHighScore;
@property (strong, nonatomic) NSNumber *overallWins, *overallLosses;
@property (strong, nonatomic) NSNumber *theirNumWinsAgainstMe, *myNumWinsAgainstThem;

- (NSComparisonResult)compare:(FFRecord*)otherRecord;

@end
