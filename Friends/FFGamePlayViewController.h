//
//  FFGamePlayViewController.h
//  Friends
//
//  Created by Josh Sklar on 3/22/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "FFFacebookController.h"
#import "JSPieTimerController.h"

typedef enum {
    WAITING,
    PLAYING
} PlayingState_e;

#define kSinglePlayerGameOverAlertViewTag 100
#define kEndGameAlertViewTag 200
#define kSinglePlayerTimeRanOutAlertViewTag 300
#define kSinglePlayerNewHighScoeAlertViewTag 400

static const CGFloat kRoundDuration = 20.;

@class FFGamesListViewController, FFOptionButton;

@interface FFGamePlayViewController : UIViewController <FFFacebookControllerDelegate>

/* UI Elements */
@property (strong, nonatomic) UILabel *timerLabel;
@property (strong, nonatomic) UILabel *questionLabel;
@property (strong, nonatomic) UILabel *answerLabel;
@property (strong, nonatomic) FFOptionButton *optionButton1, *optionButton2, *optionButton3, *optionButton4;
@property (strong, nonatomic) NSArray *optionButtons;
@property (strong, nonatomic) UIActivityIndicatorView *loadingActivityIndicator;
@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) JSPieTimerController *pieTimerCountroller;

/* game elements */
@property (strong, nonatomic) NSString *answersName;
@property (strong, nonatomic) NSString *currentQuesiton;
@property (strong, nonatomic) NSString *currentAnswerText;
@property (strong, nonatomic) NSArray *currentFriendOptions;

/* For timer */
@property CGFloat timeTakenToAnswer;
@property (strong, nonatomic) NSTimer *timerForAnswer;

@property (strong, nonatomic) FFGamesListViewController *home;

/* these are ONLY for multiplayer */
@property (strong, nonatomic) NSDictionary *friendDict;
@property (strong, nonatomic) NSString *gameID;
@property (strong, nonatomic) NSDictionary *gameObj;

/* ONLY for single player */
@property (strong, nonatomic) PFObject *singlePlayerGameObj;

/* overridden functions */
- (void)setupUI;

/* used by subclasses */
- (void)animateCorrectOptionButtonWithCompletionBlock:(void (^)(BOOL finished))block;

- (void)beginTimer;
- (void)endTimer;

@end
