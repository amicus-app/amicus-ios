//
//  FFUtilities.h
//  Friends
//
//  Created by Josh Sklar on 3/28/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FFUtilities : NSObject

void JSAssert(BOOL condition, NSString* msg);

@end
