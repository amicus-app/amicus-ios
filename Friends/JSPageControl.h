//
//  JSPageControl.h
//  Amicus
//
//  Created by Josh Sklar on 5/29/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JSPageControl : UIPageControl
{
    UIImage *activeImage;
    UIImage *inactiveImage;
}

@end
