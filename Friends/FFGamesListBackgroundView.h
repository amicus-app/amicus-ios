//
//  FFGamesListBackgroundView.h
//  Amicus
//
//  Created by Josh Sklar on 4/15/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FFGamesListViewController;

@interface FFGamesListBackgroundView : UIView

- (id)initWithFrame:(CGRect)frame andGamesList:(FFGamesListViewController*)gamesList;

@end
