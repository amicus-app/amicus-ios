//
//  FFGamesListViewController.h
//  Friends
//
//  Created by Josh Sklar on 3/22/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FFGamesListViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate>


/* for the options view controller to call */
- (void)didTapDeleteAllGames;
- (void)didTapShowRecords;
- (void)didTapShowMyAccount;

- (void)fetchGames;
- (void)fetchSinglePlayerGame;

- (void)disableSinglePlayerButton;

@end
