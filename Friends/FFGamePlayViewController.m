//
//  FFGamePlayViewController.m
//  Friends
//
//  Created by Josh Sklar on 3/22/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import "FFGamePlayViewController.h"
#import "UIView+Animation.h"
#import "FFGamesListViewController.h"
#import "FFUtilities.h"
#import "FFOptionButton.h"
#import "FFProfilePictureCache.h"
#import "JSCountdownView.h"
#import "UIView+Positioning.h"
#import <QuartzCore/QuartzCore.h>
#import "UILabel+Font.h"
#import "JSReachability.h"
#import "JSBackButton.h"

static const CGFloat kSpaceBetweenImageViewAndOptionButtons = 2.;

@interface FFGamePlayViewController () 

@property (strong, nonatomic) JSCountdownView *countdownView;
@property PlayingState_e playingState;
@property (strong, nonatomic) JSBackButton *backButton;

@end

@implementation FFGamePlayViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self setupOptionButtons];
    [self setupUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.imageView setHidden:YES];
    [self.imageView setImage:nil];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    [self.questionLabel setText:@"Loading..."];
    [self.answerLabel setText:@""];
        
    self.timeTakenToAnswer = kRoundDuration;
    [self.timerLabel setText:[NSString stringWithFormat:@"%i",(NSInteger)kRoundDuration]];
    [self.timerForAnswer invalidate];
    self.timerForAnswer = nil;
    
    [self.pieTimerCountroller reset];

    NSArray *optionButtons = @[self.optionButton1, self.optionButton2, self.optionButton3, self.optionButton4];
    for (UIButton *b in optionButtons) {
        [b setAlpha:0.];
//        [b setEnabled:YES];
    }
    
    [self.loadingActivityIndicator setHidden:NO];

    [[FFFacebookController sharedInstance] activateNetworkConnectionState];
    [self determineMove];
    
    [self.backButton setToBackButton];
    [self.backButton setHidden:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.playingState = WAITING;
    [self.backButton setToBackButton];
    [self.backButton setHidden:YES];
    
    [self.timerForAnswer invalidate];
    self.timerForAnswer = nil;
    [self.pieTimerCountroller reset];
        
    [[FFFacebookController sharedInstance] inactivateNetworkConnectionState];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    /* clean up */
    /* NOT working */
    [self.imageView setHidden:YES];
    [self.imageView setImage:nil];
    
    NSArray *optionButtons = @[self.optionButton1, self.optionButton2, self.optionButton3, self.optionButton4];
    for (FFOptionButton *b in optionButtons) {
        [b resetBackgroundImage];
    }
    
    NSArray *labels = @[self.questionLabel, self.timerLabel];
    for (UILabel *l in labels) {
        [l setText:@""];
    }
}

- (void)setupOptionButtons
{
    CGFloat kOptionButtonWidth = 83.;
    CGFloat kOptionButtonHeight = 105.;
    CGFloat viewHeight = 480. - 20. - self.navigationController.navigationBar.frame.size.height;
    if (IS_IPHONE_5) {
//        kOptionButtonSize = 100.;
        viewHeight = 568. - 20. - self.navigationController.navigationBar.frame.size.height;
    }
    
    CGFloat offset = (kSpaceBetweenImageViewAndOptionButtons * 3) + (kOptionButtonHeight * 2);
    CGFloat firstRowButtonOriginY = viewHeight - offset;
    CGFloat buttonWidthOffsetFromEdge = 40.;
    
    if (IS_IPHONE_5) {
        self.optionButton1 = [[FFOptionButton alloc]initWithFrame:CGRectMake(buttonWidthOffsetFromEdge,
                                              firstRowButtonOriginY + kSpaceBetweenImageViewAndOptionButtons,
                                              kOptionButtonWidth,
                                              kOptionButtonHeight)];
        
        self.optionButton2 = [[FFOptionButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width - kOptionButtonWidth - buttonWidthOffsetFromEdge,
                                              firstRowButtonOriginY + kSpaceBetweenImageViewAndOptionButtons,
                                              kOptionButtonWidth,
                                              kOptionButtonHeight)];
        
        self.optionButton3 = [[FFOptionButton alloc]initWithFrame:CGRectMake(buttonWidthOffsetFromEdge,
                                              firstRowButtonOriginY + kSpaceBetweenImageViewAndOptionButtons*2 + kOptionButtonHeight,
                                              kOptionButtonWidth,
                                              kOptionButtonHeight)];
        
        self.optionButton4 = [[FFOptionButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width - kOptionButtonWidth - buttonWidthOffsetFromEdge,
                                              firstRowButtonOriginY + kSpaceBetweenImageViewAndOptionButtons*2 + kOptionButtonHeight,
                                              kOptionButtonWidth,
                                              kOptionButtonHeight)];
    }
    else if (IS_IPHONE_4) {
        CGFloat buffer = (self.view.frame.size.width - (kOptionButtonWidth * 4)) / 4;
        CGFloat yPos = 480. - kOptionButtonHeight - 44 - 20 - 10;
        
        self.optionButton1 = [[FFOptionButton alloc]initWithFrame:CGRectMake(buffer,
                                                                             yPos,
                                                                             kOptionButtonWidth,
                                                                             kOptionButtonHeight)];
        
        self.optionButton2 = [[FFOptionButton alloc]initWithFrame:CGRectMake(self.optionButton1.frame.origin.x + self.optionButton1.frame.size.width + buffer,
                                                                             yPos,                                                                             kOptionButtonWidth,
                                                                             kOptionButtonHeight)];
        
        self.optionButton3 = [[FFOptionButton alloc]initWithFrame:CGRectMake(self.optionButton2.frame.origin.x + self.optionButton2.frame.size.width + buffer,
                                                                             yPos,
                                                                             kOptionButtonWidth, kOptionButtonHeight)];
        
        self.optionButton4 = [[FFOptionButton alloc]initWithFrame:CGRectMake(self.optionButton3.frame.origin.x + self.optionButton3.frame.size.width + buffer,
                                                                             yPos,                                                                                                                                                          kOptionButtonWidth, kOptionButtonHeight)];
    }
    else {
        JSAssert(NO, @"You need either an iPhone 4 or 5 for this app. Sorry.");
        return;
    }
    
    self.optionButtons = @[self.optionButton1, self.optionButton2, self.optionButton3, self.optionButton4];
    
    for (FFOptionButton *b in self.optionButtons) {
        [self.view addSubview:b];
        [b addTarget:self action:@selector(didSelectOption:) forControlEvents:UIControlEventTouchUpInside];
        [b addTarget:self action:@selector(didTouchDownOptionButton:) forControlEvents:(UIControlEventTouchDown | UIControlEventTouchDragEnter)];
        [b addTarget:self action:@selector(didDragOutsideOptionButton:) forControlEvents:UIControlEventTouchDragExit];
    }
}

#pragma mark - Overridden functions

- (void)setupUI
{
    /* override this in each MultiPlayer and SinglePlayer, and call the super */
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]];
    
    UIImageView *backgroundIv = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 70)];
    [backgroundIv setImage:[UIImage imageNamed:@"scoreboard-background"]];
    [self.view addSubview:backgroundIv];
    [self.view sendSubviewToBack:backgroundIv];
    
    CGFloat height = 217;
    UIImageView *backgroundIv2 = [[UIImageView alloc]initWithFrame:CGRectMake(0, backgroundIv.frame.origin.y + backgroundIv.frame.size.height, self.view.frame.size.width, height)];
    [backgroundIv2 setImage:[UIImage imageNamed:@"gameplay-screen-background"]];
    [self.view addSubview:backgroundIv2];
    [self.view sendSubviewToBack:backgroundIv2];
    
    
    CGRect backgroundTimerFrame = CGRectMake(255, 5, 60, 60);
    CGRect timerFrame = CGRectMake(0, 0, 45, 45);
    
    /* countdown pie timer view */
    /* create another one for the background color */
    JSCountdownView *backgroundCountdownView = [[JSCountdownView alloc]initWithFrame:backgroundTimerFrame];
    backgroundCountdownView.color = [UIColor colorWithRed:213./255. green:230./255. blue:241./255. alpha:1.];
    [backgroundCountdownView setHidden:NO];
    [self.view addSubview:backgroundCountdownView];
    
    self.countdownView = [[JSCountdownView alloc]initWithFrame:backgroundTimerFrame];
    self.countdownView.color = [UIColor colorWithRed:100.0/255.0 green:106.0/255.0 blue:108.0/255.0 alpha:1.0];
    self.countdownView.clockwise = NO;
    [self.countdownView setHidden:YES];
    [self.view addSubview:self.countdownView];
    
    
    self.pieTimerCountroller = [[JSPieTimerController alloc]init];
    self.pieTimerCountroller.countdownView = self.countdownView;
    self.pieTimerCountroller.duration = kRoundDuration;
    
    self.pieTimerCountroller.numTimesSpun = 1;
    self.pieTimerCountroller.endingPercent = 1.;

    
    /* timer label */
    /* create another circle for the background of the label */
    JSCountdownView *labelBackground = [[JSCountdownView alloc]initWithFrame:timerFrame];
    labelBackground.color = [UIColor whiteColor];
    [labelBackground setHidden:NO];
    labelBackground.center = self.countdownView.center;
    [self.view addSubview:labelBackground];
    
    self.timerLabel = [[UILabel alloc]initWithFrame:timerFrame];
    [self.timerLabel setTextAlignment:NSTextAlignmentCenter];
    self.timerLabel.backgroundColor = [UIColor clearColor];
    self.timerLabel.center = self.countdownView.center;
    [self.view addSubview:self.timerLabel];
    
    self.questionLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, backgroundIv.frame.origin.y + backgroundIv.frame.size.height, self.view.frame.size.width, 43)];
    [self.questionLabel setAppFontBlackItalic:25 color:[UIColor whiteColor]];
    [self.questionLabel configureFontSizeWithInitialSize:25];
    [self.questionLabel setTextAlignment:NSTextAlignmentCenter];
    [self.questionLabel setNumberOfLines:2];
    [self.questionLabel setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:self.questionLabel];
    
    
    self.imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0,
                                                                  [self.questionLabel getLocationOfBottom],
                                                                  280,
                                                                  170)];

    [self.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [self.imageView setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *tappedImageView = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didTapImageView)];
    [tappedImageView setNumberOfTapsRequired:1];
    [self.imageView addGestureRecognizer:tappedImageView];

    self.imageView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.imageView.layer.borderWidth = 5.;
    
    [self.imageView centerHorizontally];
    
    [self.view addSubview:self.imageView];
    self.answerLabel = [[UILabel alloc]initWithFrame:self.imageView.frame];
    [self.answerLabel setAppFontRegular:22 color:[UIColor blackColor]];
    [self.answerLabel setTextAlignment:NSTextAlignmentCenter];
    [self.answerLabel setNumberOfLines:6];
    [self.answerLabel setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:self.answerLabel];
    
    self.loadingActivityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(160, 200, 0,0)];
        [self.loadingActivityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [self.loadingActivityIndicator setColor:[UIColor blueColor]];
    [self.loadingActivityIndicator startAnimating];
    [self.view addSubview:self.loadingActivityIndicator];
    
    self.navigationItem.hidesBackButton = YES;

    self.backButton = [[JSBackButton alloc]init];
    [self.backButton addTarget:self action:@selector(didPressBackButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.backButton.titleLabel setAppFontBlackItalic:15 color:[UIColor whiteColor]];
    
    [self.navigationController.navigationBar addSubview:self.backButton];
    
    self.playingState = WAITING;
    
    [self.view bringSubviewToFront:self.imageView];
    
    /*
    UIButton *hintButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [hintButton setTitle:@"Hint" forState:UIControlStateNormal];
    [hintButton addTarget:self action:@selector(didTapHintButton) forControlEvents:UIControlEventTouchUpInside];
    hintButton.frame = CGRectMake(0, 0, 50, 50);
    [self.view addSubview:hintButton];
    */
}

- (void)didTapImageView
{
     if (self.imageView.contentMode == UIViewContentModeScaleAspectFill) {
         self.imageView.layer.borderWidth = 5.;
         [self.imageView setContentMode:UIViewContentModeScaleAspectFit];
     }
     else {
         self.imageView.layer.borderWidth = 0.;
         [self.imageView setContentMode:UIViewContentModeScaleAspectFill];
     }
}

- (void)didPressBackButton:(id)sender
{
    if (self.playingState == WAITING) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (self.playingState == PLAYING) {
        [self onEnd];
    }
}

- (void)onEnd
{
    if (![JSReachability hasInternetConnection:@"Can't play"])
        return;
    
    UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"Back" message:@"Are you sure? By tapping end, you will get this question incorrect." delegate:self cancelButtonTitle:@"Resume" otherButtonTitles:@"End", nil];
    av.tag = kEndGameAlertViewTag;
    [av show];
    // end the game, lose the current question
}

#pragma mark - Pure virtual functions

- (void)determineMove
{
    /* this is a virtual function. using a fat interface, make the implementation empty here */
}

- (void)processMoveWithNameSelected:(NSString*)name
{
    /* virtual function */
}

- (void)storeQuestionForOpponent
{
    /* virtual function */
}

- (void)dispatchSelectedOptionButton:(FFOptionButton*)btn
{
    /* virtual function */
}

- (void)dispatchTimeRanOut
{
    /* virtual function */
}

- (void)animateCorrectOptionButtonWithCompletionBlock:(void (^)(BOOL finished))block
{
    for (FFOptionButton *btn in self.optionButtons) {
        if ([btn.name isEqualToString:self.answersName]) {
            [btn flashWithCompletionBlock:^(BOOL finished) {
                block(finished);
            }];
        }
    }
}

- (void)didSelectOption:(id)sender
{
    self.playingState = WAITING;
    [self.backButton setToBackButton];
    
    FFOptionButton *btn = (FFOptionButton*)sender;
    /* disable all buttons */
    for (FFOptionButton *b in self.optionButtons) {
        [b setEnabled:NO];
    }
    
    [self dispatchSelectedOptionButton:btn];
}

- (void)didTouchDownOptionButton:(id)sender
{
    FFOptionButton *btn = (FFOptionButton*)sender;
    /* disable all buttons */
    for (FFOptionButton *b in self.optionButtons) {
        if (b != btn)
            [b setEnabled:NO];
    }
}

- (void)didDragOutsideOptionButton:(id)sender
{
    for (FFOptionButton *b in self.optionButtons) {
        [b setEnabled:YES];
    }
}

#pragma mark - FFFacebookController delegate methods

- (void)informOfWhatIsBeingFetched:(NSString *)desc
{
    [self.questionLabel setText:desc];
    [self.questionLabel configureFontSizeWithInitialSize:25];
}

- (void)didRecieveRandomFriendsPost:(NSString *)questionText answer:(NSString *)answerText forFriend:(NSDictionary *)friendInfo withOtherFriends:(NSMutableArray *)otherFriends
{
    self.playingState = PLAYING;
    
//    NSLog(@"received randomf riends post:%@", post);
    [[FFFacebookController sharedInstance] inactivateNetworkConnectionState];
    
    [self beginTimer];

    [self.loadingActivityIndicator setHidden:YES];
    
    // set up buttons
    if (otherFriends.count == 3)
        [otherFriends addObject:friendInfo];
    
    [otherFriends sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [[obj1 valueForKey:@"name"] compare:[obj2 valueForKey:@"name"] options:NSCaseInsensitiveSearch];
    }];
    
 
    [self initOptionButtons:otherFriends];

    for (FFOptionButton *btn in self.optionButtons) {
        [btn setEnabled:YES];
    }

    [self.questionLabel setText:questionText];

    [self.answerLabel setText:answerText];
    
    /* replace their name with a '???' */
    NSRange rangeOfSubstring = [questionText rangeOfString:[friendInfo valueForKey:@"name"]];
    if(rangeOfSubstring.location == NSNotFound) {
        // their name isn't in the post
    }
    else {
        questionText = [questionText stringByReplacingOccurrencesOfString:[friendInfo valueForKey:@"name"] withString:@"???"];
        [self.questionLabel setText:@"Fill in the '???'."];
        [self.answerLabel setText:questionText];
    }
    [self.questionLabel configureFontSizeWithInitialSize:25];

    
    self.answersName = [friendInfo valueForKey:@"name"];
    
    [self.imageView setHidden:YES];
    [self.answerLabel setHidden:NO];

    self.currentQuesiton = self.questionLabel.text;
    self.currentAnswerText = self.answerLabel.text;
    self.currentFriendOptions = otherFriends;
}

- (void)didReceivePhoto:(NSString *)photoURL question:(NSString *)question forFriend:(NSDictionary *)friendInfo withOtherFriends:(NSMutableArray *)otherFriends
{
    self.playingState = PLAYING;
    
    NSLog(@"received photo: %@", friendInfo);
    [[FFFacebookController sharedInstance] inactivateNetworkConnectionState];
    
//    [self.optionButton1 partialFadeWithDuration:0.2 afterDelay:0 withFinalAlpha:1. completionBlock:nil];
//    [self.optionButton2 partialFadeWithDuration:0.2 afterDelay:0 withFinalAlpha:1. completionBlock:nil];
//    [self.optionButton3 partialFadeWithDuration:0.2 afterDelay:0 withFinalAlpha:1. completionBlock:nil];
//    [self.optionButton4 partialFadeWithDuration:0.2 afterDelay:0 withFinalAlpha:1. completionBlock:nil];
    
    // set up buttons
    if (otherFriends.count == 3)
        [otherFriends addObject:friendInfo];
    
    [otherFriends sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [[obj1 valueForKey:@"name"] compare:[obj2 valueForKey:@"name"] options:NSCaseInsensitiveSearch];
    }];
    
    [self initOptionButtons:otherFriends];
    for (FFOptionButton *b in self.optionButtons) {
        [b setEnabled:NO];
    }
    
    /* make the question label shorter - do this for answer label */
//    self.questionLabel.frame = CGRectMake(25, 40, 265, 50);
    [self.questionLabel setText:question];
    [self.questionLabel configureFontSizeWithInitialSize:25];

    
    self.answersName = [friendInfo valueForKey:@"name"];
    
    self.currentQuesiton = [NSString stringWithFormat:@"%@%@", question, photoURL];
    self.currentAnswerText = @"";
    self.currentFriendOptions = otherFriends;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSData *photoData = [NSData dataWithContentsOfURL:[NSURL URLWithString:photoURL]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self beginTimer];
            
            [self.loadingActivityIndicator setHidden:YES];
            for (FFOptionButton *btn in self.optionButtons) {
                [btn setEnabled:YES];
            }
            
            self.imageView.layer.borderWidth = 5.;
            [self.imageView setContentMode:UIViewContentModeScaleAspectFit];
            [self.imageView setHidden:NO];
            [self.answerLabel setHidden:YES];
            
            [self.imageView setImage:[UIImage imageWithData:photoData]];
        });
    });
}

- (void)didReceiveRandomFriendsLike:(NSString *)questionText answer:(NSString *)answerText forFriend:(NSDictionary *)friendInfo withOtherFriends:(NSMutableArray *)otherFriends
{
    [self didRecieveRandomFriendsPost:questionText answer:answerText forFriend:friendInfo withOtherFriends:otherFriends];
}

- (void)informUserOfFriendFetchingData:(NSString *)friendName
{
    [self.questionLabel setText:[NSString stringWithFormat:@"Looking at %@'s feed...", friendName]];
}

- (void)facebookRequestDidFail
{
    
}

- (void)timerForAnswerFired
{
    self.timeTakenToAnswer -= 1;
    if (self.timeTakenToAnswer == 0.) {
        
        [self processMoveWithNameSelected:@""];
        [self.timerForAnswer invalidate];
        self.timerForAnswer = nil;
        
        for (FFOptionButton *b in self.optionButtons) {
            [b setEnabled:NO];
        }
        
        [self animateCorrectOptionButtonWithCompletionBlock:^(BOOL finished) {
            [self dispatchTimeRanOut];
        }];
    }
    
    [self.timerLabel setText:[NSString stringWithFormat:@"%i", (int)self.timeTakenToAnswer]];
}

- (void)beginTimer
{
    // start the timer
    self.timerForAnswer = [NSTimer scheduledTimerWithTimeInterval:1. target:self selector:@selector(timerForAnswerFired) userInfo:nil repeats:YES];
    [self.countdownView setHidden:NO];
    if (!self.pieTimerCountroller.isAnimating)
        [self.pieTimerCountroller startAnimation];
}

- (void)endTimer
{
    
}

- (void)initOptionButtons:(NSMutableArray*)otherFriends
{
    // should be 4 friends
    JSAssert(otherFriends.count == 4, @"Not 4 other friends");
    
    NSArray *optionButtonsInfo = @[ @[self.optionButton1, [otherFriends objectAtIndex:0]],
                                    @[self.optionButton2, [otherFriends objectAtIndex:1]],
                                    @[self.optionButton3, [otherFriends objectAtIndex:2]],
                                    @[self.optionButton4, [otherFriends objectAtIndex:3]]];
    
    for (NSArray *optionButtonInfo in optionButtonsInfo) {
        FFOptionButton *b = [optionButtonInfo objectAtIndex:0];
        NSDictionary *friendInfo = [optionButtonInfo objectAtIndex:1];
        
        [b setName:[friendInfo valueForKey:@"name"]];
        
        /* get their picture */
        [[FFProfilePictureCache sharedInstance] getProfilePictureForFriend:[friendInfo valueForKey:@"id"] withCompletionBlock:^(NSData *profilePictureData) {
            [b setBackgroundImage:profilePictureData];
        }];
    }
}

- (void)didTapHintButton
{
    /*
    NSArray *btns = @[self.optionButton1, self.optionButton2, self.optionButton3, self.optionButton4];
    int count = 0;
    for (UIButton *b in btns) {
        if (count == 2)
            break;
        for (UIView *subview in [b subviews]) {
            if (subview.tag == 1000) {
                UILabel *label = (UILabel*)subview;
                if (![label.text isEqualToString:self.answersName]) {
                    [b partialFadeWithDuration:0.2 afterDelay:0. withFinalAlpha:0.3 completionBlock:nil];
                    [b setEnabled:NO];
                    count++;
                }
            }
        }
    }
     */
}

@end
