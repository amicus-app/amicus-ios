//
//  FFProfilePictureCache.h
//  Friends
//
//  Created by Josh Sklar on 4/2/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FFProfilePictureCache : NSObject

+ (id)sharedInstance;
- (void)getProfilePictureForFriend:(NSString*)friendId withCompletionBlock:(void (^)(NSData *profilePictureData))block;
- (void)getProfilePictureThumbnailForFriend:(NSString*)friendId withCompletionBlock:(void (^)(NSData *profilePictureData))block;

@end
