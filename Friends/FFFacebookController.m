//
//  FFFacebookController
//  Amicus
//
//  Created by Josh Sklar on 5/18/12.
//  Copyright (c) 2012 Sklar. All rights reserved.
//

#import "FFFacebookController.h"
#import <Parse/Parse.h>
#import "FFUtilities.h"

#define kQuestionArraySize 26

#define kFBAppId @"401114983342617"

typedef enum {
    COVER_PHOTO = 0,
    POST,
    BIRTHDAY,
    STATUS,
    LIKE,
    UPLOADED_PHOTO
} QuestionType_e;

static NSString *kMeKey = @"me";
/* 
 The questions are weighted as follows:
 cover photo    30%
 like           20%
 uploaded photo 20%
 status         20%
 post message   5%
 birthday       5%
 */
static int kQuestionArray[kQuestionArraySize] = {COVER_PHOTO, COVER_PHOTO, COVER_PHOTO, COVER_PHOTO, COVER_PHOTO, COVER_PHOTO,
    LIKE, LIKE, LIKE, LIKE,
    UPLOADED_PHOTO, UPLOADED_PHOTO, UPLOADED_PHOTO, UPLOADED_PHOTO,
    STATUS, STATUS, STATUS, STATUS,
    POST, POST,
    BIRTHDAY, BIRTHDAY};

typedef enum {
	NETWORK_CONNECTION_ACTIVE,
    NETWORK_CONNECTION_INACTIVE
} NetworkConnectionState_e;

@implementation FFFacebookController
{
    Facebook *facebook;
    
    NetworkConnectionState_e networkConnectionState;

    NSString *currentMutualFriendsID;
    NSDictionary *currentFriendWhoseFeedPostsViewing;
    NSString *currentStoryID;
    NSMutableArray *currentRandomMutialFriends;
    NSString *currentCoverPhotoURLSuffix;
    NSString *currentPhotoURLSuffix;
    NSString *currentFriendsBirthdayViewing;
    NSString *currentFriendWhoseStatusViewing;
    NSString *currentFriendWhoseLikesViewing;
    
    int questionArrayIndexer;
}

@synthesize delegate;

- (id)init
{
    self = [super init];
    if (self) {
        facebook = [[Facebook alloc] initWithAppId:kFBAppId andDelegate:self];
        [facebook extendAccessTokenIfNeeded];
//        [FBSession setActiveSession:facebook.session];
//        FBSession *session = [[FBSession alloc]initWithAppID:kFBAppId
//                                                 permissions:@[@"email", @"friends_status", @"friends_birthday", @"friends_activities", @"friends_photos", @"share_item", @"publish_actions"]
//                                             defaultAudience:FBSessionDefaultAudienceEveryone
//                                             urlSchemeSuffix:nil
//                                          tokenCacheStrategy:nil];
//        [FBSession setActiveSession:session];
        
        networkConnectionState = NETWORK_CONNECTION_ACTIVE;
        
        currentRandomMutialFriends = [[NSMutableArray alloc]init];
        currentStoryID = [[NSString alloc]init];
        currentCoverPhotoURLSuffix = [[NSString alloc]init];
        currentPhotoURLSuffix = [[NSString alloc]init];
        
        questionArrayIndexer = 0;
        shuffle(kQuestionArray, kQuestionArraySize);

        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if ([defaults objectForKey:@"FBAccessTokenKey"] 
            && [defaults objectForKey:@"FBExpirationDateKey"]) {
            facebook.accessToken = [defaults objectForKey:@"FBAccessTokenKey"];
            facebook.expirationDate = [defaults objectForKey:@"FBExpirationDateKey"];
            
        }
        
        if (![facebook isSessionValid]) {

        }
        
        //add init code
    }
    return self;
    
}

+ (id)sharedInstance 
{
    static id sharedInstance = nil;
    if (sharedInstance == nil) {
        sharedInstance = [[self alloc] init];
    }
    return sharedInstance;
}

+ (NSDictionary*)me
{
    NSDictionary *me = [[NSUserDefaults standardUserDefaults] valueForKey:kMeKey];
    return me;
}

- (void)inactivateNetworkConnectionState
{
    networkConnectionState = NETWORK_CONNECTION_INACTIVE;
}

- (void)activateNetworkConnectionState
{
    networkConnectionState = NETWORK_CONNECTION_ACTIVE;
}

+ (NSArray*)getFriendsArray
{
    JSAssert([[NSUserDefaults standardUserDefaults] valueForKey:kFriendsArrayKey] != nil, @"Tried to get the friends array without it being fetched before");
   return [[NSUserDefaults standardUserDefaults] valueForKey:kFriendsArrayKey];
}

+ (NSArray*)getFriendIdsArray
{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"friendsIdsArrayKey"])
        return [[NSUserDefaults standardUserDefaults] valueForKey:@"friendsIdsArrayKey"];
    else {
        NSMutableArray *friendIdsArray = [[NSMutableArray alloc]initWithCapacity:[FFFacebookController getFriendsArray].count];
        for (NSDictionary *f in [FFFacebookController getFriendsArray]) {
            [friendIdsArray addObject:[f valueForKey:@"id"]];
        }
        NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
        [def setValue:friendIdsArray forKey:@"friendsIdsArrayKey"];
        [def synchronize];
        return friendIdsArray;
    }
}

+ (NSString*)getFriendName:(NSString*)friendId
{
    NSArray *friends = [[NSUserDefaults standardUserDefaults] valueForKey:kFriendsArrayKey];
    for (NSDictionary *friend in friends) {
        if ([[friend valueForKey:@"id"] isEqualToString:friendId])
            return [friend valueForKey:@"name"];
    }
    JSAssert(false, @"shouldn't ever get here!");
    return @"";
}

- (void)postToFacebook:(NSString*)msg
{
    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   kFBAppId, @"app_id",
                                   @"https://www.facebook.com/playamicus", @"link",
//                                   @"http://fbrell.com/f8.jpg", @"picture",
                                   @"Amicus", @"name",
                                   @"Amicus iOS App", @"caption",
                                   msg, @"description",
                                   nil];
    
    [facebook dialog:@"feed" andParams:params andDelegate:self];
}

- (void)postToFriendsWall:(NSDictionary*)friendDict message:(NSString*)msg
{
    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   kFBAppId, @"app_id",
                                   @"https://www.facebook.com/playamicus", @"link",
                                   //                                   @"http://fbrell.com/f8.jpg", @"picture",
                                   @"Amicus", @"name",
                                   @"Let's play Amicus!", @"caption",
                                   @"I'd like to start a game of Amicus with you. Click the link above to download the game.", @"description",
                                   [friendDict valueForKey:@"id"], @"to",
                                   nil];
    
    [facebook dialog:@"feed" andParams:params andDelegate:self];}

- (void)login
{
    [self activateNetworkConnectionState];
    [facebook authorize:@[@"email", @"friends_status", @"friends_birthday", @"friends_activities", @"friends_photos", @"share_item", @"publish_actions"]];
    if ([facebook isSessionValid])
        NSLog(@"Session is valud");
    else
        NSLog(@"Session isn't valid");
}

- (void)fbDidLogin
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[facebook accessToken] forKey:@"FBAccessTokenKey"];
    [defaults setObject:[facebook expirationDate] forKey:@"FBExpirationDateKey"];
    [defaults synchronize];

    [facebook requestWithGraphPath:@"me" andDelegate:self];
    [facebook requestWithGraphPath:@"me/friends" andDelegate:self];
}

- (void)requestFriends
{
//    FFFBRequest *req = [[FFFBRequest alloc]initWithFacebook:facebook];
//    [req performRequestWithPath:@"me" andCompletionBlock:^(id sender) {
//        NSLog(@"hi");
//    }];
    
    [facebook requestWithGraphPath:@"me/friends" andDelegate:self];
}

- (void) requestMutualFriendsWith:(NSString*)friendID
{
    if (networkConnectionState == NETWORK_CONNECTION_INACTIVE)
        return;
    
    currentMutualFriendsID = friendID;
    NSString *path = [NSString stringWithFormat:@"me/mutualfriends/%@", friendID];
    [facebook requestWithGraphPath:path andDelegate:self];
}

- (void)requestFriendsFeedPosts:(NSDictionary*)friend
{
    currentFriendWhoseFeedPostsViewing = friend;
    
    if (questionArrayIndexer == kQuestionArraySize) {
        questionArrayIndexer = 0;
        shuffle(kQuestionArray, kQuestionArraySize);
    }
    
    int questionType = kQuestionArray[questionArrayIndexer];
    questionArrayIndexer++;
    
    if (questionType == COVER_PHOTO) {
        // do a cover photo request
        NSString *path = [NSString stringWithFormat:@"%@?fields=cover", [friend valueForKey:@"id"]];
        if ([self.delegate respondsToSelector:@selector(informOfWhatIsBeingFetched:)])
            [self.delegate informOfWhatIsBeingFetched:@"Fetching a friend's cover photo..."];
        currentCoverPhotoURLSuffix = path;
        [facebook requestWithGraphPath:path andDelegate:self];
        return;
    }
    else if (questionType == UPLOADED_PHOTO) {
        if ([self.delegate respondsToSelector:@selector(informOfWhatIsBeingFetched:)])
            [self.delegate informOfWhatIsBeingFetched:@"Fetching a friend's uploaded photo..."];
        NSString *path = [NSString stringWithFormat:@"%@/photos?fields=source&limit=1", [friend valueForKey:@"id"]];
        currentPhotoURLSuffix = path;
        [facebook requestWithGraphPath:path andDelegate:self];
        return;
    }
    else if (questionType == POST) {
        if ([self.delegate respondsToSelector:@selector(informOfWhatIsBeingFetched:)])
            [self.delegate informOfWhatIsBeingFetched:@"Fetching a friend's post..."];
        // do a feed request
        NSString *path = [NSString stringWithFormat:@"%@/feed?fields=name", [friend valueForKey:@"id"]];
        [facebook requestWithGraphPath:path andDelegate:self];
    }
    else if (questionType == BIRTHDAY) {
        if ([self.delegate respondsToSelector:@selector(informOfWhatIsBeingFetched:)])
            [self.delegate informOfWhatIsBeingFetched:@"Fetching a friend's birthday..."];
        // do a birthday request
        currentFriendsBirthdayViewing = [friend valueForKey:@"id"];
        NSString *path = [NSString stringWithFormat:@"%@?fields=birthday", [friend valueForKey:@"id"]];
        [facebook requestWithGraphPath:path andDelegate:self];
    }
    else if (questionType == STATUS) {
        if ([self.delegate respondsToSelector:@selector(informOfWhatIsBeingFetched:)])
            [self.delegate informOfWhatIsBeingFetched:@"Fetching a friend's status update..."];
        // do a status request
        currentFriendWhoseStatusViewing = [friend valueForKey:@"id"];
        NSString *path = [NSString stringWithFormat:@"%@/statuses", [friend valueForKey:@"id"]];
        [facebook requestWithGraphPath:path andDelegate:self];
    }
    else if (questionType == LIKE) {
        if ([self.delegate respondsToSelector:@selector(informOfWhatIsBeingFetched:)])
            [self.delegate informOfWhatIsBeingFetched:@"Fetching a friend's like..."];
        // do a likes request
        currentFriendWhoseLikesViewing = [friend valueForKey:@"id"];
        NSString *path = [NSString stringWithFormat:@"%@/likes", [friend valueForKey:@"id"]];
        [facebook requestWithGraphPath:path andDelegate:self];
    }
    else {
        JSAssert(NO, @"rand%3 returned something other than 0, 1, 2");
    }
    
    // or also id?fields=username
    // or also id?fields=posts
}

- (void)requestStoryInformation:(NSString*)storyID andFriend:(NSDictionary*)friend
{
    currentStoryID = storyID;
    NSString *path = storyID;
    [facebook requestWithGraphPath:path andDelegate:self];
}


- (void)request:(FBRequest *)request didLoad:(id)result
{
    if (networkConnectionState == NETWORK_CONNECTION_INACTIVE)
        return;
    
    if ([request.graphPath hasSuffix:@"me"]) {
        if ([result isKindOfClass:[NSDictionary class]]) {
            if (![result objectForKey:@"data"]) {
                NSArray *objs = @[[result objectForKey:@"name"], [result objectForKey:@"id"]];
                NSArray *keys = @[@"name", @"id"];
                NSDictionary *dict = [NSDictionary dictionaryWithObjects:objs forKeys:keys];
                [[NSUserDefaults standardUserDefaults] setValue:dict forKey:kMeKey];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSString *formattedID = [NSString stringWithFormat:@"fb%@", [result valueForKey:@"id"]];
                [PFPush subscribeToChannelInBackground:formattedID];
                
                if (![dict valueForKey:@"name"] || ![dict valueForKey:@"id"]) {
                    // something went wrong, say the login cancelled
                    [self inactivateNetworkConnectionState];
                    [self fbDidNotLogin:NO];
                    return;
                }
                
                [self saveUserInfoToParse:[dict valueForKey:@"name"] withId:[dict valueForKey:@"id"]];
        
//                /* initialize the Record objet. first, make sure none have already been created with the given id */
//                PFQuery *q = [PFQuery queryWithClassName:kPFRecordsClassName];
//                [q whereKey:kPFRecordsUserIdKey equalTo:[result objectForKey:@"id"]];
//                [q findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
//                    if (!error) {
//                        if (objects.count == 0) {
//                            PFObject *recordObj = [PFObject objectWithClassName:kPFRecordsClassName];
//                            [recordObj setValue:[result objectForKey:@"id"] forKey:kPFRecordsUserIdKey];
//                            [recordObj setValue:[result objectForKey:@"name"] forKey:@"userName"];
//                            [recordObj setValue:@0 forKey:@"wins"];
//                            [recordObj setValue:@0 forKey:@"losses"];
//                            [recordObj setValue:@0 forKey:@"single_player_high_score"];
//                            [recordObj saveInBackground];
//                        }
//                    }
//                    else {
//                        NSLog(@"FFFacebookConroller:request didLoad, %@", [error description]);
//                    }
//                }];
                
                if ([delegate respondsToSelector:@selector(didLogin)]) {
                    [delegate didLogin];
                }
                
                return;
            }
        }
    }
    
    if ([request.graphPath hasSuffix:@"me/friends"]) {
        if ([result isKindOfClass:[NSDictionary class]]) {
            
            id data = [result valueForKey:@"data"];
            if ([data isKindOfClass:[NSArray class]]) {
                [[NSUserDefaults standardUserDefaults] setValue:data forKey:kFriendsArrayKey];
                // clear the friendId's array
                [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"friendsIdsArrayKey"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
				if ([delegate respondsToSelector:@selector(didReceiveFriends:)]) {
					[delegate didReceiveFriends:data];
                }
            }
        }
        return;
    }

    NSString *mutualFriendsSuffix = nil;
    if (currentMutualFriendsID)
        mutualFriendsSuffix = [NSString stringWithFormat:@"me/mutualfriends/%@", currentMutualFriendsID];
    else
        mutualFriendsSuffix = @"blah";
    
    NSString *feedPostsSuffix = nil;
    if (currentFriendWhoseFeedPostsViewing)
        feedPostsSuffix = [NSString stringWithFormat:@"%@/feed?fields=name", [currentFriendWhoseFeedPostsViewing valueForKey:@"id"]];
    else
        feedPostsSuffix = @"blah";
    
    NSString *birthdaySuffix = nil;
    if (currentFriendsBirthdayViewing)
        birthdaySuffix = [NSString stringWithFormat:@"%@?fields=birthday", currentFriendsBirthdayViewing];
    else
        birthdaySuffix = @"blah";
    
    NSString *statusSuffix = nil;
    if (currentFriendWhoseStatusViewing)
        statusSuffix = [NSString stringWithFormat:@"%@/statuses", currentFriendWhoseStatusViewing];
    else
        statusSuffix = @"blah";
    
    NSString *likesSuffix = nil;
    if (currentFriendWhoseLikesViewing)
        likesSuffix = [NSString stringWithFormat:@"%@/likes", currentFriendWhoseLikesViewing];
    else
        likesSuffix = @"blah";

    if ([request.graphPath hasSuffix:mutualFriendsSuffix]) {
        if ([result isKindOfClass:[NSDictionary class]]) {
            id data = [result valueForKey:@"data"];
            if ([data isKindOfClass:[NSArray class]]) {
                NSArray *friends = (NSArray*)data;
                NSInteger randomInt = arc4random() % friends.count;
                NSDictionary *randomFriend = [friends objectAtIndex:randomInt];
                
                NSInteger randomInt1 = arc4random() % friends.count;
                while (randomInt1 == randomInt) {
                    randomInt1 = arc4random() & friends.count;
                }
                
                NSInteger randomInt2 = arc4random() % friends.count;
                while (randomInt2 == randomInt1 || randomInt2 == randomInt) {
                    randomInt2 = arc4random() % friends.count;
                }
                
                NSInteger randomInt3 = arc4random() % friends.count;
                while (randomInt3 == randomInt2 || randomInt3 == randomInt1 || randomInt3 == randomInt) {
                    randomInt3 = arc4random() % friends.count;
                }
                
                // get 3 friends
                [currentRandomMutialFriends removeAllObjects];
                [currentRandomMutialFriends addObject:[friends objectAtIndex:randomInt1]];
                [currentRandomMutialFriends addObject:[friends objectAtIndex:randomInt2]];
                [currentRandomMutialFriends addObject:[friends objectAtIndex:randomInt3]];
                
//                if ([self.delegate respondsToSelector:@selector(informUserOfFriendFetchingData:)])
//                    [self.delegate informUserOfFriendFetchingData:[randomFriend objectForKey:@"name"]];
                
                NSLog(@"%@", [randomFriend objectForKey:@"name"]);
                [self requestFriendsFeedPosts:randomFriend];
            }
        }
        return;
    }
    else if ([request.graphPath hasSuffix:feedPostsSuffix]) {
        if ([result isKindOfClass:[NSDictionary class]]) {
            id data = [result valueForKey:@"data"];
            if ([data isKindOfClass:[NSArray class]]) {
                if ([data count] == 0) {
                    // need to do it again with a different friend. find your mutual friends and then get a random one
                    [self requestMutualFriendsWith:currentMutualFriendsID];
                }
                else {
                    NSLog(@"%@", currentMutualFriendsID);
//                    NSLog(@"%@",data);
                    NSDictionary *firstStory = [data objectAtIndex:0];
                    NSString *storyID = [firstStory objectForKey:@"id"];
                    
                    [self requestStoryInformation:storyID andFriend:currentFriendWhoseFeedPostsViewing];
                    
                }
            }
        }
        return;
    }
    else if ([request.graphPath hasSuffix:currentStoryID]) {
        if ([result isKindOfClass:[NSDictionary class]]) {
            NSString *story = nil;
            if ([result valueForKey:@"story"]) {
                story = [result valueForKey:@"story"];
            }
            else if ([result valueForKey:@"name"]) {
                story = [result valueForKey:@"name"];
            }
            else if ([result valueForKey:@"message"]) {
                story = [result valueForKey:@"message"];
            }
            
            // check to make sure it's not "likes a photo", "are now friends", or any number of those.
            /*
            if ([story rangeOfString:@"likes a photo"].location != NSNotFound || [story rangeOfString:@"are now friends"].location != NSNotFound || [story rangeOfString:@"likes a post"].location != NSNotFound || [story rangeOfString:@"updated his cover photo"].location != NSNotFound || [story rangeOfString:@"updated her cover photo"].location != NSNotFound) {
                // need to do it again with a different friend. find your mutual friends and then get a random one
                [self requestMutualFriendsWith:currentMutualFriendsID];
                return;
            }
             */
            
            // take out "likes a photo", "likes a post", or any number of those types of posts
            if ([story rangeOfString:@"likes a photo"].location != NSNotFound || [story rangeOfString:@"likes a post"].location != NSNotFound || [story rangeOfString:@"likes a link"].location != NSNotFound || 
                [story rangeOfString:@"updated his cover photo"].location != NSNotFound || [story rangeOfString:@"updated her cover photo"].location != NSNotFound || [story rangeOfString:@"likes a status"].location != NSNotFound) {
                [self requestMutualFriendsWith:currentMutualFriendsID];
                return;
            }
            
            JSAssert(story != nil, @"story did not have a story, name, or message field field");
            
            /* check if it's a likes a photo post */
            if ([story rangeOfString:@"likes a photo"].location != NSNotFound) {
                JSAssert(NO, @"Got into the liked photo if statement");
                return;
                if ([self.delegate respondsToSelector:@selector(didReceiveRandomFriendsLikedPhotoInfo:forFriend:withOtherFriends:)]) {
                    JSAssert([currentRandomMutialFriends count] > 0, @"Current random mutual friends got messed up somewhere");
                    
                    NSString *photoURL = nil;
                    [self.delegate didReceiveRandomFriendsLikedPhotoInfo:photoURL forFriend:currentFriendWhoseFeedPostsViewing withOtherFriends:currentRandomMutialFriends];
                    
                    return;
                }
            }
            if ([self.delegate respondsToSelector:@selector(didRecieveRandomFriendsPost:answer:forFriend:withOtherFriends:)]) {
                JSAssert([currentRandomMutialFriends count] > 0, @"Current random mutual friends got messed up somewhere");
                
                [self.delegate didRecieveRandomFriendsPost:story answer:@"" forFriend:currentFriendWhoseFeedPostsViewing withOtherFriends:currentRandomMutialFriends];
            }

        }
        return;
    }
    else if ([request.graphPath hasSuffix:currentCoverPhotoURLSuffix]) {
        if ([result isKindOfClass:[NSDictionary class]]) {
            // this guy doesn't have a cover photo
            if (![result valueForKey:@"cover"]) {
                [self requestMutualFriendsWith:currentMutualFriendsID];
                return;
            }
            
            NSString *sourceURL = [[result valueForKey:@"cover"] valueForKey:@"source"];
            if ([self.delegate respondsToSelector:@selector(didReceivePhoto:question:forFriend:withOtherFriends:)]) {
                JSAssert([currentRandomMutialFriends count] > 0, @"Current random mutual friends got messed up somewhere");
                [self.delegate didReceivePhoto:sourceURL question:@"Whose cover photo is this?" forFriend:currentFriendWhoseFeedPostsViewing withOtherFriends:currentRandomMutialFriends];
            }
        }
    }
    else if ([request.graphPath hasSuffix:birthdaySuffix]) {
        if ([result isKindOfClass:[NSDictionary class]]) {

            if (![result valueForKey:@"birthday"]) {
                [self requestMutualFriendsWith:currentMutualFriendsID];
                return;
            }
            
            NSString *birthday = [result valueForKey:@"birthday"];
            
            if ([self.delegate respondsToSelector:@selector(didRecieveRandomFriendsPost:answer:forFriend:withOtherFriends:)]) {
                [self.delegate didRecieveRandomFriendsPost:@"Whose birthday is this?" answer:birthday forFriend:currentFriendWhoseFeedPostsViewing withOtherFriends:currentRandomMutialFriends];
            }
            
        }
        return;
    }
    else if ([request.graphPath hasSuffix:statusSuffix]) {
        if ([result isKindOfClass:[NSDictionary class]]) {
            JSAssert([result objectForKey:@"data"] != nil, @"When doing a status request, it returned nil data");
            
            NSArray *data = [result objectForKey:@"data"];
            
            if ([data count] == 0) {
                [self requestMutualFriendsWith:currentMutualFriendsID];
                return;
            }
            
            NSDictionary *mostRecentStatus = [data objectAtIndex:0];
            
            if (![mostRecentStatus objectForKey:@"message"]) {
                [self requestMutualFriendsWith:currentMutualFriendsID];
                return;
            }
            
            if ([self.delegate respondsToSelector:@selector(didRecieveRandomFriendsPost:answer:forFriend:withOtherFriends:)]) {
                JSAssert([currentRandomMutialFriends count] > 0, @"Current random mutual friends got messed up somewhere");
                NSString *status = [mostRecentStatus valueForKey:@"message"];
                [self.delegate didRecieveRandomFriendsPost:@"Who posted this status?" answer:status forFriend:currentFriendWhoseFeedPostsViewing withOtherFriends:currentRandomMutialFriends];
            }
        }
        return;
    }
    else if ([request.graphPath hasSuffix:likesSuffix]) {
        if ([result isKindOfClass:[NSDictionary class]]) {
            JSAssert([result objectForKey:@"data"] != nil, @"When doing a likes request, it returned nil data");
            
            NSArray *data = [result objectForKey:@"data"];
            
            if ([data count] == 0) {
                [self requestMutualFriendsWith:currentMutualFriendsID];
                return;
            }
            
            NSDictionary *mostRecentLike = [data objectAtIndex:0];
            
            if (![mostRecentLike objectForKey:@"name"]) {
                [self requestMutualFriendsWith:currentMutualFriendsID];
                return;
            }
            
            NSString *likedName = [mostRecentLike valueForKey:@"name"];
            if ([self.delegate respondsToSelector:@selector(didReceiveRandomFriendsLike:answer:forFriend:withOtherFriends:)]) {
                JSAssert([currentRandomMutialFriends count] > 0, @"Current random mutual friends got messed up somewhere");
                
                NSString *question = [NSString stringWithFormat:@"Who liked this %@?", [mostRecentLike valueForKey:@"category"]];
                [self.delegate didReceiveRandomFriendsLike:question answer:likedName forFriend:currentFriendWhoseFeedPostsViewing withOtherFriends:currentRandomMutialFriends];
            }
        }
        return;
    }
    else if ([request.graphPath hasSuffix:currentPhotoURLSuffix]) {
        if ([result isKindOfClass:[NSDictionary class]]) {
            JSAssert([result objectForKey:@"data"] != nil, @"When doing a uploaded photo request, it returned nil data");
            NSArray *data = [result objectForKey:@"data"];
            if (data.count == 0) {
                // failed - no uploaded photos, do another request with another random friend
                [self requestMutualFriendsWith:currentMutualFriendsID];
            }
            else {
                NSDictionary *mostRecentPhoto = [data objectAtIndex:0];
                if (![mostRecentPhoto valueForKey:@"source"]) {
                    // failed, do another request with a mutual friend
                    [self requestMutualFriendsWith:currentMutualFriendsID];
                }
                else {
                    [self.delegate didReceivePhoto:[mostRecentPhoto valueForKey:@"source"] question:@"Who uploaded this photo?" forFriend:currentFriendWhoseFeedPostsViewing withOtherFriends:currentRandomMutialFriends];
                    return;
                }
            }
        }
        return;
    }
}

-(void)saveToDefaults
{
}

- (void)saveUserInfoToParse:(NSString*)uname withId:(NSString*)uid
{
    /* create the user object in the MasterUserList */
//    PFQuery *qu = [PFQuery queryWithClassName:kPFMasterUserListClass];
//    [qu whereKey:@"fbId" equalTo:uid];
//    [qu findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
//        if (!error) {
//            if (objects.count == 0) {
//                PFObject *u = [PFObject objectWithClassName:kPFMasterUserListClass];
//                [u setValue:uid forKey:@"fbId"];
//                [u setValue:uname forKey:@"name"];
//                [u setValue:@"YES" forKey:@"mplayer_enabled"];
//                [u saveInBackground];
//            }
//        }
//    }];
}

- (void)request:(FBRequest *)request didFailWithError:(NSError *)error
{
    [facebook logout];
    [self login];
    [[[UIAlertView alloc]initWithTitle:@"Facebook Error" message:@"Let Facebook log you back in and then return to the home screen." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    if ([self.delegate respondsToSelector:@selector(facebookRequestDidFail)])
        [self.delegate facebookRequestDidFail];
    // call a delegate method that signals a failure
}

- (void)fbDidNotLogin:(BOOL)cancelled
{
    if ([self.delegate respondsToSelector:@selector(didCancel)])
        [self.delegate didCancel];
}

- (void)fbDidExtendToken:(NSString*)accessToken
               expiresAt:(NSDate*)expiresAt
{
    NSLog(@"token extended");
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:accessToken forKey:@"FBAccessTokenKey"];
    [defaults setObject:expiresAt forKey:@"FBExpirationDateKey"];
    [defaults synchronize];
}

- (void)fbDidLogout
{
    
}

- (void)fbSessionInvalidated
{
    
}

- (BOOL)handleOpenURL:(NSURL *)url 
{
    return [facebook handleOpenURL:url];
}

void shuffle(int *array, size_t n)
{
    for (int i = 0; i < n; i++) {
        int rand = arc4random() % n;
        int rand2 = arc4random() % n;
        int temp = array[rand];
        array[rand] = array[rand2];
        array[rand2] = temp;
    }
}

@end
