//
//  JSInviteFriendsViewController.h
//  Amicus
//
//  Created by Josh Sklar on 5/21/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    JSInviteFriendsTypeFacebook,
    JSInviteFriendsTypeContacts
} JSInviteFriendsType;

@interface JSInviteFriendsViewController : UIViewController

- (void)setFriends:(NSArray*)friends;

- (id)initWithType:(JSInviteFriendsType)type_;





@end
