//
//  FFLoadingView.h
//  Amicus
//
//  Created by Josh Sklar on 6/1/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FFSavingView : UIView

- (id)initWithFrame:(CGRect)frame;

- (void)show;
- (void)hide;

@end
