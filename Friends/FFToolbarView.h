//
//  FFToolbarView.h
//  Friends
//
//  Created by Josh Sklar on 3/31/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FFGamesListViewController;

@interface FFToolbarView : UIView

- (id)initWithFrame:(CGRect)frame andHomeView:(FFGamesListViewController*)home;

@end
