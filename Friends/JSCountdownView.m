//
//  JSCountdownView.m
//  pie
//
//  Created by Josh Sklar on 5/15/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import "JSCountdownView.h"

#import <QuartzCore/QuartzCore.h>


static NSString * const kAngleKey = @"angle";
static NSString * const kBorderWidthKey = @"borderWidth";
static NSString * const kColorKey = @"color";
static NSString * const kValueKey = @"value";
static NSString * const kClockwiseKey = @"clockwise";

@interface JSCountdownView()
- (void)prepareView;
@end


@implementation JSCountdownView

@synthesize angle = _angle;
@synthesize borderWidth = _borderWidth;
@synthesize clockwise = _clockwise;
@synthesize color = _color;
@synthesize value = _value;

#pragma mark - Object Lifecycle

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
	
    if (self) {
		[self prepareView];
    }
	
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self prepareView];
    }
    return self;
}

- (void)prepareView
{
    [self setBackgroundColor:[UIColor clearColor]];
    
    _angle = 1.5f * M_PI;
    _borderWidth = 0.0f;
    _clockwise = YES;
    _color = [UIColor lightGrayColor];
    _value = 1.0f;
    
    [self addObserver:self
           forKeyPath:kAngleKey
              options:NSKeyValueObservingOptionNew
              context:NULL];
    
    [self addObserver:self
           forKeyPath:kBorderWidthKey
              options:NSKeyValueObservingOptionNew
              context:NULL];
    
    [self addObserver:self
           forKeyPath:kClockwiseKey
              options:NSKeyValueObservingOptionNew
              context:NULL];
    
    [self addObserver:self
           forKeyPath:kColorKey
              options:NSKeyValueObservingOptionNew
              context:NULL];
    
    [self addObserver:self
           forKeyPath:kValueKey
              options:NSKeyValueObservingOptionNew
              context:NULL];
}

- (void)dealloc
{
	[self removeObserver:self
			  forKeyPath:kAngleKey];
	
	[self removeObserver:self
			  forKeyPath:kBorderWidthKey];
	
	[self removeObserver:self
			  forKeyPath:kClockwiseKey];
	
	[self removeObserver:self
			  forKeyPath:kColorKey];
	
	[self removeObserver:self
			  forKeyPath:kValueKey];
}

#pragma mark - View Lifecycle

- (void)drawRect:(CGRect)rect
{
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	CGContextSetLineWidth(context, [self borderWidth]);
	
	CGContextSetStrokeColorWithColor(context, [[self color] CGColor]);
	
	CGContextStrokeEllipseInRect(context, CGRectInset([self bounds],
													  [self borderWidth] / 2.0f,
													  [self borderWidth] / 2.0f));
	
	CGRect innerBounds = CGRectInset([self bounds],
									 [self borderWidth],
									 [self borderWidth]);
	
	CGContextSetFillColorWithColor(context, [[self color] CGColor]);
	
	// If the value is 1.0, then just draw a circle.
	if ([self value] >= 1.0f) {
		CGContextFillEllipseInRect(context, innerBounds);
		return;
	} else if ([self value] <= 0.0f) {
		// Nothing to do here.
		return;
	}
	
	CGRect bounds = [self bounds];
	CGFloat width = CGRectGetWidth(bounds);
	CGFloat height = CGRectGetHeight(bounds);
    
	// Add a subpath to get a line segment from the center to the starting point of the arc
	CGContextMoveToPoint(context,
						 width / 2.0f,
						 height / 2.0f);
	
	// Compute the ending angle.
	CGFloat angleAdd = (2.0f * M_PI) * [self value];
	
	if ([self isClockwise]) {
		angleAdd *= -1.0f;
	}
	
	CGContextAddArc(context,
					width / 2.0f,
					height / 2.0f,
					innerBounds.size.width / 2.0f,
					[self angle],
					[self angle] + angleAdd,
					[self isClockwise]);
	
	CGContextAddLineToPoint(context,
							width / 2.0f,
							height / 2.0f);
	
	CGContextClosePath(context);
	
	// Fill the path
	CGContextFillPath(context);
}

#pragma mark - Key-Value Observing

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if (object == self) {
		if ([keyPath isEqualToString:kAngleKey] ||
			[keyPath isEqualToString:kColorKey] ||
			[keyPath isEqualToString:kValueKey] ||
			[keyPath isEqualToString:kClockwiseKey] ||
			[keyPath isEqualToString:kBorderWidthKey]) {
			dispatch_async(dispatch_get_main_queue(), ^{
				[self setNeedsDisplay];
			});
		}
	}
}

#pragma mark -

@end
