//
//  FFRecordCell.m
//  Amicus
//
//  Created by Josh Sklar on 5/22/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import "FFRecordCell.h"
#import "UILabel+Font.h"
#import "FFRecord.h"
#import "FFProfilePictureCache.h"
#import <QuartzCore/QuartzCore.h>

@interface FFRecordCell ()

@property (weak, nonatomic) IBOutlet UILabel *overallRecordLabel;
@property (weak, nonatomic) IBOutlet UILabel *themVsYouLabel;
@property (weak, nonatomic) IBOutlet UILabel *spHighScoreLabel;


@end

@implementation FFRecordCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {

        
        
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureLabels
{
    [self.nameLabel setAppFontMedium:20 color:[UIColor whiteColor]];
    [self.overallRecordLabel setAppFontMedium:14 color:[UIColor whiteColor]];
    [self.themVsYouLabel setAppFontMedium:14 color:[UIColor whiteColor]];
    [self.spHighScoreLabel setAppFontBlackItalic:13 color:[UIColor colorWithRed:31./255. green:83./255. blue:135./255. alpha:1]];
    
    [self.overallRecordValueLabel setAppFontMedium:17 color:nil];
    [self.themVsYouValueLabel setAppFontMedium:17 color:nil];
    
    [self.spHighScoreValueLabel configureLabelForSinglePlayerScore];
}

- (void)initWithRecord:(FFRecord*)r
{
    self.nameLabel.text = r.name;
    self.themVsYouLabel.text = [NSString stringWithFormat:@"%@ vs.you", [self getFirstName:r.name]];
    
    NSString *overallRecordText = [NSString stringWithFormat:@"%@-%@",  [r overallWins], [r overallLosses]];
    NSString *highScoreText = [NSString stringWithFormat:@"%@", [r spHighScore]];
    
    NSString *individualRecordText = [NSString stringWithFormat:@"%@-%@", [r theirNumWinsAgainstMe], [r myNumWinsAgainstThem]];
    
    self.overallRecordValueLabel.text = overallRecordText;
    self.themVsYouValueLabel.text = individualRecordText;
    self.spHighScoreValueLabel.text = highScoreText;
}

- (NSString*)getFirstName:(NSString*)name
{
    NSRange rangeOfSpace = [name rangeOfString:@" "];
    if (rangeOfSpace.location == NSNotFound)
        return name;
    else {
        NSString *firstName = [name substringToIndex:rangeOfSpace.location];
        return firstName;
    }
    
}


@end
