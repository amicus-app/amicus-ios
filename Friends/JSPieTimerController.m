//
//  JSPieTimerController.m
//  pie
//
//  Created by Josh Sklar on 5/15/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import "JSPieTimerController.h"

@interface JSPieTimerController ()

@property (strong, nonatomic) NSTimer *timer;
@property (strong, nonatomic) NSDate *startTime;

-(void)onTimer:(id)sender;

@end

@implementation JSPieTimerController {
    BOOL lastSpin;
}

-(void)startAnimation {
    
    self.isAnimating = YES;
    
    self.countdownView.value = 0.0f;
    
    self.startTime = [NSDate date];
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1/60 target:self selector:@selector(onTimer:) userInfo:nil repeats:YES];
}

-(void)stopAnimation
{
//    NSLog(@"stopping pie timer");
    self.isAnimating = NO;
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
}

- (void)reset
{
//    NSLog(@"resetting pie timer");
    [self stopAnimation];
    self.countdownView.value = 0.;
}

-(void)onTimer:(id)sender {
    
    CGFloat timeInterval = [self.startTime timeIntervalSinceNow] * -1;
    CGFloat angle = timeInterval/self.duration;
    
    if (angle > self.endingPercent)
        angle = self.endingPercent;
    
    self.countdownView.value = angle;
}

@end
