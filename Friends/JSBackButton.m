//
//  JSBackButton.m
//  Amicus
//
//  Created by Josh Sklar on 5/29/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import "JSBackButton.h"

@interface JSBackButton ()

@property (strong, nonatomic) UIImageView *backArrowIV;

@end

@implementation JSBackButton

- (id)init;
{
    self = [super init];
    if (self) {
        self.frame = CGRectMake(5, 0, 74, 44);

        self.backArrowIV = [[UIImageView alloc]initWithFrame:CGRectMake(6, 0, 22, 22)];
        
        CGRect oldFrame = self.backArrowIV.frame;
        self.backArrowIV.frame = CGRectMake(oldFrame.origin.x,
                                (self.frame.size.height - oldFrame.size.height)/2,
                                oldFrame.size.width,
                                oldFrame.size.height);
        
        
        [self.backArrowIV setImage:[UIImage imageNamed:@"back-icon"]];
        [self addTarget:self action:@selector(didTouchInButton:) forControlEvents:UIControlEventTouchDown | UIControlEventTouchDragEnter];
        [self addTarget:self action:@selector(didTouchOutOfButton:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchDragExit];
    
        [self addSubview:self.backArrowIV];
    }
    return self;
}

- (void)didTouchInButton:(id)sender
{
        [self.backArrowIV setAlpha:0.5];
}

- (void)didTouchOutOfButton:(id)sender
{
        [self.backArrowIV setAlpha:1.0];
}

- (void)setToBackButton
{
    self.showsTouchWhenHighlighted = NO;
    [self.backArrowIV setHidden:NO];
    [self.titleLabel setAlpha:0.];
}

- (void)setToNormalButton
{
    self.showsTouchWhenHighlighted = YES;
    [self.backArrowIV setHidden:YES];
    [self.titleLabel setAlpha:1.];
}



@end
