//
//  main.m
//  Friends
//
//  Created by Josh Sklar on 3/21/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FFAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FFAppDelegate class]));
    }
}
