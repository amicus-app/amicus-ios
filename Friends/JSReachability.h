//
//  JSReachability.h
//  JSKit
//
//  Created by Josh Sklar on 1/7/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSReachability : NSObject

+ (BOOL)hasInternetConnection:(NSString*)errorMessage;

@end
