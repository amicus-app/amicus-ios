//
//  UILabel+Font.h
//  Amicus
//
//  Created by Josh Sklar on 5/15/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Font)

- (void)setAppFontRegular:(CGFloat)size color:(UIColor*)color;
- (void)setAppFontRegularItalic:(CGFloat)size color:(UIColor*)color;
- (void)setAppFontBlack:(CGFloat)size color:(UIColor*)color;

- (void)setAppFontMedium:(CGFloat)size color:(UIColor*)color;

- (void)setAppFontBlackItalic:(CGFloat)size color:(UIColor*)color;
- (void)setAppFontLightItalic:(CGFloat)size color:(UIColor*)color;
- (void)setAppFontBoldItalic:(CGFloat)size color:(UIColor*)color;

- (void)configureFontSizeWithInitialSize:(CGFloat)initialSize;

- (void)configureLabelForSinglePlayerScore;

@end
