//
//  JSPieTimerController.h
//  pie
//
//  Created by Josh Sklar on 5/15/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSCountdownView.h"

@interface JSPieTimerController : NSObject

@property (strong, nonatomic) JSCountdownView *countdownView;
@property CGFloat duration;
@property CGFloat endingPercent;
@property NSInteger numTimesSpun;
@property BOOL isAnimating;

- (void)startAnimation;
- (void)stopAnimation;
- (void)reset;

@end
