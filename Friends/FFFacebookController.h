//
//  FFFacebookController.h
//  Amicus
//
//  Created by Josh Sklar on 5/18/12.
//  Copyright (c) 2012 Sklar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FBConnect.h"

@protocol FFFacebookControllerDelegate <NSObject>
@optional
- (void) didLogin;
- (void) didCancel;
- (void) informOfWhatIsBeingFetched:(NSString*)desc;
- (void) didReceiveFriends:(NSArray *) friends;
- (void) didRecieveRandomFriendsPost:(NSString*)questionText answer:(NSString*)answerText forFriend:(NSDictionary*)friendInfo withOtherFriends:(NSMutableArray*)otherFriends;

- (void) didReceivePhoto:(NSString*)photoURL question:(NSString*)question forFriend:(NSDictionary*)friendInfo withOtherFriends:(NSMutableArray*)otherFriends;

- (void) didReceiveRandomFriendsLikedPhotoInfo:(NSString*)photoURL forFriend:(NSDictionary*)friendInfo withOtherFriends:(NSMutableArray*)otherFriends;
- (void) didReceiveRandomFriendsLike:(NSString*)questionText answer:(NSString*)answerText forFriend:(NSDictionary*)friendInfo withOtherFriends:(NSMutableArray*)otherFriends;

- (void) informUserOfFriendFetchingData:(NSString*)friendName;

- (void) facebookRequestDidFail;

@end

@interface FFFacebookController : NSObject <FBSessionDelegate, FBRequestDelegate, FBDialogDelegate>

+(id) sharedInstance;

- (void) requestFriends;
- (void) requestMutualFriendsWith:(NSString*)friendID;
- (BOOL) handleOpenURL:(NSURL *) url;
- (void) login;
+ (NSDictionary*)me;

- (void)inactivateNetworkConnectionState;
- (void)activateNetworkConnectionState;

+ (NSArray*)getFriendsArray;
+ (NSArray*)getFriendIdsArray;
+ (NSString*)getFriendName:(NSString*)friendId;

- (void)postToFacebook:(NSString*)msg;
- (void)postToFriendsWall:(NSDictionary*)friendDict message:(NSString*)msg;

@property (strong, nonatomic) id <FFFacebookControllerDelegate> delegate;




@end
