//
//  JSInviteFriendsViewController.m
//  Amicus
//
//  Created by Josh Sklar on 5/21/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import "JSInviteFriendsViewController.h"
#import "UILabel+Font.h"
#import <QuartzCore/QuartzCore.h>
#import "FFProfilePictureCache.h"
#import <AddressBook/AddressBook.h>
#import <MessageUI/MessageUI.h>
#import "FFFacebookController.h"
#import "JSBackButton.h"
#import "UIView+Animation.h"

@interface JSInviteFriendsViewController () <UITableViewDataSource, UITableViewDelegate, MFMessageComposeViewControllerDelegate, UISearchBarDelegate>
{
    JSInviteFriendsType type;
}

@property (strong, nonatomic) NSMutableArray *filteredFriendsArray;
@property (strong, nonatomic) NSArray *friendsArray;

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) UISearchBar *searchBar;
@property (strong, nonatomic) UIButton *dismiss;

@end

@implementation JSInviteFriendsViewController

- (id)initWithType:(JSInviteFriendsType)type_
{
    self = [super init];
    if (self) {
        type = type_;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Invite Friends";
    [self setupTableView];
    
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 0)];
    self.searchBar.delegate = self;
    [self.searchBar sizeToFit];
    [[[self.searchBar subviews] objectAtIndex:0] removeFromSuperview];
    
    self.navigationItem.hidesBackButton = YES;
    
    
    [self.searchBar setPlaceholder:@"Search Friends"];
    self.tableView.tableHeaderView = self.searchBar;
    self.dismiss = [[JSBackButton alloc]init];
    [self.dismiss addTarget:self action:@selector(didTapCancel) forControlEvents:UIControlEventTouchUpInside];
    
    [self.navigationController.navigationBar addSubview:self.dismiss];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.dismiss partialFadeWithDuration:0.2 afterDelay:0. withFinalAlpha:1. completionBlock:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.dismiss partialFadeWithDuration:0.2 afterDelay:0. withFinalAlpha:0. completionBlock:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setFriends:(NSArray*)friends
{
    self.friendsArray = [NSArray arrayWithArray:friends];
    self.filteredFriendsArray = [NSMutableArray arrayWithArray:friends];
}

#pragma mark - Internal methods

- (void)setupTableView
{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.bounds.size.height - 44)];
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    [self.view addSubview:self.tableView];
}

- (void)didTapCancel
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableView data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.filteredFriendsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"CellID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell)
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    
    if (type == JSInviteFriendsTypeFacebook) {
        NSDictionary *friend = [self.filteredFriendsArray objectAtIndex:indexPath.row];
        [cell.textLabel setText:[friend valueForKey:@"name"]];
        [cell.textLabel setAppFontRegular:20 color:nil];
        
        [cell.imageView setHidden:NO];
        cell.imageView.layer.borderColor = [UIColor blackColor].CGColor;
        [cell.imageView setContentMode:UIViewContentModeScaleToFill];
        cell.imageView.layer.borderWidth = 2.;
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
        
        [cell.imageView setImage:[UIImage imageNamed:@"fb_default_pic"]];
        [[FFProfilePictureCache sharedInstance] getProfilePictureThumbnailForFriend:[friend valueForKey:@"id"] withCompletionBlock:^(NSData *profilePictureData) {
            [cell.imageView setImage:[UIImage imageWithData:profilePictureData]];
        }];
    }
    else if (type == JSInviteFriendsTypeContacts) {
        NSString* compositeName = (__bridge NSString *)ABRecordCopyCompositeName((__bridge ABRecordRef)[self.filteredFriendsArray objectAtIndex:indexPath.row]);
        
        [cell.textLabel setAppFontRegular:20 color:nil];
        [cell.textLabel setText:compositeName];
    }
    else {
        NSAssert(NO, @"Type was not facebook or contacts");
    }
    
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    return cell;
}

#pragma mark - UITableView delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.searchBar resignFirstResponder];
    
    if (type == JSInviteFriendsTypeFacebook) {
        NSDictionary *friend = [self.filteredFriendsArray objectAtIndex:indexPath.row];
        [[FFFacebookController sharedInstance] postToFriendsWall:friend message:@"hi"];
    }
    else if (type == JSInviteFriendsTypeContacts) {
        CFTypeRef phoneProperty = ABRecordCopyValue((__bridge ABRecordRef)[self.filteredFriendsArray objectAtIndex:indexPath.row], kABPersonPhoneProperty);
        NSArray *phones = (__bridge NSArray *)ABMultiValueCopyArrayOfAllValues(phoneProperty);
        NSString *firstPhone = [phones objectAtIndex:0];
        if (!firstPhone) {
            [[[UIAlertView alloc]initWithTitle:@"Error" message:@"No phone number for this contact exists." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
            return;
        }
        
        if ([MFMessageComposeViewController canSendText]) {
            MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
            if([MFMessageComposeViewController canSendText])
            {
                controller.recipients = @[firstPhone];
                controller.body = @"Download Amicus on the iOS App Store!";
                controller.messageComposeDelegate = self;
                [self presentViewController:controller animated:YES completion:nil];
            }
        }
        else {
            [[[UIAlertView alloc]initWithTitle:@"Shucks :-/" message:@"Can't send sms on this device" delegate:nil cancelButtonTitle:@"OK :-(" otherButtonTitles:nil]show];
        }
        
        // send sms message to that friend
    }
    else {
        NSAssert(NO, @"Type was not facebook or contacts");
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.searchBar resignFirstResponder];
}

#pragma mark - MailComposer methods

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:^{
    }];

    switch (result) {
        case MessageComposeResultSent:
            NSLog(@"Message Sent");
            break;
            
        case MessageComposeResultFailed:
            NSLog(@"Could not send message, try again");
            break;
            
        case MessageComposeResultCancelled:
            NSLog(@"Message Send Canceled");
            break;
    
        default:
            break;
    }
}

#pragma mark - UISearchBar delegate mehods

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
	[self filterFriends:searchText];
    [self.tableView reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar_
{
	[searchBar_ resignFirstResponder];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
}

- (void)filterFriends:(NSString*)searchText
{
    [self.filteredFriendsArray removeAllObjects];
	
	if (searchText == nil || [searchText length] == 0) {
		[self.filteredFriendsArray addObjectsFromArray:self.friendsArray];
	}
	else {
		for (id friend in self.friendsArray) {
			NSString *friendName;
            if (type == JSInviteFriendsTypeFacebook) {
                friendName = [(NSDictionary*)friend valueForKey:@"name"];
            }
            else if (type == JSInviteFriendsTypeContacts) {
                friendName = (__bridge NSString *)ABRecordCopyCompositeName((__bridge ABRecordRef)friend);
            }
            NSRange searchRange = [friendName rangeOfString:searchText options:NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch];
            if (searchRange.length > 0) {
                [self.filteredFriendsArray addObject:friend];
            }
		}
	}
}

@end
