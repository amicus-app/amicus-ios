//
//  FFRecord.m
//  Amicus
//
//  Created by Josh Sklar on 5/22/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import "FFRecord.h"

@implementation FFRecord

- (NSComparisonResult)compare:(FFRecord*)otherRecord
{
    return [self.overallWins compare:otherRecord.overallWins];
}

@end
