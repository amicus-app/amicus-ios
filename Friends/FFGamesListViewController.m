//
//  FFGamesListViewController.m
//  Friends
//
//  Created by Josh Sklar on 3/22/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import "FFGamesListViewController.h"
#import "FFFacebookController.h"
#import "FFChooseFriendViewController.h"
#import "FFMultiPlayerViewController.h"
#import "FFSinglePlayerViewController.h"
#import <Parse/Parse.h>
#import "FFUtilities.h"
#import "FFGamesListFlowLayout.h"
#import "FFGamesListCell.h"
#import <QuartzCore/QuartzCore.h>
#import "FFProfilePictureCache.h"
#import "FFRecordViewController.h"
#import "UIView+Animation.h"
#import "FFGamesListBackgroundView.h"
#import "UILabel+Font.h"
#import "FFMyAccountViewController.h"
#import "JSReachability.h"
#import "JSPageControl.h"

const static CGFloat kPanViewHeight = 30 + 44;
const static CGFloat kTurnLabelHeight = 39;
const static CGFloat kMoveAmount = 240.;

const static CGFloat kSinglePlayerBtnWidth = 248.;
const static CGFloat kSinglePlayerBtnHeight = 61.;

@interface FFGamesListViewController () <FFFacebookControllerDelegate, FFChooseFriendDelegate, UIAlertViewDelegate, UIScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
{
}

/* EGO pull to refresh methods */
- (void)reloadTableViewDataSource;
- (void)doneLoadingTableViewData;

/* tableview data source */
@property (strong, nonatomic) NSMutableArray *yourTurnGamesArray;
@property (strong, nonatomic) NSMutableArray *theirTurnGamesArray;

/* different game play objects */
@property (strong, nonatomic) FFGamePlayViewController *multiPlayerGamePlay;
@property (strong, nonatomic) FFGamePlayViewController *singlePlayerGamePlay;

/* for Single Player */
@property (strong, nonatomic) PFObject *singlePlayerGameObject;

/* UI elements */
@property (strong, nonatomic) UIBarButtonItem *addButton, *refreshButton;
@property (strong, nonatomic) UIButton *singlePlayerButton;
@property (strong, nonatomic) UIAlertView *deleteAllGamesAlertView;
@property (strong, nonatomic) UICollectionView *yourTurnCollectionView;
@property (strong, nonatomic) UICollectionView *theirTurnCollectionView;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) UIPageControl *pageControl;
@property (strong, nonatomic) NSMutableDictionary *profilePictureCache;
@property (strong, nonatomic) UIView *overlay;

/* for choosing opponent */
@property (strong, nonatomic) FFChooseFriendViewController *chooseFriend;

/* managing only a single fetch from parse */
@property BOOL fetchingFromParse;

@end

@implementation FFGamesListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Amicus";
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]];
    
    self.fetchingFromParse = NO;
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"home-navbar"] forBarMetrics:UIBarMetricsDefault];

    
    [self setupBackgroundView];
    [self setupSinglePlayerButton];
    [self setupCollectionViews];
    [self setupScrollView];

    [self setupPageControl];
    [self setupBarButtonItems];
    [self setupOverlay];
    //[selt setupPullToRefresh];
    
    self.yourTurnGamesArray = [[NSMutableArray alloc]init];
    self.theirTurnGamesArray = [[NSMutableArray alloc]init];
    
    self.multiPlayerGamePlay = [[FFMultiPlayerViewController alloc]init];
    self.singlePlayerGamePlay = [[FFSinglePlayerViewController alloc]init];
    
    [self.multiPlayerGamePlay setHome:self];
    [self.singlePlayerGamePlay setHome:self];
    
    self.profilePictureCache = [[NSMutableDictionary alloc]init];

    [self fetchGames];
    
    /* set up notification center observers */
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchGames) name:kPushNotificationReceived object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchGames) name:kFFHasTurnsPending object:nil];
    
    
    self.deleteAllGamesAlertView = [[UIAlertView alloc]initWithTitle:@"Delete All Games" message:@"Are you sure you want to delete all of yours games? This will also remove them from your opponents devices." delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    
    self.chooseFriend = [[FFChooseFriendViewController alloc]init];
    [self.chooseFriend setDelegate:self];
    [self.chooseFriend setFriendsArray:[[NSUserDefaults standardUserDefaults] valueForKey:kFriendsArrayKey]];
    [self.chooseFriend filterFriendsList];
    
    [self performActionsOnFirstStartup];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Scroll view delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.scrollView) {
        CGRect oldFrame = self.singlePlayerButton.frame;
        CGFloat initialX = (self.view.frame.size.width - kSinglePlayerBtnWidth)/2;
        self.singlePlayerButton.frame = CGRectMake(initialX + scrollView.contentOffset.x, oldFrame.origin.y, oldFrame.size.width, oldFrame.size.height);
        CGRect frame = [[UIScreen mainScreen] applicationFrame];
        float roundedValue = round(scrollView.contentOffset.x / frame.size.width);
        self.pageControl.currentPage = roundedValue;
    }
}

#pragma mark - UICollectionView DataSource Method

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    /* + 1 for the 'Add Game' cell */
    if (collectionView == self.yourTurnCollectionView) {
        return [self.yourTurnGamesArray count] + 1;
    }
    else
        return [self.theirTurnGamesArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    FFGamesListCell * cell = (FFGamesListCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"FFGamesListCell" forIndexPath:indexPath];
    
    if (indexPath.row == 0 && collectionView == self.yourTurnCollectionView) {
        [cell.nameLabel setText:@"Start A New Game"];
        [cell.nameLabel setAppFontRegular:11. color:nil];
        // the picture for this */
        NSData *data = UIImagePNGRepresentation([UIImage imageNamed:@"new-game"]);
        [cell setProfilePicture:data];
        return cell;
    }
    
    PFObject *game;
    if (collectionView == self.yourTurnCollectionView)
        game = [self.yourTurnGamesArray objectAtIndex:indexPath.row - 1];
    else
        game = [self.theirTurnGamesArray objectAtIndex:indexPath.row];
    
    [cell.nameLabel setText:[game valueForKey:@"name"]];
    [cell.nameLabel configureFontSizeWithInitialSize:17];
    
    NSString *friendId = [game valueForKey:@"id"];
    
    [[FFProfilePictureCache sharedInstance] getProfilePictureForFriend:friendId withCompletionBlock:^(NSData *profilePictureData) {
        [cell setProfilePicture:profilePictureData];
    }];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (![JSReachability hasInternetConnection:@"Can't play game"])
        return;
    
    if (indexPath.row == 0 && collectionView == self.yourTurnCollectionView) {
        [self didTapAdd:nil];
        return;
    }
    
    NSDictionary *gameObj;
    if (collectionView == self.yourTurnCollectionView)
        gameObj = [self.yourTurnGamesArray objectAtIndex:indexPath.row - 1];
    else
        gameObj = [self.theirTurnGamesArray objectAtIndex:indexPath.row];
    
    if (self.fetchingFromParse)
        return;
    
    [self.multiPlayerGamePlay setFriendDict:gameObj];
    NSString *decoyGameID = [gameObj valueForKey:@"decoy_gameID"];
    [self.multiPlayerGamePlay setGameID:decoyGameID];
    [self.multiPlayerGamePlay setGameObj:gameObj];
    [self.navigationController pushViewController:self.multiPlayerGamePlay animated:YES];
}


#pragma mark - Internal methods

- (void)didTapAdd:(id)sender
{
    [self.addButton setEnabled:NO];
    if ([[NSUserDefaults standardUserDefaults] valueForKey:kFriendsArrayKey]) {
        [self.addButton setEnabled:YES];
        [self presentViewController:self.chooseFriend animated:YES completion:nil];
    }
    else {
        JSAssert(false, @"tried to tap add without there already being a stored friends array");
    }
}

- (void)fetchGames
{
    if (!self.fetchingFromParse) {
        if (![JSReachability hasInternetConnection:@"Can't load games"])
            return;

        self.fetchingFromParse = YES;
        [self.refreshButton setEnabled:NO];
        
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"creator_id == %@ OR opponent_id == %@", [[FFFacebookController me] valueForKey:@"id"], [[FFFacebookController me] valueForKey:@"id"]];
        PFQuery *quer = [PFQuery queryWithClassName:kPFGamesClassName predicate:pred];
        [quer findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            self.fetchingFromParse = NO;
            if (!error) {
                /* empty the arrays */
                [self.yourTurnGamesArray removeAllObjects];
                [self.theirTurnGamesArray removeAllObjects];
                
                for (PFObject *obj in objects) {
                    if ([[obj valueForKey:kPFCreatorID] isEqualToString:[[FFFacebookController me] valueForKey:@"id"]]) {
                        NSArray *objs = @[[obj objectForKey:kPFOpponentName], [obj objectForKey:kPFOpponentID], [obj valueForKey:@"decoy_gameID"], [obj valueForKey:@"creator_name"], [obj valueForKey:@"creator_score"], [obj valueForKey:@"opponent_name"], [obj valueForKey:@"opponent_score"], [obj valueForKey:@"turn"], [obj valueForKey:kPFGameStatusKey], [obj createdAt], obj];
                        NSArray *keys = @[@"name", @"id", @"decoy_gameID", @"creator_name", @"creator_score", @"opponent_name", @"opponent_score", @"turn", kPFGameStatusKey, @"created_at", @"pfObj_ptr"];
                        NSDictionary *d = [NSDictionary dictionaryWithObjects:objs forKeys:keys];
                        
                        if ([[obj valueForKey:@"turn"] isEqualToString:[[FFFacebookController me] valueForKey:@"id"]])
                            [self.yourTurnGamesArray addObject:d];
                        else
                            [self.theirTurnGamesArray addObject:d];
                    }
                    else {
                        NSArray *objs = @[[obj objectForKey:kPFCreatorName], [obj objectForKey:kPFCreatorID], [obj valueForKey:@"decoy_gameID"],  [obj valueForKey:@"creator_name"], [obj valueForKey:@"creator_score"], [obj valueForKey:@"opponent_name"], [obj valueForKey:@"opponent_score"], [obj valueForKey:@"turn"], [obj valueForKey:kPFGameStatusKey], [obj createdAt], obj];
                        NSArray *keys = @[@"name", @"id", @"decoy_gameID", @"creator_name", @"creator_score", @"opponent_name", @"opponent_score", @"turn", kPFGameStatusKey, @"created_at", @"pfObj_ptr"];
                        NSDictionary *d = [NSDictionary dictionaryWithObjects:objs forKeys:keys];
                        
                        if ([[obj valueForKey:@"turn"] isEqualToString:[[FFFacebookController me] valueForKey:@"id"]])
                            [self.yourTurnGamesArray addObject:d];
                        else
                            [self.theirTurnGamesArray addObject:d];
                    }
                }
                
                [self.yourTurnGamesArray sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                    return [[obj2 valueForKey:@"created_at"] compare:[obj1 valueForKey:@"created_at"]];
                }];
                [self.theirTurnGamesArray sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                    return [[obj2 valueForKey:@"created_at"] compare:[obj1 valueForKey:@"created_at"]];
                }];
                
                /* set the badge number to however many games are 'your' turn */
                PFInstallation *currentInstallation = [PFInstallation currentInstallation];
                if(currentInstallation.badge > 0) {
                    currentInstallation.badge = self.yourTurnGamesArray.count;
                    [currentInstallation saveInBackground];
                }
                
                [self.yourTurnCollectionView reloadData];
                [self.theirTurnCollectionView reloadData];
                [self.refreshButton setEnabled:YES];
                [self doneLoadingTableViewData];
            }
            else {
                NSLog(@"parse error");
            }
        }];
        [self fetchSinglePlayerGame];
    }
}

- (void)didTapShowMenu:(id)sender
{
    [self.navigationController.view move:@"up" by:10 withDuration:0.1 completionBlock:^(BOOL finished) {
        [self.navigationController.view move:@"down" by:kMoveAmount withDuration:0.4 completionBlock:nil];
    }];

    [self.overlay partialFadeWithDuration:0.5 afterDelay:0. withFinalAlpha:0.7 completionBlock:nil];
}

- (void)fetchSinglePlayerGame
{
    /* fetch the single player object */
    if (![JSReachability hasInternetConnection:@"Can't load single player game"]) {
        return;
    }
    
    [self.singlePlayerButton setEnabled:NO];
    
    PFQuery *singlePlayerQuery = [PFQuery queryWithClassName:kPFSinglePlayerGamesClassName];
    NSString *gameName = [NSString stringWithFormat:@"SG%@",[[FFFacebookController me]valueForKey:@"id"]];
    [singlePlayerQuery whereKey:@"game_name" equalTo:gameName];
    [singlePlayerQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error) {
            JSAssert(NO, [error description]);
        }
        else {
            JSAssert(objects.count <= 1, @"More than 1 single player game for you in database. Occured in fetchSinglePlayerGame");
            
            if (objects.count > 0) {
                [self.singlePlayerButton setEnabled:YES];
                self.singlePlayerGameObject = [objects objectAtIndex:0];
            }
            else {
                self.singlePlayerGameObject = nil;
            }
        }
    }];
}

/* called from other classes */
- (void)disableSinglePlayerButton
{
    [self.singlePlayerButton setEnabled:NO];
}

- (void)didTapDeleteAllGames
{
    [self.deleteAllGamesAlertView show];
    [self moveNavbarUpWithCompletionBlock:nil];
}

- (void)didTapShowRecords
{
    FFRecordViewController *record = [[FFRecordViewController alloc]init];
    [self presentViewController:[[UINavigationController alloc]initWithRootViewController:record] animated:YES completion:nil];
    [self moveNavbarUpWithCompletionBlock:nil];
}

- (void)didTapShowMyAccount
{
    FFMyAccountViewController *account = [[FFMyAccountViewController alloc]init];
    [self presentViewController:[[UINavigationController alloc]initWithRootViewController:account] animated:YES completion:nil];
    [self moveNavbarUpWithCompletionBlock:nil];
}

- (void)deleteAllGames
{
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"creator_id == %@ OR opponent_id == %@", [[FFFacebookController me] valueForKey:@"id"], [[FFFacebookController me] valueForKey:@"id"]];
    PFQuery *q = [PFQuery queryWithClassName:kPFGamesClassName predicate:pred];
    [q findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            int counter = 0;
            for (PFObject *obj in objects) {
                [obj deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                    if (counter == objects.count - 1) {
                        [self fetchGames];
                    }
                }];
                counter++;
            }
        }
        else
            NSLog(@"%@", [error description]);
    }];
}

- (void)didTapOverlay
{
    [self moveNavbarUpWithCompletionBlock:nil];
}

- (void)moveNavbarUpWithCompletionBlock:(void (^)())block
{
    [self.navigationController.view move:@"down" by:10 withDuration:0.1 completionBlock:^(BOOL finished) {
        [self.navigationController.view move:@"up" by:kMoveAmount withDuration:0.4 completionBlock:^(BOOL finished) {
            if (block)
                block();
        }];
    }];
    [self.overlay partialFadeWithDuration:0.5 afterDelay:0. withFinalAlpha:0. completionBlock:nil];
}


#pragma mark - FFChooseFriend delegate methods

- (void)didTapSoloPlay
{
    if (![JSReachability hasInternetConnection:@"Can't play single player"])
        return;
    
    JSAssert(self.singlePlayerGameObject != nil, @"singlePlayerGameObj was nil");
    
    [self.singlePlayerGamePlay setSinglePlayerGameObj:self.singlePlayerGameObject];
    [self.navigationController pushViewController:self.singlePlayerGamePlay animated:YES];
}

- (void)didSelectFriend:(NSDictionary *)friendDict
{
    if (![JSReachability hasInternetConnection:@"Can't start game"])
        return;
    
    PFObject *obj = [PFObject objectWithClassName:kPFGamesClassName];
    [obj setValue:[[FFFacebookController me] valueForKey:@"id"] forKey:kPFCreatorID];
    [obj setValue:[[FFFacebookController me] valueForKey:@"name"] forKey:kPFCreatorName];
    [obj setValue:[friendDict valueForKey:@"id"] forKey:kPFOpponentID];
    [obj setValue:[friendDict valueForKey:@"name"] forKey:kPFOpponentName];
    [obj setValue:[[FFFacebookController me] valueForKey:@"id"] forKey:@"turn"];
    [obj setValue:@0 forKey:kPFCreatorScore];
    [obj setValue:@0 forKey:kPFOpponentScore];
    [obj setValue:kPFGameStatusStarted forKey:kPFGameStatusKey];
    
    
    NSString *formattedDate = [NSString stringWithFormat:@"%@",[NSDate date]];
    formattedDate = [formattedDate stringByReplacingOccurrencesOfString:@"-" withString:@"_"];
    formattedDate = [formattedDate stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    formattedDate = [formattedDate stringByReplacingOccurrencesOfString:@":" withString:@"_"];


    NSRange rangeOfPlus = [formattedDate rangeOfString:@"+"];
    formattedDate = [formattedDate stringByReplacingCharactersInRange:NSMakeRange(rangeOfPlus.location - 1, 6)  withString:@""];
    
    JSAssert(formattedDate.length == 19, @"Formatted date is not a length of 19 chars. in didSelectFriend, FFGamesListVC");
    
    NSString *decoyGameId = [NSString stringWithFormat:@"G%@vs%@d%@",[[FFFacebookController me] valueForKey:@"id"], [friendDict valueForKey:@"id"], formattedDate];
    [obj setValue:decoyGameId forKey:@"decoy_gameID"];
    [obj saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            [self fetchGames];
        }
        else
            NSLog(@"FFGamesListController:didSelectFriend parse error");
    }];
    
    NSString *pushMsg = [NSString stringWithFormat:@"%@ has started a game with you.", [[FFFacebookController me] valueForKey:@"name"]];
    NSDictionary *pushDict = [NSDictionary dictionaryWithObjectsAndKeys:
                              pushMsg, @"alert",
                              @"default", @"sound",
                              nil];

    JSAssert(friendDict != nil, @"didn't send a push to anyone...");
    NSString *formattedID = [NSString stringWithFormat:@"fb%@", [friendDict valueForKey:@"id"]];
    [PFPush sendPushDataToChannelInBackground:formattedID withData:pushDict];
}

- (void) pageTurn: (UIPageControl *) aPageControl
{
    int whichPage = aPageControl.currentPage;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3f];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    self.scrollView.contentOffset = CGPointMake(320.0f * whichPage, 0.0f);
    [UIView commitAnimations];
}

#pragma mark - UIAlertView delegate methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView == self.deleteAllGamesAlertView) {
        if (buttonIndex == 1) {
            [self deleteAllGames];
        }
    }
    else if (alertView.tag == kSinglePlayerGameOverAlertViewTag) {
        if (buttonIndex == 1) {
            [self didTapSoloPlay];
        }
    }
}

#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource{
	[self fetchGames];
	//  should be calling your tableviews data source model to reload
	//  put here just for demo
	
}

- (void)doneLoadingTableViewData
{
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
}

#pragma mark - UI Helper methods

- (void)setupBackgroundView
{
    FFGamesListBackgroundView *v = [[FFGamesListBackgroundView alloc]initWithFrame:self.navigationController.view.frame andGamesList:self];
    [self.navigationController.view.superview addSubview:v];
    [self.navigationController.view.superview sendSubviewToBack:v];
}

- (void)setupCollectionViews
{
    CGFloat yPos = self.singlePlayerButton.frame.origin.y + self.singlePlayerButton.frame.size.height + 25;
    CGFloat xOffset = 0;
    
    /* THIS IS FOR iphone 5! */
    FFGamesListFlowLayout *layout1 = [[FFGamesListFlowLayout alloc]init];
    self.yourTurnCollectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(xOffset, yPos, self.view.bounds.size.width - xOffset, self.view.bounds.size.height - yPos - kPanViewHeight - 20) collectionViewLayout:layout1];
    
    FFGamesListFlowLayout *layout = [[FFGamesListFlowLayout alloc]init];
    self.theirTurnCollectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(self.view.bounds.size.width + xOffset, yPos, self.view.bounds.size.width - xOffset, self.view.bounds.size.height - yPos - kPanViewHeight - 20) collectionViewLayout:layout];
    
    NSArray *arr = @[self.yourTurnCollectionView, self.theirTurnCollectionView];
    for (UICollectionView *cv in arr) {
        [cv setDelegate:self];
        [cv setDataSource:self];
        [cv registerClass:[FFGamesListCell class] forCellWithReuseIdentifier:@"FFGamesListCell"];
        [cv setBackgroundColor:[UIColor clearColor]];
    }
}

- (void)setupScrollView
{
    UILabel *yourTurnLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, kTurnLabelHeight)];
    [yourTurnLabel setText:@"Your Turn"];
    [yourTurnLabel setAppFontBlack:30 color:[UIColor whiteColor]];

    UILabel *theirTurnLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width, 0, self.view.frame.size.width, kTurnLabelHeight)];
    [theirTurnLabel setText:@"Opponent's Turn"];
    [theirTurnLabel setAppFontBlack:30 color:[UIColor whiteColor]];
    
    NSArray *labels = @[yourTurnLabel, theirTurnLabel];
    for (UILabel *l in labels) {
        [l setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"turn-bar"]]];
        l.textAlignment = NSTextAlignmentCenter;
    }

    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - kPanViewHeight)];
    [self.scrollView setDelegate:self];
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width * 2, self.view.frame.size.height - kPanViewHeight);
	self.scrollView.bouncesZoom = YES;
    [self.scrollView setPagingEnabled:YES];
    [self.scrollView setShowsHorizontalScrollIndicator:NO];
    [self.scrollView addSubview:yourTurnLabel];
    [self.scrollView addSubview:theirTurnLabel];
    [self.scrollView addSubview:self.singlePlayerButton];
    
    CGFloat yPos = self.singlePlayerButton.frame.origin.y + self.singlePlayerButton.frame.size.height + 5;
    
    UIImageView *iv1 = [[UIImageView alloc]initWithFrame:CGRectMake(0, yPos, self.view.bounds.size.width, self.view.bounds.size.height - yPos - kPanViewHeight)];
    [iv1 setImage:[UIImage imageNamed:@"games-list-background-i5"]];
    [self.scrollView addSubview:iv1];
    
    UIImageView *iv2 = [[UIImageView alloc]initWithFrame:CGRectMake(self.view.bounds.size.width, yPos, self.view.bounds.size.width, self.view.bounds.size.height - yPos - kPanViewHeight)];
    [iv2 setImage:[UIImage imageNamed:@"games-list-background-i5"]];
    [self.scrollView addSubview:iv2];

    [self.scrollView addSubview:self.yourTurnCollectionView];
    [self.scrollView addSubview:self.theirTurnCollectionView];
    
    [self.view addSubview:self.scrollView];
}

- (void)setupPageControl
{
    self.pageControl = [[JSPageControl alloc]initWithFrame:CGRectMake(0, self.scrollView.frame.origin.y + self.scrollView.frame.size.height, self.view.frame.size.width, 30)];
    [self.pageControl addTarget:self action:@selector(pageTurn:) forControlEvents:UIControlEventValueChanged];
    
    [self.pageControl setNumberOfPages:2];
    [self.pageControl setCurrentPage:0];
    [self.view addSubview:self.pageControl];
}

- (void)setupSinglePlayerButton
{
    self.singlePlayerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.singlePlayerButton setFrame:CGRectMake((self.view.frame.size.width - kSinglePlayerBtnWidth)/2, 5 + kTurnLabelHeight, kSinglePlayerBtnWidth, kSinglePlayerBtnHeight)];
    [self.singlePlayerButton setBackgroundImage:[UIImage imageNamed:@"single-player-button"] forState:UIControlStateNormal];
    [self.singlePlayerButton addTarget:self action:@selector(didTapSoloPlay) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setupBarButtonItems
{
    UIButton *addBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [addBtn setImage:[UIImage imageNamed:@"navbar-add"] forState:UIControlStateNormal];
    [addBtn addTarget:self action:@selector(didTapAdd:) forControlEvents:UIControlEventTouchUpInside];
    addBtn.showsTouchWhenHighlighted = YES;
    self.addButton = [[UIBarButtonItem alloc]initWithCustomView:addBtn];
    
    
    UIButton *showMenu = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [showMenu setImage:[UIImage imageNamed:@"navbar-settings"] forState:UIControlStateNormal];
    [showMenu addTarget:self action:@selector(didTapShowMenu:) forControlEvents:UIControlEventTouchUpInside];
    showMenu.showsTouchWhenHighlighted = YES;
    UIBarButtonItem *s = [[UIBarButtonItem alloc]initWithCustomView:showMenu];
    
    UIButton *refresh = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [refresh setImage:[UIImage imageNamed:@"navbar-refresh"] forState:UIControlStateNormal];
    [refresh addTarget:self action:@selector(fetchGames) forControlEvents:UIControlEventTouchUpInside];
    refresh.showsTouchWhenHighlighted = YES;
    self.refreshButton = [[UIBarButtonItem alloc]initWithCustomView:refresh];
    
    self.navigationItem.leftBarButtonItems = @[s, self.refreshButton];
    
    self.navigationItem.rightBarButtonItem = self.addButton;
}

- (void)setupOverlay
{
    self.overlay = [[UIView alloc]initWithFrame:self.view.frame];
    [self.overlay setBackgroundColor:[UIColor blackColor]];
    [self.overlay setAlpha:0.];
    UITapGestureRecognizer *tappedOverlay = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didTapOverlay)];
    [self.overlay addGestureRecognizer:tappedOverlay];
    [self.navigationController.view addSubview:self.overlay];
}

- (void)performActionsOnFirstStartup
{
    NSString *seenTutorialKey = @"hasSeenTutorial";
    NSString *savedUserInfoToParseKey = @"savedUserInfoToParse";
    NSString *savedRecordObjectToParse = @"saveRecordObjectToParse";
    NSString *savedSinglePlayerObjectToParse = @"savedSinglePlayerObjectToParse";
    
    
    // mini-tutorial
    if (![[NSUserDefaults standardUserDefaults] valueForKey:seenTutorialKey]) {
        UIImageView *iv = [[UIImageView alloc]initWithFrame:self.view.bounds];
        [iv setImage:[UIImage imageNamed:@"swipe-left"]];
        [self.view addSubview:iv];
        [iv partialFadeWithDuration:1. afterDelay:3 withFinalAlpha:0. completionBlock:^(BOOL finished) {
            [iv removeFromSuperview];
        }];
        [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:seenTutorialKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    // saving their user info to parse
    if (![[NSUserDefaults standardUserDefaults] valueForKey:savedUserInfoToParseKey]) {
        NSString *uid = [[FFFacebookController me] valueForKey:@"id"];
        NSString *uname = [[FFFacebookController me] valueForKey:@"name"];
        
        JSAssert(uid != nil, @"uid == nil");
        JSAssert(uname != nil, @"uname == nil");
        
        PFQuery *qu = [PFQuery queryWithClassName:kPFMasterUserListClass];
        [qu whereKey:@"fbId" equalTo:uid];
        [qu findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:savedUserInfoToParseKey];
            [[NSUserDefaults standardUserDefaults] synchronize];

            if (!error) {
                if (objects.count == 0) {
                    PFObject *u = [PFObject objectWithClassName:kPFMasterUserListClass];
                    [u setValue:uid forKey:@"fbId"];
                    [u setValue:uname forKey:@"name"];
                    [u setValue:@"YES" forKey:@"mplayer_enabled"];
                    [u saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                        if (succeeded) {
                        }
                        else {
                            [[[UIAlertView alloc]initWithTitle:@"Error Saving" message:@"Please reinstall the app." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
                        }
                    }];
                }
            }
        }];
    }
    
    if (![[NSUserDefaults standardUserDefaults] valueForKey:savedRecordObjectToParse]) {
        NSString *uid = [[FFFacebookController me] valueForKey:@"id"];
        NSString *uname = [[FFFacebookController me] valueForKey:@"name"];
        
        JSAssert(uid != nil, @"uid == nil");
        JSAssert(uname != nil, @"uname == nil");
        
        /* initialize the Record objet. first, make sure none have already been created with the given id */
        PFQuery *q = [PFQuery queryWithClassName:kPFRecordsClassName];
        [q whereKey:kPFRecordsUserIdKey equalTo:uid];
        [q findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            // tried to look, so next time don't look
            [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:savedRecordObjectToParse];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            if (!error) {
                if (objects.count == 0) {
                    PFObject *recordObj = [PFObject objectWithClassName:kPFRecordsClassName];
                    [recordObj setValue:uid forKey:kPFRecordsUserIdKey];
                    [recordObj setValue:uname forKey:@"userName"];
                    [recordObj setValue:@0 forKey:@"wins"];
                    [recordObj setValue:@0 forKey:@"losses"];
                    [recordObj setValue:@0 forKey:@"single_player_high_score"];
                    [recordObj saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                        if (succeeded) {
                            
                        }
                        if (error) {
                            [[[UIAlertView alloc]initWithTitle:@"Error Saving Record" message:@"Please reinstall the app." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
                        }
                    }];
                }
            }
        }];
    }
    
    // also save the single player object. make sure it doesn't exist first
    
    if (![[NSUserDefaults standardUserDefaults] valueForKey:savedSinglePlayerObjectToParse]) {
        PFQuery *q = [PFQuery queryWithClassName:kPFSinglePlayerGamesClassName];
        NSString *gameName = [NSString stringWithFormat:@"SG%@",[[FFFacebookController me]valueForKey:@"id"]];
        [q whereKey:kPFSGGameName equalTo:gameName];
        [q findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:savedSinglePlayerObjectToParse];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            if (!error) {
                if (objects.count == 0) {
                    PFObject *sing = [PFObject objectWithClassName:kPFSinglePlayerGamesClassName];
                    [sing setValue:gameName forKey:kPFSGGameName];
                    [sing setValue:@0 forKey:kPFSGScore];
                    [sing setValue:@0 forKey:kPFSGNumIncorrectAnswers];
                    [sing setValue:@0 forKey:kPFSGHighScore];
                    self.singlePlayerGameObject = sing;
                    [sing saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                        [self.singlePlayerButton setEnabled:YES];
                        if (succeeded) {
                        }
                        else
                            JSAssert(NO, [error description]);
                    }];
                }
            }
        }];
        
    }

}

@end
