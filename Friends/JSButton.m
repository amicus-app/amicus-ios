//
//  JSButton.m
//  Amicus
//
//  Created by Josh Sklar on 4/9/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import "JSButton.h"
#import <QuartzCore/QuartzCore.h>


@interface JSButton()
{
    block tappedButtonBlock;
}

@end
@implementation JSButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.layer.cornerRadius = 6.;
        self.layer.borderColor = [UIColor blackColor].CGColor;
        self.layer.borderWidth = 1.5;
        [self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        // Initialization code
    }
    return self;
}

- (void)performBlockOnTouchUpInside:(block)b
{
    tappedButtonBlock = b;
    [self addTarget:self action:@selector(didTapBtn:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didTapBtn:(id)sender
{
    tappedButtonBlock(sender);
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
