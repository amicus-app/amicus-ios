//
//  UIView+Positioning.h
//  Amicus
//
//  Created by Josh Sklar on 5/16/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Positioning)

- (void)centerHorizontally;
- (void)centerHorizontallyWithinView:(UIView*)view;
- (CGFloat)getLocationOfBottom;
- (CGFloat)getLocationOfRight;

@end
