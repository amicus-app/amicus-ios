//
//  JSPageControl.m
//  Amicus
//
//  Created by Josh Sklar on 5/29/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import "JSPageControl.h"

@implementation JSPageControl

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        activeImage = [UIImage imageNamed:@"dot-active"];
        inactiveImage = [UIImage imageNamed:@"dot-inactive.png"];
    }
    return self;
}

-(void)updateDots
{
    for (int i = 0; i < [self.subviews count]; i++)
    {
        UIImageView* dot = [self.subviews objectAtIndex:i];
        dot.frame = CGRectMake(dot.frame.origin.x, dot.frame.origin.y, 14, 14.5);
        if (i == self.currentPage) {
            if ([dot isKindOfClass:[UIImageView class]]) {
                dot.image = activeImage;
            }
        }
        else {
            if ([dot isKindOfClass:[UIImageView class]]) {
                dot.image = inactiveImage;
            }
        }
    }
}

-(void)setCurrentPage:(NSInteger)page
{
    [super setCurrentPage:page];
    [self updateDots];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
