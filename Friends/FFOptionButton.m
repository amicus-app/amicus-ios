//
//  FFOptionButton.m
//  Friends
//
//  Created by Josh Sklar on 3/28/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import "FFOptionButton.h"
#import <QuartzCore/QuartzCore.h>
#import "UIView+Animation.h"
#import "UILabel+Font.h"

@interface FFOptionButton ()

@property (strong, nonatomic) UILabel *nameLabel;
@property (strong, nonatomic) UIImageView *profilePictureImageView;

@end

@implementation FFOptionButton


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

        [self setBackgroundImage:[UIImage imageNamed:@"picture-box"] forState:UIControlStateNormal];

        self.profilePictureImageView = [[UIImageView alloc]initWithFrame:CGRectMake(4, 3, 74, 74)];
        [self.profilePictureImageView setImage:[UIImage imageNamed:@"fb_default_pic"]];
        [self addSubview:self.profilePictureImageView];
        
        self.nameLabel = [[UILabel alloc] init];
        [self.nameLabel setFrame:CGRectMake(0., 78., frame.size.width, 26)];
        
        [self.nameLabel setNumberOfLines:2];
        [self.nameLabel setAppFontRegular:17. color:nil];
        
        [self.nameLabel setTextColor:[UIColor blackColor]];//colorWithRed:90./255. green:91./255. blue:93./255. alpha:1.]];
        [self.nameLabel setBackgroundColor:[UIColor clearColor]];
        [self.nameLabel setTextAlignment:NSTextAlignmentCenter];
        [self addSubview:self.nameLabel];
        
        
        [self addTarget:self action:@selector(enlarge) forControlEvents:(UIControlEventTouchDown | UIControlEventTouchDragEnter)];
        [self addTarget:self action:@selector(de_enlarge) forControlEvents:(UIControlEventTouchUpInside | UIControlEventTouchDragExit)];
        [self setAdjustsImageWhenHighlighted:NO];
        [self setAdjustsImageWhenDisabled:NO];
        
    }
    return self;
}

- (void)setName:(NSString*)title
{
    [self.nameLabel setText:title];
    [self.nameLabel configureFontSizeWithInitialSize:17];
}

- (NSString*)name
{
    return self.nameLabel.text;
}

- (void)setBackgroundImage:(NSData*)data
{
    [self.profilePictureImageView setImage:[UIImage imageWithData:data]];
//    [self setBackgroundImage:[UIImage imageWithData:data] forState:UIControlStateNormal];
}

- (void)resetBackgroundImage
{
    [self.profilePictureImageView setImage:[UIImage imageNamed:@"fb_default_pic"]];
//    [self setBackgroundImage:[UIImage imageNamed:@"fb_default_pic"] forState:UIControlStateNormal];
}

- (void)flashWithCompletionBlock:(void (^)(BOOL finished))block
{
    [self.profilePictureImageView grow:15 withDuration:0.2 completionBlock:^(BOOL finished) {
        [self.profilePictureImageView shrink:10 withDuration:0.2 completionBlock:^(BOOL finished) {
            [self.profilePictureImageView grow:15 withDuration:0.2 completionBlock:^(BOOL finished) {
                [self.profilePictureImageView shrink:20 withDuration:0.2 completionBlock:^(BOOL finished) {
                    block(finished);
                }];
            }];
        }];
    }];
}

- (void)grow:(NSInteger)amount withDuration:(NSTimeInterval)duration completionBlock:(void (^)(BOOL finished))block
{
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         CGRect newFrame = CGRectMake(self.frame.origin.x - amount,
                                                      self.frame.origin.y - amount,
                                                      self.frame.size.width + amount*2,
                                                      self.frame.size.height + amount*2);
                         [self setFrame:newFrame];
                     } completion:block];
    
}

- (void)shrink:(NSInteger)amount withDuration:(NSTimeInterval)duration completionBlock:(void (^)(BOOL finished))block
{
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         CGRect newFrame = CGRectMake(self.frame.origin.x + amount,
                                                      self.frame.origin.y + amount,
                                                      self.frame.size.width - amount*2,
                                                      self.frame.size.height - amount*2);
                         [self setFrame:newFrame];
                     } completion:block];
}

- (void)enlarge
{
    [self.profilePictureImageView grow:5 withDuration:0.1 completionBlock:nil];
}

- (void)de_enlarge
{
    [self.profilePictureImageView shrink:5 withDuration:0.1 completionBlock:nil];
}

- (void)setEnabled:(BOOL)enabled
{
    [super setEnabled:enabled];
    if (enabled == NO) {
        [self partialFadeWithDuration:0.2 afterDelay:0. withFinalAlpha:0.6 completionBlock:nil];
    }
    else {
        [self partialFadeWithDuration:0.2 afterDelay:0. withFinalAlpha:1.0 completionBlock:nil];
    }

}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
