//
//  FFToolbarView.m
//  Friends
//
//  Created by Josh Sklar on 3/31/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import "FFToolbarView.h"
#import "FFGamesListViewController.h"

@interface FFToolbarView ()

@property (strong, nonatomic) FFGamesListViewController *home;
@property (strong, nonatomic) UILabel *label;
@property (strong, nonatomic) UIButton *addGameButton;


@end

@implementation FFToolbarView

- (id)initWithFrame:(CGRect)frame andHomeView:(FFGamesListViewController*)home;
{
    self = [super initWithFrame:frame];
    if (self) {
        self.home = home;
        
        [self setBackgroundColor:[UIColor blueColor]];
        
        self.addGameButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [self.addGameButton setFrame:CGRectMake(250, 0, 100, kPanViewHeight-40)];
        [self.addGameButton setTitle:@"Pause" forState:UIControlStateNormal];
        [self.addGameButton addTarget:self.home action:@selector(newGameButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.addGameButton];
        
        UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc]initWithTarget:self.home action:@selector(didPanToolbarView:)];
        [self addGestureRecognizer:pan];
        
        self.label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, kPanViewHeight)];
        [self.label setText:@"Friends"];
        [self.label setTextColor:[UIColor whiteColor]];
        [self.label setBackgroundColor:[UIColor clearColor]];
        [self.label setTextAlignment:NSTextAlignmentCenter];
        [self addSubview:self.label];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
