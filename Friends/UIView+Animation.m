//
//  UIView+Animation.m
//  LegendsCard
//
//  Created by Josh Sklar on 7/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIView+Animation.h"

@implementation UIView (Animation)

-(void) partialFadeWithDuration: (NSTimeInterval) duration afterDelay: (NSTimeInterval) delay withFinalAlpha:(double)finalAlpha completionBlock:(void (^)(BOOL finished))block {
    [UIView animateWithDuration:duration
                          delay:delay
                        options:UIViewAnimationCurveEaseIn
                     animations:^{
                         [self setAlpha:finalAlpha];
                         
                     } 
                     completion:block];
}

- (void)hideWithFade:(BOOL)hidden withDuration:(NSTimeInterval)duration afterDelay:(NSTimeInterval)delay
{
    [UIView animateWithDuration:duration delay:delay
                        options:UIViewAnimationCurveEaseIn
                     animations:^{
                            if (hidden)
                                [self setAlpha:0.0];
                            else
                                [self setAlpha:1.0];
                     } completion:^(BOOL finished){}];
}

//-(void)shakeScreen {
//    CABasicAnimation *animation = 
//    [CABasicAnimation animationWithKeyPath:@"position"];
//    [animation setDuration:0.1];
//    [animation setRepeatCount:2];
//    [animation setAutoreverses:YES];
//    [animation setFromValue:[NSValue valueWithCGPoint:
//                             CGPointMake(self.center.x - 10.0f, self.center.y)]];
//    [animation setToValue:[NSValue valueWithCGPoint:
//                           CGPointMake(self.center.x + 10.0f, self.center.y)]];
//    [self.layer addAnimation:animation forKey:@"position"];
//}

-(void)move:(NSString*)direction by:(NSInteger)amount withDuration:(NSTimeInterval)duration completionBlock:(void (^)(BOOL finished))block {
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationCurveEaseIn
                     animations:^{
                         if ([direction isEqualToString:@"up"]) {
                             [self setCenter:CGPointMake(self.center.x, self.center.y-amount)];
                         }
                         else if ([direction isEqualToString:@"down"]) {
                             [self setCenter:CGPointMake(self.center.x, self.center.y+amount)];
                         }
                         else if ([direction isEqualToString:@"left"]) {
                             [self setCenter:CGPointMake(self.center.x - amount, self.center.y)];
                         }
                         else if ([direction isEqualToString:@"right"]) {
                             [self setCenter:CGPointMake(self.center.x + amount, self.center.y)];

                         }
                     }
                     completion:block];
}

- (void)moveToFrame:(CGRect)frame withDuration:(NSTimeInterval)duration completionBlock:(void (^)(BOOL finished))block
{
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationCurveEaseIn
                     animations:^{
                         [self setFrame:frame];
                     } completion:block];
}

- (void)shrink:(NSInteger)amount withDuration:(NSTimeInterval)duration completionBlock:(void (^)(BOOL finished))block
{
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationCurveEaseIn
                     animations:^{
                         CGRect newFrame = CGRectMake(self.frame.origin.x + amount,
                                                      self.frame.origin.y + amount,
                                                      self.frame.size.width - amount*2,
                                                      self.frame.size.height - amount*2);
                         [self setFrame:newFrame];
                     } completion:block];
}

- (void)grow:(NSInteger)amount withDuration:(NSTimeInterval)duration completionBlock:(void (^)(BOOL finished))block
{
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationCurveEaseIn
                     animations:^{
                         CGRect newFrame = CGRectMake(self.frame.origin.x - amount,
                                                      self.frame.origin.y - amount,
                                                      self.frame.size.width + amount*2,
                                                      self.frame.size.height + amount*2);
                         [self setFrame:newFrame];
                     } completion:block];
    
}

- (void)blinkwithDuration:(NSTimeInterval)duration completionBlock:(void (^)(BOOL finished))block
{
    [self partialFadeWithDuration:duration/5. afterDelay:0 withFinalAlpha:1.0 completionBlock:^(BOOL finished) {
        [self partialFadeWithDuration:duration/5. afterDelay:0 withFinalAlpha:0.0 completionBlock:^(BOOL finished) {
            [self partialFadeWithDuration:duration/5. afterDelay:0 withFinalAlpha:1.0 completionBlock:^(BOOL finished) {
                [self partialFadeWithDuration:duration/5. afterDelay:0 withFinalAlpha:0.0 completionBlock:^(BOOL finished) {
                    [self partialFadeWithDuration:duration/5. afterDelay:0 withFinalAlpha:1.0 completionBlock:^(BOOL finished) {
                    }];
                }];
            }];
        }];
    }];
}

@end
