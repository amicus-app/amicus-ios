//
//  FFGamesListFlowLayout.m
//  Friends
//
//  Created by Josh Sklar on 4/2/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import "FFGamesListFlowLayout.h"

@implementation FFGamesListFlowLayout

-(id)init
{
    self = [super init];
    if (self) {
        self.itemSize = CGSizeMake(83, 105);
        self.scrollDirection = UICollectionViewScrollDirectionVertical;
//        self.minimumInteritemSpacing = 0.0f;
//        self.minimumLineSpacing = 10.0f;
        self.sectionInset = UIEdgeInsetsMake(0, 10, 0, 10);
//        self.sectionInset = UIEdgeInsetsMake(5.0f, 15.0f, 5.0f, 15.0f);
    }
    return self;
}

@end
