//
//  FFMultiPlayerViewController.h
//  Friends
//
//  Created by Josh Sklar on 3/30/13.
//  Copyright (c) 2013 Josh Sklar. All rights reserved.
//

#import "FFGamePlayViewController.h"

@interface FFMultiPlayerViewController : FFGamePlayViewController

@end
